package com.wuyan.web.form.helper;

import lombok.Getter;

/**
 * 分词存储模式
 */

@Getter
public enum WordStoragePathTypeEnum {
    LOCAL("同级存储", "local"),
    NEW("独立存储", "new"),
    ALL("并存存储", "all");

    private String name;

    private String value;

    WordStoragePathTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
