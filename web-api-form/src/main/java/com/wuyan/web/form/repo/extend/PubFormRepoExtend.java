package com.wuyan.web.form.repo.extend;

import com.wuyan.web.entity.PubForm;
import com.wuyan.web.repo.PubFormRepo;
import org.springframework.stereotype.Repository;

/**
 * 扩展
 */

@Repository
public interface PubFormRepoExtend extends PubFormRepo {
    PubForm findFirstByFormRefAndStatusAndDisabled(String formRef, Integer status, Integer disabled);
}