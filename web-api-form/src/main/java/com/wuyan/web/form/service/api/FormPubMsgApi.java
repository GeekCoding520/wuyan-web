package com.wuyan.web.form.service.api;

import com.wuyan.web.entity.PubMsg;
import com.wuyan.web.form.PubMsgForm;
import com.wuyan.web.helper.rep.RepBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 消息
 */

@FeignClient(url = "${wuyan.web.base-url}/wuyan/api/pub-msg", name = "FormPubMsgApi")
public interface FormPubMsgApi {

    @PostMapping("/create-dept-notify/{dept}")
    RepBody<List<PubMsg>> createDeptNotify(@PathVariable("dept") Integer dept,
                                           @RequestParam(value = "dataId", required = false, defaultValue = "0") String dataId,
                                           @RequestBody @Validated String content);
}
