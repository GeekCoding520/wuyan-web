package com.wuyan.web.form.helper;

import com.wuyan.web.entity.PubConfig;
import com.wuyan.web.helper.PubConfigHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FormHelper {

    @Autowired
    private PubConfigHelper pubConfigHelper;

    /**
     * 获取流程表单名称
     *
     * @return String
     */
    public String getFlowTableName() {
        List<PubConfig> config = pubConfigHelper.configs("sys", "flow.tableName");
        if (null == config || config.size() < 1) return "";
        return config.get(0).getCfgValue();
    }

    /**
     * 获取流程处理过程记录表名称
     *
     * @return String
     */
    public String getFlowNodeTableName() {
        List<PubConfig> config = pubConfigHelper.configs("sys", "flow.node.tableName");
        if (null == config || config.size() < 1) return "";
        return config.get(0).getCfgValue();
    }

    /**
     * 新增时，强制登录的表单ID
     *
     * @return Set<Integer>
     */
    public Set<Integer> getAddForceLogin() {
        List<PubConfig> config = pubConfigHelper.configs("form", "add.forceLogin");
        return config.stream().map(t -> Integer.parseInt(t.getCfgValue())).collect(Collectors.toSet());
    }

    /**
     * 新增时，需要进行消息通知的配置
     *
     * @return Set<Integer>
     */
    public List<String> getAddNotifyByFormId(Integer formId) {
        List<PubConfig> config = pubConfigHelper.configs("form", "add.notify." + formId);
        return config.stream().map(PubConfig::getCfgValue).collect(Collectors.toList());
    }
}
