package com.wuyan.web.helper.rep;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * 业务处理代码
 *
 * @author wuyan
 */

@Getter
public enum RepCodeEnum implements RepCodeInterface {
    // TODO
    OK(0, "success"),

    ERR_PARAMS(1000, "参数异常"),
    ERR_NO_ID(1001, "不存在此项"),
    ERR_NO_DATA(1002, "执行异常：即本应存在响应数据但没有返回"),
    ERR_NO_FORM(1004, "表单不存在"),
    ERR_UPDATE(1005, "更新失败"),
    ERR_DELETE(1006, "删除失败"),
    ERR_FORM_DISABLED(1007, "当前不可操作"),
    ERR_REPEAT_OP(1008, "请不要重复操作"),

    ERR_LOGIN(2000, "登录失败"),
    ERR_LOGIN_USER(2001, "账户异常"),
    ERR_LOGIN_USERNAME(2002, "账户名不存在"),
    ERR_LOGIN_PASSWORD(2003, "密码错误"),
    ERR_LOGIN_VERIFY_INVALID(2004, "验证码已失效"),
    ERR_LOGIN_VERIFY(2005, "验证码错误"),
    ERR_LOGIN_VERIFY_AUTH(2006, "验证码认证失败"),
    ERR_LOGIN_VERIFY_STOP(2007, "未启用验证码服务"),
    ERR_LOGIN_STATUS(2008, "账户被禁用"),
    ERR_LOGIN_EXPIRE(2009, "登录过期"),

    ERR_UPLOAD_NULL(3001, "没有上传任何文件"),
    ERR_UPLOAD_NOT_ALLOW(3002, "此类文件不被允许上传"),

    ERR_ORDER_INVENTORY(4001, "库存不足"),
    ERR_ORDER_CART_NO(4002, "没有需要结算的商品"),

    ERR_OP_NO(5001, "没有操作权限"),

    UNKNOWN(-1, "请求失败");

    private final int code;
    private final String msg;

    RepCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 获取指定code的msg
     *
     * @param code 业务码
     * @return String
     */
    public static String getMsgByCode(int code) {
        RepCodeEnum[] values = RepCodeEnum.values();
        Optional<RepCodeEnum> optionalRepCodeEnum = Arrays.stream(values).filter(t -> t.code == code).findFirst();
        return optionalRepCodeEnum.map(repCodeEnum -> repCodeEnum.msg).orElse("failed");
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
