package com.wuyan.web.helper.async;

import com.wuyan.web.entity.PubMsg;
import com.wuyan.web.form.PubMsgForm;
import com.wuyan.web.repo.extend.PubMsgRepoExtend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * 功能描述：异步任务业务类（@Async也可添加在方法上，@Async放在类上，表示类内方法皆为异步方法）
 */

@Component
@Async
@Slf4j
public class AsyncTaskService {

    @Autowired
    private PubMsgRepoExtend pubMsgRepo;

    /**
     * 发送单个消息
     *
     * @param form 数据
     * @return Future<RepBody < PubMsg>>
     */
    public Future<PubMsg> sendMsg(PubMsgForm form) {
        PubMsg pubMsg = PubMsg.builder().build();
        BeanUtils.copyProperties(form, pubMsg);
        pubMsg.setCreateTime(LocalDateTime.now());
        return new AsyncResult<>(pubMsgRepo.save(pubMsg));
    }

    /**
     * 批量发送消息
     *
     * @param forms 数据
     * @return Future<RepBody < List < PubMsg>>>
     */
    public Future<List<PubMsg>> sendMsg(List<PubMsgForm> forms) {
        List<PubMsg> res = forms.stream().map(t -> {
            PubMsg pubMsg = PubMsg.builder().build();
            BeanUtils.copyProperties(t, pubMsg);
            pubMsg.setCreateTime(LocalDateTime.now());
            return pubMsgRepo.save(pubMsg);
        }).collect(Collectors.toList());

        return new AsyncResult<>(res);
    }
}