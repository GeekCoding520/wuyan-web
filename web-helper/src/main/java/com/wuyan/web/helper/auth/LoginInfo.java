package com.wuyan.web.helper.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.web.entity.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 账户登录信息
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginInfo {
    /**
     * 授权码
     */
    private String token;

    /**
     * token有效期单位秒
     */
    private long expiresIn;

    /**
     * 账户信息
     */
    private PubAccount account;

    /**
     * 所属部门
     */
    private PubDept dept;

    /**
     * 拥有的职位
     */
    private List<PubPost> posts;

    /**
     * 角色组
     */
    private List<PubRole> roles;

    /**
     * 角色所拥有的接口组
     */
    private List<PubApi> apis;

    /**
     * 角色所拥有的菜单组
     */
    private List<PubFunction> menus;

    /**
     * 用户信息
     */
    private PubUser user;

    /**
     * 登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime loginTime;

    /**
     * 刷新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime refreshTime;

    /**
     * 下线时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime unLineTime;
}
