package com.wuyan.web.helper.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义查询条件: 查询实体
 *
 * @author wuyan
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomGroupParams {

    /**
     * 分组名，可用于多条件的混合查询
     */
    private String team;

    /**
     * 返回的字段名称
     */
    private String valueName;

    /**
     * 统计的字段名称
     */
    private String fieldName;

    /**
     * 统计方式
     */
    private String op;
}
