package com.wuyan.web.helper.req;


import com.querydsl.core.types.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomQueryOrderParams {
    private String filedName;
    private String orderName;
    private Order order;

    /**
     * 完整性校验
     * @return boolean
     */
    public boolean check() {
        init();
        return StringUtils.isNotBlank(filedName) && null != order;
    }

    /**
     * 初始化数据
     */
    public void init() {
        if (StringUtils.isNotBlank(filedName) && null == order) {
            this.order = "DESC".equals(orderName) ? Order.DESC : Order.ASC;
        }

        if (null == order) {
            this.order = Order.ASC;
        }
    }
}
