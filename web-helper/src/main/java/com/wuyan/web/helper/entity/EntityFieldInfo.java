package com.wuyan.web.helper.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体类字段描述
 *
 * @author wuyan
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EntityFieldInfo<T> {
    /**
     * 字段名
     */
    private String name;

    /**
     * 字段类类型
     */
    private Class<T> clz;

    /**
     * 附加属性
     */
    private String additional;
}