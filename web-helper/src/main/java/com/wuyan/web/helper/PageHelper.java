package com.wuyan.web.helper;

import com.querydsl.core.QueryResults;
import com.wuyan.web.helper.rep.RepPageData;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author gaochen
 * @date 2019/2/12
 */
@UtilityClass
public class PageHelper {
    private static final String[] DEFAULT_ORDER = new String[]{"id"};

    /**
     * 创建分页条件
     *
     * @param page  当前页
     * @param limit 每页大小
     * @param sort  升降序
     * @param order 排序字段
     * @return Pageable
     */
    public static Pageable page(int page, int limit, boolean sort, String... order) {
        if (limit == -1 || limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        if (order == null || order.length == 0) {
            order = DEFAULT_ORDER;
        }

        return PageRequest.of(page, limit, sort ? Sort.Direction.ASC : Sort.Direction.DESC, order);
    }

    /**
     * 创建分页条件
     *
     * @param page  当前页
     * @param limit 每页大小
     * @param sort  排序信息
     * @return Pageable
     */
    public static Pageable page(int page, int limit, Sort sort) {
        if (limit == -1 || limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        return PageRequest.of(page, limit, sort);
    }

    /**
     * 来自querydsl的关怀分页
     * @param list 分页查询结果
     * @param page 当前页
     * @param limit 页大小
     * @param <T> 数据类类型
     * @return RepPageData<T>
     */
    public static <T> RepPageData<T> pageResp(QueryResults<T> list, int page, int limit) {
        return RepPageData.<T>builder()
                .page(page)
                .limit(limit)
                .count(list.getTotal())
                .list(list.getResults()).build();
    }

    /**
     * 封装分页响应数据体
     *
     * @param list     当前页数据
     * @param pageable 分页条件
     * @param count    总计
     * @param <T>      数据类型
     * @return RepPageData<T>
     */
    public static <T> RepPageData<T> pageResp(List<T> list,
                                              Pageable pageable,
                                              long count) {
        return RepPageData.<T>builder()
                .page(pageable == null ? 0 : pageable.getPageNumber())
                .limit(pageable == null ? list.size() : pageable.getPageSize())
                .count(count == 0 ? list.size() : count)
                .list(list).build();
    }

    /**
     * 封装分页响应数据体
     *
     * @param listAll  数据
     * @param pageable 分页条件
     * @param <T>      数据类型
     * @return Map<String, Object>
     */
    public static <T> RepPageData<T> pageResp(List<T> listAll, Pageable pageable) {
        List<T> list = listAll.stream().skip(pageable.getOffset())
                .limit(pageable.getPageSize())
                .collect(Collectors.toList());
        return pageResp(list, pageable, list.size());
    }
}
