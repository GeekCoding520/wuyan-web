package com.wuyan.web.helper.convert;

import com.wuyan.helper.kit.endecryption.EncryptionHelper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.security.NoSuchAlgorithmException;

/**
 * AttributeConverter: 此接口用于转化实体属性的
 * 用于对数据进行加密或者解密,可逆: 选取AES
 */

@Slf4j
public class EncryptionUnReversibleConvert implements AttributeConverter<String, String> {

    /**
     * 密钥
     */
    private static final String secret = "wuyan-202103221930";

    @SneakyThrows
    @Override
    public String convertToDatabaseColumn(String value) {
        return encode(value);
    }

    @SneakyThrows
    @Override
    public String convertToEntityAttribute(String value) {
        return value;
    }

    /**
     * 加密
     *
     * @param value 值
     * @return String
     * @throws NoSuchAlgorithmException e
     */
    public static String encode(Object value) throws NoSuchAlgorithmException {
        if (null == value) {
            return null;
        }

        return EncryptionHelper.getInstance().md5(value.toString());
    }
}
