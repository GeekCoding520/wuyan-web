package com.wuyan.web.helper.rep;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 统一分页响应数据体结构
 *
 * @author wuyan
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepPageData<T> {
    /**
     * 当前页
     */
    private int page;

    /**
     * 每页条目数
     */
    private int limit;

    /**
     * 总计
     */
    private long count;

    /**
     * 当前页数据
     */
    private List<T> list;
}
