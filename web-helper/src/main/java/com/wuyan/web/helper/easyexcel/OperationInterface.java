package com.wuyan.web.helper.easyexcel;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 一个操作定义。
 */

public interface OperationInterface<T> {
    /**
     * 执行单元
     *
     * @param name 执行体名称
     * @param data 数据区
     * @return boolean 执行结果
     */
    boolean make(String name, T data, T firstData, AtomicInteger row) throws Exception;
}
