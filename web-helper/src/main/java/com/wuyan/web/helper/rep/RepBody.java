package com.wuyan.web.helper.rep;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一响应数据结构
 *
 * @author wuyan
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepBody<T> {
    /**
     * 业务码
     */
    private int code;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 数据体
     */
    private T data;
}
