package com.wuyan.web.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis 操作
 */

@Component
@Slf4j
public class SpringRedisHelper {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 写入缓存
     *
     * @param key   键
     * @param value 值
     * @return 处理结果
     */
    public <T> boolean set(final String key, T value) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 写入缓存设置时效时间
     *
     * @param key        键
     * @param value      值
     * @param expireTime 常驻时间,秒
     * @return boolean
     */
    public <T> boolean set(final String key, T value, Long expireTime) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 批量删除对应的value
     *
     * @param keys 键
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * 批量删除key
     *
     * @param pattern pattern
     */
    public void removePattern(final String pattern) {
        Set<String> keys = redisTemplate.keys(pattern);
        if (keys != null && !keys.isEmpty()) {
            redisTemplate.delete(keys);
        }
    }

    /**
     * 删除对应的value
     *
     * @param key key
     */
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * 判断缓存中是否有对应的value
     *
     * @param key key
     * @return boolean
     */
    public boolean exists(final String key) {
        Boolean bl = redisTemplate.hasKey(key);
        return bl != null && bl;
    }

    /**
     * 读取缓存
     *
     * @param key    键
     * @param tClass 类型
     * @return T
     */
    public <T> T get(final String key, Class<T> tClass) {
        return tClass.cast(redisTemplate.opsForValue().get(key));
    }

    /**
     * 哈希 添加
     *
     * @param key     键
     * @param hashKey 哈希值
     * @param value   值
     */
    public void hmSet(String key, Object hashKey, Object value) {
        redisTemplate.opsForHash().put(key, hashKey, value);
    }

    /**
     * 哈希获取数据
     *
     * @param key     键
     * @param hashKey 哈希
     * @return T
     */
    public <T> T hmGet(String key, Object hashKey, Class<T> tClass) {
        return tClass.cast(redisTemplate.opsForHash().get(key, hashKey));
    }

    /**
     * 列表添加
     *
     * @param key   键
     * @param value 列表
     */
    public <T> void lPush(String key, List<T> value) {
        redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 列表获取
     *
     * @param key   键
     * @param start 开始
     * @param end   结束
     * @return List<T>
     */
    public <T> List<T> lRange(String key, long start, long end, Class<List<T>> tClass) {
        return tClass.cast(redisTemplate.opsForList().range(key, start, end));
    }

    /**
     * 集合添加
     *
     * @param key   键
     * @param value 值
     */
    public <T> void addSet(String key, Set<T> value) {
        redisTemplate.opsForSet().add(key, value);
    }

    /**
     * 集合获取
     *
     * @param key 键
     * @return Set<T>
     */
    public <T> Set<T> getSet(String key, Class<Set<T>> listClass) {
        return listClass.cast(redisTemplate.opsForSet().members(key));
    }

    /**
     * 有序集合添加
     *
     * @param key   键
     * @param value 值
     * @param score 比值
     */
    public <T> void addSetSort(String key, Set<T> value, double score) {
        redisTemplate.opsForZSet().add(key, value, score);
    }

    /**
     * 有序集合获取
     *
     * @param key 键
     * @param min 左边
     * @param max 右边
     * @return Set<T>
     */
    public <T> Set<T> rangeByScore(String key, double min, double max, Class<Set<T>> setClass) {
        return setClass.cast(redisTemplate.opsForZSet().rangeByScore(key, min, max));
    }
}