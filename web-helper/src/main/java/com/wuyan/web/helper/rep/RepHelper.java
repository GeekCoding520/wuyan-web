package com.wuyan.web.helper.rep;

import java.util.Collection;

/**
 * 响应数据处理工具
 *
 * @author wuyan
 */

public interface RepHelper {

    /**
     * 成功，默认响应
     *
     * @param <T> 数据类型
     * @return RepBody<T>
     */
    default <T> RepBody<T> ok() {
        return RepBody.<T>builder()
                .code(RepCodeEnum.OK.getCode())
                .msg("success")
                .build();
    }

    /**
     * 成功，同时携带数据
     *
     * @param data 响应数据
     * @param <T>  数据类型
     * @return RepBody<T>
     */
    default <T> RepBody<T> ok(T data) {
        if(null == data){
            return error(RepCodeEnum.ERR_NO_DATA);
        }

        if(data instanceof Collection){
            Collection collection = (Collection) data;
            if(collection.isEmpty()) {
                return error(RepCodeEnum.ERR_NO_DATA);
            }
        }

        return RepBody.<T>builder()
                .code(RepCodeEnum.OK.getCode())
                .msg("success")
                .data(data)
                .build();
    }

    default <T> RepBody<T> error(RepCodeInterface code){
        return RepBody.<T>builder().code(code.getCode()).msg(code.getMsg()).build();
    }

    default <T> RepBody<T> error(int code, String msg){
        return RepBody.<T>builder().code(code).msg(msg).build();
    }
}
