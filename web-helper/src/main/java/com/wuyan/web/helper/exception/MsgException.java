package com.wuyan.web.helper.exception;

/**
 * 消息通知类自定义异常体
 */

public class MsgException extends RuntimeException {

    private final int code;

    public MsgException(String message) {
        this(-1, message);
    }

    public MsgException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "code: " + this.code + " -> " + getMessage();
    }
}
