package com.wuyan.web.helper;


import com.wuyan.web.helper.convert.EncryptionReversibleConvert;
import com.wuyan.web.helper.convert.EncryptionUnReversibleConvert;
import com.wuyan.web.helper.entity.EntityFieldInfo;

import java.util.List;
import java.util.Map;

/**
 * 隐私数据加解密配置
 */

public class DataEncryptionHelper {

    /**
     * 加密处理
     *
     * @param key    配置键
     * @param obj    数据实例
     * @param config 配置信息
     * @throws Exception e
     */
    public static void encode(String key, Object obj, Map<String, List<EntityFieldInfo<?>>> config)
            throws Exception {
        if (config.containsKey(key)) {
            List<EntityFieldInfo<?>> entityFieldInfos = config.get(key);

            for (EntityFieldInfo<?> entityFieldInfo : entityFieldInfos) {
                Object value = ReflexHelper.getFieldValue(obj,
                        entityFieldInfo.getName(),
                        entityFieldInfo.getClz());

                String encode = encode(entityFieldInfo, value);

                ReflexHelper.setFieldValue(obj, entityFieldInfo.getName(), encode);
            }
        }
    }

    /**
     * 加密处理
     *
     * @param field            字段名
     * @param value            被加密的数据
     * @param entityFieldInfos 字段描述
     * @return String
     * @throws Exception e
     */
    public static String encode(String field, String value, List<EntityFieldInfo<?>> entityFieldInfos)
            throws Exception {
        // 加密扫描
        if (null != entityFieldInfos && !entityFieldInfos.isEmpty()) {
            for (EntityFieldInfo<?> entityFieldInfo : entityFieldInfos) {
                if (entityFieldInfo.getName().equals(field)) {
                    value = encode(entityFieldInfo, value);
                    break;
                }
            }
        }

        return value;
    }

    /**
     * 加密处理
     *
     * @param entityFieldInfo 实体字段配置
     * @param value           原始数据
     * @return String
     * @throws Exception e
     */
    public static String encode(EntityFieldInfo<?> entityFieldInfo, Object value)
            throws Exception {
        String encode;

        if (entityFieldInfo.getAdditional().startsWith("un")) {
            encode = EncryptionUnReversibleConvert.encode(value);
        } else {
            encode = EncryptionReversibleConvert.encode(value);
        }

        return encode;
    }

    /**
     * 解密处理
     *
     * @param key    配置键
     * @param obj    数据实例
     * @param config 配置信息
     * @throws Exception e
     */
    public static void decode(String key, Object obj, Map<String, List<EntityFieldInfo<?>>> config)
            throws Exception {
        if (config.containsKey(key)) {
            List<EntityFieldInfo<?>> entityFieldInfos = config.get(key);

            for (EntityFieldInfo<?> entityFieldInfo : entityFieldInfos) {
                Object value = ReflexHelper.getFieldValue(obj,
                        entityFieldInfo.getName(),
                        entityFieldInfo.getClz());

                String decode;

                if (!entityFieldInfo.getAdditional().startsWith("un")) {
                    decode = EncryptionReversibleConvert.decode(value);
                    ReflexHelper.setFieldValue(obj, entityFieldInfo.getName(), decode);
                }
            }
        }
    }
}
