package com.wuyan.web.helper.req;

import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.helper.rep.RepBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 基础架构服务调用配置
 */

@FeignClient(url = "${wuyan.web.auth-url}/auth", name = "wuyan-web-auth-url")
public interface WuyanWebAuthApi {
    /**
     * 根据授权码校验授权状态
     *
     * @param token 临时授权码
     * @return RepBody<LoginInfo>
     */
    @GetMapping("/api/check/{token}")
    RepBody<LoginInfo> check(@PathVariable(value = "token") String token);
}
