package com.wuyan.web.helper;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * 使json解析器同时解析[text/plain]的数据
 * @author wuyan
 */

public class MappingJackson2HttpMessageTextPlainConverter extends MappingJackson2HttpMessageConverter {
    public MappingJackson2HttpMessageTextPlainConverter() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_PLAIN);
        setSupportedMediaTypes(mediaTypes);
    }
}
