package com.wuyan.web.helper.rep;

public interface RepCodeInterface {
    int getCode();
    String getMsg();
}
