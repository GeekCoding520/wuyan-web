package com.wuyan.web.helper;

import com.wuyan.web.entity.PubConfig;
import com.wuyan.web.helper.req.CustomQueryParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 配置区帮助工具
 */

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PubConfigHelper {

    /**
     * 配置
     */
    private List<PubConfig> configs;

    /**
     * 获取配置
     *
     * @param type 配置类型
     * @return List<PubConfig>
     */
    public List<PubConfig> configs(String type) {
        return configs(type, null);
    }

    /**
     * 获取配置
     *
     * @param type 类型
     * @param key  键
     * @return List<PubConfig>
     */
    public List<PubConfig> configs(String type, String key) {
        List<PubConfig> res = new ArrayList<>();

        if (null == this.configs) {
            return res;
        }

        for (PubConfig config : this.configs) {
            if (null == type) {
                return res;
            } else if (type.equals(config.getType())) {
                if (null == key || key.equals(config.getCfgKey())) {
                    res.add(config);
                }
            }
        }

        return res;
    }

    /**
     * 在知道配置只存在单个的情况下，强制返回第一条信息
     *
     * @param type 类别
     * @param key  键
     * @return PubConfig
     */
    public PubConfig configSingle(String type, String key) {
        return configSingle(type, key, null);
    }

    /**
     * 在知道配置只存在单个的情况下，强制返回第一条信息，如果不存在则返回指定的默认值
     *
     * @param type         类别
     * @param key          键
     * @param defaultValue 默认值
     * @return PubConfig
     */
    public PubConfig configSingle(String type, String key, String defaultValue) {
        List<PubConfig> configs = configs(type, key);
        return null == configs || configs.size() < 1
                ? PubConfig.builder().type(type).cfgKey(key).cfgValue(defaultValue).build()
                : configs.get(0);
    }

    /**
     * 转换为本地类
     *
     * @param data 数据
     * @param <T>  被转化的类型
     * @return List<PubConfig>
     */
    public static <T> List<PubConfig> convert(List<T> data) {
        List<PubConfig> res = new ArrayList<>();

        if (null != data && !data.isEmpty()) {
            data.forEach(t -> {
                PubConfig pubConfig = PubConfig.builder().build();
                BeanUtils.copyProperties(t, pubConfig);
                res.add(pubConfig);
            });
        }

        return res;
    }

    /**
     * 封装配置请求参数
     *
     * @param type 类型
     * @param key  键
     * @return List<CustomQueryParams>
     */
    public static List<CustomQueryParams> newParams(String type, String key) {
        List<CustomQueryParams> params = new ArrayList<>();

        if (StringUtils.isNotBlank(type)) {
            String[] typeRight = {type};
            params.add(CustomQueryParams.builder().left("type").op("eq").right(typeRight).build());
        }

        if (StringUtils.isNotBlank(key)) {
            String[] keyRight = {key};
            params.add(CustomQueryParams.builder().left("cfgKey").op("eq").right(keyRight).build());
        }

        return params;
    }
}
