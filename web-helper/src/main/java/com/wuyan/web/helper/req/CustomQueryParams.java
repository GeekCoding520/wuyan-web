package com.wuyan.web.helper.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * 自定义查询条件: 查询实体
 *
 * @author wuyan
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomQueryParams {

    /**
     * 查询分组名，可用于多条件的混合查询
     */
    private String team;

    /**
     * 操作数
     */
    private String op;

    /**
     * 字段名
     */
    private String left;

    /**
     * 参数
     */
    private Object[] right;

    /**
     * 检查
     *
     * @return boolean
     */
    public boolean check() {
        return StringUtils.isNotBlank(this.op) &&
                StringUtils.isNotBlank(this.left) &&
                this.right != null && this.right.length > 0;
    }

}
