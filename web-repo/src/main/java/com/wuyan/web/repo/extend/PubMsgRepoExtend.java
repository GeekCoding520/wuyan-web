package com.wuyan.web.repo.extend;

import com.wuyan.web.repo.PubMsgRepo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 扩展服务
 */

@Repository
public interface PubMsgRepoExtend extends PubMsgRepo {

    /**
     * 统计某个账户的未读消息量
     *
     * @param recvId 接收者ID
     * @return int
     */
    int countAllByRecvUserIdAndReadTimeIsNull(Integer recvId);

    /**
     * 更新消息为已读
     *
     * @param ids      主键
     * @param readTime 阅读时间
     * @return int
     */
    @Modifying
    @Query("update PubMsg set readTime = (:readTime) where id in (:ids) and readTime is null")
    int read(@Param("ids") List<Integer> ids, @Param("readTime") LocalDateTime readTime);


    @Modifying
    @Query("update PubMsg set readTime = (:readTime) where recvUserId = (:recvUserId) and readTime is null")
    int readAll(@Param("recvUserId") Integer recvUserId, @Param("readTime") LocalDateTime readTime);
}
