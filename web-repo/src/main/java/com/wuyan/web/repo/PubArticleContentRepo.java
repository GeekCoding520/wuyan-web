package com.wuyan.web.repo;

import com.wuyan.web.entity.PubArticleContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;


/**
 * repository for PubArticleContent generated by jpa-codegen
 * codegen auto
 *
 * @author wuyan
 * @date 2021-09-14 16:20:42
 */

@Repository
public interface PubArticleContentRepo extends JpaRepository<PubArticleContent, Integer>, QuerydslPredicateExecutor<PubArticleContent> {
    /**
    * 批量删除
    * @param ids 主键
    * @return int
    */
    @Modifying
    @Query("delete from PubArticleContent where id in (:ids)")
    int deleteAll(@Param("ids") List<Integer> ids);
}