package com.wuyan.web.repo.extend;


import com.wuyan.web.entity.PubAccount;
import com.wuyan.web.repo.PubAccountRepo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 扩展服务
 */

@Repository
public interface PubAccountRepoExtend extends PubAccountRepo {

    /**
     * 查找部门下的所有成员
     *
     * @param dept 部门ID
     * @return List<PubAccount>
     */
    List<PubAccount> findAllByDept(Integer dept);

    /**
     * 查找拥有指定角色的账户
     *
     * @param roleId 角色ID
     * @return List<PubAccount>
     */
    @Query(value = "select * from pub_account where find_in_set(:roleId, role)", nativeQuery = true)
    List<PubAccount> findAllByRoleContain(@Param("roleId") String roleId);

    /**
     * 查找拥有指定角色的正常状态的账户
     *
     * @param roleId 角色ID
     * @return List<PubAccount>
     */
    @Query(value = "select * from pub_account where find_in_set(:roleId, role) and status=1", nativeQuery = true)
    List<PubAccount> findAllByRoleContainAndStatusOK(@Param("roleId") String roleId);
}
