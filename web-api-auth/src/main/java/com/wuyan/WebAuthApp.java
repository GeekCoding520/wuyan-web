//package com.wuyan;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//import org.springframework.cloud.openfeign.EnableFeignClients;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//
///**
// * 入口
// *
// * @author wuyan
// */
//
//@SpringBootApplication
//@Slf4j
//@EnableFeignClients
//@EnableAspectJAutoProxy
//public class WebAuthApp extends SpringBootServletInitializer{
//    public static void main(String[] args) {
//        Thread thread = Thread.currentThread();
//        thread.setName("主入口");
//        SpringApplication.run(WebAuthApp.class, args);
//    }
//
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        // 错误页面有容器来处理，而不是SpringBoot
//        this.setRegisterErrorPageFilter(false);
//        return builder.sources(WebAuthApp.class);
//    }
//}
