package com.wuyan.web.auth.service.api;

import com.wuyan.web.auth.helper.wx.AccessToken;
import com.wuyan.web.auth.helper.wx.Userinfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 微信接口服务-微信授权相关
 */

@FeignClient(url = "https://api.weixin.qq.com", name = "wx-api-auth")
public interface WxAuthApiService {
    /**
     * 通过code换取网页授权access_token
     *
     * @param appid      公众号的唯一标识
     * @param secret     公众号的appsecret
     * @param code       填写第一步获取的code参数
     * @param grant_type 填写为authorization_code
     * @return AccessToken
     */
    @GetMapping("/sns/oauth2/access_token")
    AccessToken getAccessToken(@RequestParam(value = "appid") String appid,
                                       @RequestParam(value = "secret") String secret,
                                       @RequestParam(value = "code") String code,
                                       @RequestParam(value = "grant_type", required = false, defaultValue = "authorization_code") String grant_type);

    /**
     * 通过code换取网页授权access_token
     *
     * @param appid         公众号的唯一标识
     * @param refresh_token 填写通过access_token获取到的refresh_token参数
     * @param grant_type    填写为refresh_token
     * @return AccessToken
     */
    @GetMapping("/sns/oauth2/refresh_token")
    AccessToken getRefreshToken(@RequestParam(value = "appid") String appid,
                                @RequestParam(value = "refresh_token") String refresh_token,
                                @RequestParam(value = "grant_type", required = false, defaultValue = "refresh_token") String grant_type);

    /**
     * 检验授权凭证（access_token）是否有效
     *
     * @param access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param openid       用户的唯一标识
     * @return AccessToken
     */
    @GetMapping("/sns/auth")
    AccessToken authAccessToken(@RequestParam(value = "access_token") String access_token,
                            @RequestParam(value = "openid") String openid);

    /**
     * 拉取用户信息(需scope为 snsapi_userinfo)
     *
     * @param access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param openid       用户的唯一标识
     * @param lang         返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @return Userinfo
     */
    @GetMapping("/sns/userinfo")
    Userinfo getUserinfo(@RequestParam(value = "access_token") String access_token,
                         @RequestParam(value = "openid") String openid,
                         @RequestParam(value = "lang", required = false, defaultValue = "zh_CN") String lang);

}
