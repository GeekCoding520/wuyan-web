package com.wuyan.web.auth.api;

import com.wuyan.web.auth.helper.AuthHelper;
import com.wuyan.web.auth.helper.AuthRequestMapping;
import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.helper.rep.RepBody;
import com.wuyan.web.helper.rep.RepCodeEnum;
import com.wuyan.web.helper.rep.RepHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 鉴权接口
 */

@Slf4j
@RestController
@AuthRequestMapping("/api")
public class CheckAuthApi implements RepHelper {

    @Autowired
    private AuthHelper authHelper;

    /**
     * 获取当前登录信息，并且校验登录状态
     *
     * @param token 临时授权码
     * @return RepBody<LoginInfo>
     */
    @GetMapping("/check/{token}")
    public RepBody<LoginInfo> check(@PathVariable(value = "token") String token, HttpSession session) throws IOException {
        LoginInfo loginInfo = authHelper.get(session, token);
        if (null == loginInfo) {
            return error(RepCodeEnum.ERR_LOGIN_EXPIRE);
        } else {
            return ok(loginInfo);
        }
    }
}
