package com.wuyan.web.auth.repo.extend;

import com.wuyan.web.entity.PubAccount;
import com.wuyan.web.repo.PubAccountRepo;

/**
 * 扩展
 */

public interface PubAccountExtendRepo extends PubAccountRepo {

    /**
     * 根据用户名获取指定用户
     *
     * @param username 用户名
     * @return PubAccount
     */
    PubAccount findFirstByUsernameOrPhone(String username, String phone);

    PubAccount findFirstByUsername(String username);
}
