package com.wuyan.web.auth.repo.extend;

import com.wuyan.web.entity.PubRoleFunction;
import com.wuyan.web.repo.PubRoleFunctionRepo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 扩展
 */

@Repository
public interface PubRoleFunctionRepoExtend extends PubRoleFunctionRepo {

    /**
     * @param ids 角色ID组
     * @return List<PubRoleFunctionForm>
     */
    @Query("from PubRoleFunction where roleId in (:ids)")
    List<PubRoleFunction> findAllByRole(@Param("ids") List<Integer> ids);

    /**
     * 批量删除: 根据角色ID
     *
     * @param ids 角色ID组
     * @return int
     */
    @Modifying
    @Query("delete from PubRoleFunction where roleId in (:ids)")
    int deleteAllByRole(@Param("ids") List<Integer> ids);
}
