package com.wuyan.web.auth.form;

import com.wuyan.web.form.PubRoleApiForm;
import com.wuyan.web.form.PubRoleFunctionForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 角色权限授权信息
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleAuthForm {
    /**
     * 功能区
     */
    private List<PubRoleFunctionForm> functions;

    /**
     * 接口区
     */
    private List<PubRoleApiForm> apis;
}
