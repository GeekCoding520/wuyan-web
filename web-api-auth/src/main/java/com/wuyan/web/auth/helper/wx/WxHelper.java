package com.wuyan.web.auth.helper.wx;

import com.wuyan.helper.kit.endecryption.EncryptionHelper;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

/**
 * 微信配置工具类
 */

@Component
@Data
public class WxHelper {
    /**
     * 刷新间隔
     */
    private static long refreshInterval = 90 * 60 * 1000L;

    /**
     * 最后一次刷新的时间
     */
    private LocalDateTime operateTime;

    /**
     * 公众号appId
     */
    private String appId;

    /**
     * 公众号秘钥
     */
    private String secret;

    private AccessToken accessToken;

    /**
     * 创建JSAPI签名信息
     *
     * @param noncestr    随机字符串
     * @param timestamp   时间戳： 秒
     * @param url         请求路径
     * @param jsapiTicket 临时票据
     * @return String
     */
    public String signJSAPI(String noncestr, String timestamp, String url, String jsapiTicket) throws NoSuchAlgorithmException {
        String string = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
        return EncryptionHelper.getInstance().sha1(string);
    }

//    /**
//     * SHA1签名
//     *
//     * @param decript 待签名信息
//     * @return String
//     */
//    private String sha1(String decript) {
//        try {
//            MessageDigest digest = MessageDigest.getInstance("SHA-1");
//            digest.update(decript.getBytes());
//            byte[] messageDigest = digest.digest();
//            // Create Hex String
//            StringBuilder hexString = new StringBuilder();
//            // 字节数组转换为 十六进制 数
//            for (int i = 0; i < messageDigest.length; i++) {
//                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
//                if (shaHex.length() < 2) {
//                    hexString.append(0);
//                }
//                hexString.append(shaHex);
//            }
//            return hexString.toString();
//
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return "";
//    }
}
