package com.wuyan.web.auth.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wuyan.helper.kit.RandomHelper;
import com.wuyan.web.auth.repo.extend.PubApiRepoExtend;
import com.wuyan.web.auth.repo.extend.PubFunctionRepoExtend;
import com.wuyan.web.auth.repo.extend.PubRoleApiRepoExtend;
import com.wuyan.web.auth.repo.extend.PubRoleRepoExtend;
import com.wuyan.web.entity.*;
import com.wuyan.web.helper.SpringRedisHelper;
import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.repo.PubAccountRepo;
import com.wuyan.web.repo.PubDeptRepo;
import com.wuyan.web.repo.PubPostRepo;
import com.wuyan.web.repo.PubUserRepo;
import com.wuyan.web.repo.extend.PubAccountRepoExtend;
import com.wuyan.web.repo.extend.PubUserRepoExtend;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * 授权信息相关工具
 *
 * @author wuyan
 */

@Component
@Slf4j
public class AuthHelper {
    @Autowired
    private ObjectMapper mapper;

    private static final String SESSION_AUTH_LOGIN_NAME = "auth-login";

    /**
     * 存放于响应头部的token名称
     */
    public static final String HEADER_AUTH_NAME = "authorization";

    @Autowired
    private SpringRedisHelper springRedisHelper;

    @Autowired
    private PubRoleApiRepoExtend pubRoleApiRepoExtend;

    @Autowired
    private PubRoleRepoExtend pubRoleRepoExtend;

    @Autowired
    private PubApiRepoExtend pubApiRepoExtend;

    @Autowired
    private PubFunctionRepoExtend pubFunctionRepoExtend;

    @Autowired
    private PubDeptRepo pubDeptRepo;

    @Autowired
    private PubPostRepo pubPostRepo;

    @Autowired
    private PubAccountRepoExtend pubAccountRepo;

    @Autowired
    private PubUserRepoExtend pubUserRepo;

    /**
     * 保存登录信息, 并生成token
     *
     * @param request        请求体
     * @param pubAccount     账户信息
     * @param pubUser        用户信息
     * @param tokenExpiresIn token有效时间
     * @return LoginInfo
     */
    public LoginInfo save(HttpServletRequest request,
                          HttpServletResponse response,
                          PubAccount pubAccount,
                          PubUser pubUser,
                          long tokenExpiresIn) throws JsonProcessingException {
        HttpSession session = request.getSession();
        String accessToken = SESSION_AUTH_LOGIN_NAME + "-" + RandomHelper.createAccessToken();

        List<PubRole> roles = getRole(pubAccount);
        LoginInfo loginInfo = LoginInfo.builder()
                .token(accessToken)
                .expiresIn(tokenExpiresIn)
                .account(pubAccount)
                .user(pubUser)
                .dept(null == pubAccount.getDept() ? null : getDept(pubAccount))
                .posts(StringUtils.isBlank(pubAccount.getPost()) ? null : getPosts(pubAccount))
                .roles(roles)
                .apis(getApi(roles))
                .menus(getMenus(roles))
                .loginTime(LocalDateTime.now())
                .refreshTime(LocalDateTime.now())
                .build();

        saveToSession(session, loginInfo);
        saveToCache(loginInfo);

        response.setHeader(HEADER_AUTH_NAME, accessToken);

        return loginInfo;
    }

    /**
     * 获取部门信息
     *
     * @param pubAccount 账户信息
     * @return PubDept
     */
    public PubDept getDept(PubAccount pubAccount) {
        return pubDeptRepo.findById(pubAccount.getDept()).orElse(null);
    }

    /**
     * 获取职位信息
     *
     * @param pubAccount 账户信息
     * @return List<PubPost>
     */
    public List<PubPost> getPosts(PubAccount pubAccount) {
        return pubPostRepo.findAllById(
                Arrays.stream(pubAccount.getPost().split(",")).map(Integer::parseInt).collect(Collectors.toSet())
        );
    }

    @SneakyThrows
    public boolean update(HttpSession session, LoginInfo loginInfo) {
        saveToSession(session, loginInfo);
        saveToCache(loginInfo);
        return true;
    }

    /**
     * 获取账户角色,已经启用的
     *
     * @param pubAccount 账户信息
     * @return List<PubAccountRole>
     */
    public List<PubRole> getRole(PubAccount pubAccount) {
        String roleIdStr = pubAccount.getRole();
        if (StringUtils.isBlank(roleIdStr)) {
            return new ArrayList<>();
        }

        String[] roleIds = roleIdStr.split(",");

        return pubRoleRepoExtend.findAllByIdInAndStatus(Arrays.stream(roleIds)
                .map(Integer::parseInt)
                .collect(toList()), 1);
    }

    /**
     * 获取角色下的已授权接口信息
     *
     * @param roles 角色组
     * @return List<PubRoleApi>
     */
    public List<PubApi> getApi(List<PubRole> roles) {
        List<Integer> ids = roles.stream().map(PubRole::getId).collect(toList());
        List<PubRoleApi> pubRoleApis =
                pubRoleApiRepoExtend.findAllByRole(ids);

        if (null == pubRoleApis || pubRoleApis.isEmpty()) {
            return new ArrayList<>();
        }

        return pubApiRepoExtend.findAllByIdInAndStatus(
                pubRoleApis.stream()
                        .map(PubRoleApi::getRoleId)
                        .collect(toList()), 1);
    }

    /**
     * 获取角色下的已授权菜单
     *
     * @param roles 角色组
     * @return List<PubRoleFunction>
     */
    public List<PubFunction> getMenus(List<PubRole> roles) {
        final String[] menuIdStrArray = {""};
        roles.forEach(t -> {
            if (StringUtils.isNotBlank(t.getMenuIds())) {
                menuIdStrArray[0] = menuIdStrArray[0] + "," + t.getMenuIds();
            }
        });

        if (StringUtils.isBlank(menuIdStrArray[0])) {
            return new ArrayList<>();
        }

        String[] menuIdStr = menuIdStrArray[0].split(",");

        List<PubFunction> menus =
                pubFunctionRepoExtend.findAllByIdInAndStatus(Arrays.stream(menuIdStr)
                        .filter(t -> StringUtils.isNotBlank(t) && StringUtils.isNumericSpace(t))
                        .map(Integer::parseInt)
                        .collect(Collectors.toSet()), 1);

        return menus;
    }

    /**
     * 获取登录信息
     *
     * @param session 会话
     * @param token   临时凭据
     * @return LoginInfo
     */
    public LoginInfo get(HttpSession session, String token) throws IOException {
        LoginInfo loginInfo = getBySession(session);
        if (null == loginInfo) {
            loginInfo = getByCache(token);

            if (null != loginInfo) {
                return expire(session, token, loginInfo);
            }
        } else {
            return expire(session, token, loginInfo);
        }

        return null;
    }

    /**
     * 保存登录信息到缓存器
     *
     * @param loginInfo 登录信息
     */
    private void saveToCache(LoginInfo loginInfo) throws JsonProcessingException {
        springRedisHelper.set(loginInfo.getToken(), mapper.writeValueAsString(loginInfo), loginInfo.getExpiresIn());
    }

    /**
     * 保存登录信息到session
     *
     * @param session   会话
     * @param loginInfo 登录信息
     */
    private void saveToSession(HttpSession session, LoginInfo loginInfo) throws JsonProcessingException {
        if(null == session) return;

        session.setAttribute(SESSION_AUTH_LOGIN_NAME, mapper.writeValueAsString(loginInfo));
    }

    /**
     * 从session中获取登录信息
     *
     * @param session 会话
     * @return LoginInfo
     */
    private LoginInfo getBySession(HttpSession session) throws IOException {
        if(null == session) return null;

        String res = (String) session.getAttribute(SESSION_AUTH_LOGIN_NAME);

        if (null == res) {
            return null;
        }

        return mapper.readValue(res.getBytes(), LoginInfo.class);
    }

    /**
     * 从缓存中获取登录信息
     *
     * @param token 临时凭据
     * @return LoginInfo
     */
    private LoginInfo getByCache(String token) throws IOException {
        String res = springRedisHelper.get(token, String.class);

        if (StringUtils.isBlank(res)) {
            return null;
        }

        return mapper.readValue(res.getBytes(), LoginInfo.class);
    }

    /**
     * 清除登录信息
     *
     * @param session 会话
     * @param token   临时授权麻
     */
    public void remove(HttpSession session, String token) {
        if(null == session) return;

        session.removeAttribute(SESSION_AUTH_LOGIN_NAME);
        springRedisHelper.remove(token);
    }

    /**
     * 判断登录是否过期，如果没有，并且即将到过期时间，则刷新过期时间
     *
     * @param session   会话
     * @param token     验证标识符
     * @param loginInfo 账户信息
     * @return LoginInfo
     */
    private LoginInfo expire(HttpSession session, String token, LoginInfo loginInfo) throws IOException {
        if (null == loginInfo) {
            return null;
        }

        long refreshTime = loginInfo.getRefreshTime().toEpochSecond(ZoneOffset.of("+8"));
        long nowTime = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
        long exMaxTime = refreshTime + loginInfo.getExpiresIn();

        // 已经过了有效期
        if (exMaxTime <= nowTime) {
            remove(session, token);
            return null;
        }

        // 刷新用户数据，暂时写死，单位秒
        if (nowTime - refreshTime > 60) {
            return updateLoginInfo(session, loginInfo);
        } else {
            return loginInfo;
        }
    }

    /**
     * 刷新用户数据
     *
     * @param session   会话
     * @param loginInfo 当前数据
     * @return LoginInfo
     * @throws JsonProcessingException e
     */
    public LoginInfo updateLoginInfo(HttpSession session,
                                     LoginInfo loginInfo) throws JsonProcessingException {
        if(loginInfo.getAccount().getId() == null){
            return loginInfo;
        }

        // 取用户数据
        PubAccount account = pubAccountRepo.findById(loginInfo.getAccount().getId()).orElse(null);

        PubUser user;
        if (loginInfo.getUser() == null || loginInfo.getUser().getId() == null) {
            user = pubUserRepo.findFirstByUserId(loginInfo.getAccount().getId());
        } else {
            user = pubUserRepo.findById(loginInfo.getUser().getId()).orElse(null);
        }

        assert account != null;
        List<PubRole> roles = getRole(account);
        loginInfo = LoginInfo.builder()
                .token(loginInfo.getToken())
                .expiresIn(loginInfo.getExpiresIn())
                .account(account)
                .user(user)
                .dept(null == account.getDept() ? null : getDept(account))
                .posts(StringUtils.isBlank(account.getPost()) ? null : getPosts(account))
                .roles(roles)
                .apis(getApi(roles))
                .menus(getMenus(roles))
                .loginTime(LocalDateTime.now())
                .refreshTime(LocalDateTime.now())
                .build();

        saveToSession(session, loginInfo);
        saveToCache(loginInfo);

        return loginInfo;
    }
}
