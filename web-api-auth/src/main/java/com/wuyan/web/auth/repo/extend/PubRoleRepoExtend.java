package com.wuyan.web.auth.repo.extend;

import com.wuyan.web.entity.PubRole;
import com.wuyan.web.repo.PubRoleRepo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PubRoleRepoExtend extends PubRoleRepo {
    List<PubRole> findAllByIdInAndStatus(List<Integer> ids, Integer status);
}