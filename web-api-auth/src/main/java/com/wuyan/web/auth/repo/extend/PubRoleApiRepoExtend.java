package com.wuyan.web.auth.repo.extend;

import com.wuyan.web.entity.PubRoleApi;
import com.wuyan.web.repo.PubRoleApiRepo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 扩展数据
 */

@Repository
public interface PubRoleApiRepoExtend extends PubRoleApiRepo {

    /**
     * @param ids 角色ID组
     * @return List<PubRoleApi>
     */
    @Query("from PubRoleApi where roleId in (:ids)")
    List<PubRoleApi> findAllByRole(@Param("ids") List<Integer> ids);

    /**
     * 批量删除: 根据角色ID
     *
     * @param ids 角色ID组
     * @return int
     */
    @Modifying
    @Query("delete from PubRoleApi where roleId in (:ids)")
    int deleteAllByRole(@Param("ids") List<Integer> ids);
}
