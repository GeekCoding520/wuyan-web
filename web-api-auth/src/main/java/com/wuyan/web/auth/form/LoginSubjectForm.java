package com.wuyan.web.auth.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 登录主体
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginSubjectForm {
    /**
     * 登录ID
     */
    @NotBlank(message = "登录ID不能为空")
    private String key;

    /**
     * 秘钥
     */
    @NotBlank(message = "登录秘钥不能为空")
    private String pwd;

    /**
     * 验证码
     */
    private String code;
}
