package com.wuyan.web.auth.api;

import com.wuyan.helper.kit.endecryption.EncryptionHelper;
import com.wuyan.web.aop.ApiLogAnnotation;
import com.wuyan.web.auth.form.LoginSubjectForm;
import com.wuyan.web.auth.helper.AuthRequestMapping;
import com.wuyan.web.auth.repo.extend.PubAccountExtendRepo;
import com.wuyan.web.auth.service.LoginService;
import com.wuyan.web.entity.PubAccount;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.DataEncryptionHelper;
import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.helper.rep.RepBody;
import com.wuyan.web.helper.rep.RepCodeEnum;
import com.wuyan.web.helper.rep.RepHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Map;

import static com.wuyan.web.config.EntityCfg.DATA_ENCRYPTION;

/**
 * 登录
 */

@Slf4j
@RestController
@AuthRequestMapping("/api/login")
public class LoginApi extends BaseApi implements RepHelper {
    @Autowired
    private PubAccountExtendRepo pubAccountExtendRepo;

    @Autowired
    private LoginService loginService;

    @Value("${wuyan.login.verify-code}")
    private boolean verifyCode;

    /**
     * 账户密码登录
     *
     * @param request 请求体
     * @param form    参数
     * @return RepBody<LoginInfo>
     */
    @PostMapping
    @ApiLogAnnotation(name = "账户密码登录", identity = "login:loginByAccount")
    public RepBody<LoginInfo> loginByAccount(HttpServletRequest request, HttpServletResponse response,
                                             @RequestBody @Validated LoginSubjectForm form)
            throws Exception {
        /*
            验证码校验
         */
        if (verifyCode) {
            Enumeration<String> headerNames = request.getHeaderNames();
            String codeSecret = request.getHeader(VerifyCodeApi.HEADER_AUTH_KEY_NAME);
            if (StringUtils.isBlank(codeSecret)) {
                codeSecret = request.getHeader(VerifyCodeApi.HEADER_AUTH_KEY_NAME2);
            }
            if (StringUtils.isBlank(codeSecret)) {
                return error(RepCodeEnum.ERR_LOGIN_VERIFY_AUTH);
            }

            String code = getAttr(request, codeSecret, String.class, true);
            if (StringUtils.isBlank(code)) {
                return error(RepCodeEnum.ERR_LOGIN_VERIFY_INVALID);
            }

            if (!code.equals(form.getCode().toUpperCase())) {
                return error(RepCodeEnum.ERR_LOGIN_VERIFY);
            }
        }

        /*
            账户校验
         */
        PubAccount pubAccount = pubAccountExtendRepo
                .findFirstByUsernameOrPhone(form.getKey(), form.getKey());
        if (null == pubAccount) {
            return error(RepCodeEnum.ERR_LOGIN_USERNAME);
        }

        if (pubAccount.getStatus() != 1) {
            return error(RepCodeEnum.ERR_LOGIN_STATUS);
        }

        if (!pubAccount.getPassword().equals(EncryptionHelper.getInstance().md5(form.getPwd()))) {
            return error(RepCodeEnum.ERR_LOGIN_PASSWORD);
        }

        LoginInfo loginInfo = loginService.saveLoginInfo(request, response, pubAccount);
        return ok(loginInfo);
    }

    /**
     * 重置密码
     *
     * @param request 请求体
     * @param form    数据
     * @return RepBody<Boolean>
     */
    @PostMapping("/reset-pwd")
    @ApiLogAnnotation(name = "重置密码", identity = "login:resetPwd")
    public RepBody<Boolean> resetPwd(HttpServletRequest request,
                                     @RequestBody Map<String, Object> form) throws Exception {
        String ak = form.containsKey("ak") ? form.get("ak").toString() : "";
        String code = getAttr(request, "resetPwd-" + ak, String.class, true);

        if (null == code) {
            return error(RepCodeEnum.ERR_LOGIN_VERIFY_INVALID);
        }
        if (!code.equals(form.get("code").toString().toUpperCase())) {
            return error(RepCodeEnum.ERR_LOGIN_VERIFY);
        }

        String username = form.containsKey("key") ? form.get("key").toString() : "";
        String password = form.containsKey("pwd") ? form.get("pwd").toString() : "";

        PubAccount pubAccount = pubAccountExtendRepo.findFirstByUsername(username);
        if (null == pubAccount) {
            return error(RepCodeEnum.ERR_LOGIN_USERNAME);
        }

        // 需要加密
        pubAccount.setPassword(password);
        DataEncryptionHelper.encode("pubAccount", pubAccount, DATA_ENCRYPTION);
        pubAccountExtendRepo.save(pubAccount);

        return ok(true);
    }

    /**
     * 登出
     *
     * @param request 请求体
     * @param key     授权码
     * @return RepBody<Boolean>
     */
    @GetMapping("/login-out/{key}")
    @ApiLogAnnotation(name = "登出", identity = "login:out")
    public RepBody<Boolean> out(HttpServletRequest request,
                                @PathVariable("key") String key) {
        if (StringUtils.isBlank(key)) {
            return error(-1, "无效的key");
        }
        loginService.removeLoginInfo(request.getSession(), key);
        return ok(true);
    }
}
