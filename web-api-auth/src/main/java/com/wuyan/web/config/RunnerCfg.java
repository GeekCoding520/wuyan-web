package com.wuyan.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 启动配置
 *
 * @author wuyan
 */

@Slf4j
@Component
public class RunnerCfg implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

    }
}
