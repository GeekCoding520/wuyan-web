package com.wuyan.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 空配置
 * 为了解决需要被扫描的包不在默认扫描范围内问题
 */

@Configuration
@ComponentScan(basePackages = {"com.wuyan"})
public class OtherJarCfg {

}
