package com.wuyan.web.config;

import com.wuyan.web.auth.helper.AuthRequestMapping;
import com.wuyan.web.helper.WuyanWebProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 配置统一接口访问路径的前缀
 *
 * @author wuyan
 */
@Configuration
public class AuthRequestMappingCfg implements WebMvcConfigurer {

    @Resource
    private WuyanWebProperties wuyanWebProperties;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer
                .addPathPrefix(wuyanWebProperties.getWeb().getAuthPrefix(), c -> c.isAnnotationPresent(AuthRequestMapping.class));
    }
}
