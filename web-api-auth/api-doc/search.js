let api = [];
api.push({
    alias: 'api',
    order: '1',
    desc: '鉴权接口',
    link: '鉴权接口',
    list: []
})
api[0].list.push({
    order: '1',
    desc: '获取当前登录信息，并且校验登录状态',
});
api.push({
    alias: 'IdentityApi',
    order: '2',
    desc: '授权相关',
    link: '授权相关',
    list: []
})
api[1].list.push({
    order: '1',
    desc: '对角色进行重新授权分配',
});
api[1].list.push({
    order: '2',
    desc: '获取指定角色的已授权功能概要',
});
api[1].list.push({
    order: '3',
    desc: '对用户账户的角色进行重新分配',
});
api[1].list.push({
    order: '4',
    desc: '临时授权：此操作会删除当前临时授权中相关的已授权角色记录',
});
api[1].list.push({
    order: '5',
    desc: '修改密码',
});
api[1].list.push({
    order: '6',
    desc: '获取当前登录账户下所有的已授权菜单',
});
api[1].list.push({
    order: '7',
    desc: '新增角色',
});
api.push({
    alias: 'LoginApi',
    order: '3',
    desc: '',
    link: '',
    list: []
})
api[2].list.push({
    order: '1',
    desc: '账户密码登录',
});
api[2].list.push({
    order: '2',
    desc: '重置密码',
});
api[2].list.push({
    order: '3',
    desc: '登出',
});
api.push({
    alias: 'RegisterApi',
    order: '4',
    desc: '',
    link: '',
    list: []
})
api[3].list.push({
    order: '1',
    desc: '注册账户',
});
api.push({
    alias: 'VerifyCodeApi',
    order: '5',
    desc: '验证码接口',
    link: '验证码接口',
    list: []
})
api[4].list.push({
    order: '1',
    desc: '创建图片验证码信息，并返回二进制流',
});
api[4].list.push({
    order: '2',
    desc: '将返回base64编码后的内容，常用于无法从header中取出授权信息的方式',
});
api[4].list.push({
    order: '3',
    desc: '发送注册验证码',
});
api[4].list.push({
    order: '4',
    desc: '发送重置密码服务的验证码',
});
api.push({
    alias: 'WxAuthApi',
    order: '6',
    desc: '微信登录授权接口',
    link: '微信登录授权接口',
    list: []
})
api[5].list.push({
    order: '1',
    desc: '微信web登陆，基于code',
});
api.push({
    alias: 'ErrorPathCfg',
    order: '7',
    desc: '请求不存在的处理',
    link: '请求不存在的处理',
    list: []
})
api[6].list.push({
    order: '1',
    desc: '',
});
api.push({
    alias: 'dict',
    order: '8',
    desc: '数据字典',
    link: 'dict_list',
    list: []
})
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        const search = document.getElementById('search');
        const searchValue = search.value;
        let searchArr = [];
        for (let i = 0; i < api.length; i++) {
            let apiData = api[i];
            const desc = apiData.desc;
            if (desc.indexOf(searchValue) > -1) {
                searchArr.push({
                    order: apiData.order,
                    desc: apiData.desc,
                    link: apiData.link,
                    alias: apiData.alias,
                    list: apiData.list
                });
            } else {
                let methodList = apiData.list || [];
                let methodListTemp = [];
                for (let j = 0; j < methodList.length; j++) {
                    const methodData = methodList[j];
                    const methodDesc = methodData.desc;
                    if (methodDesc.indexOf(searchValue) > -1) {
                        methodListTemp.push(methodData);
                        break;
                    }
                }
                if (methodListTemp.length > 0) {
                    const data = {
                        order: apiData.order,
                        desc: apiData.desc,
                        alias: apiData.alias,
                        link: apiData.link,
                        list: methodListTemp
                    };
                    searchArr.push(data);
                }
            }
        }
        let html;
        if (searchValue == '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchArr,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiData, liClass, display) {
    let html = "";
    let doc;
    if (apiData.length > 0) {
         for (let j = 0; j < apiData.length; j++) {
            html += '<li class="'+liClass+'">';
            html += '<a class="dd" href="' + apiData[j].alias + '.html#header">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
            html += '<ul class="sectlevel2" style="'+display+'">';
            doc = apiData[j].list;
            for (let m = 0; m < doc.length; m++) {
                html += '<li><a href="' + apiData[j].alias + '.html#_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + doc[m].desc + '</a> </li>';
            }
            html += '</ul>';
            html += '</li>';
        }
    }
    return html;
}