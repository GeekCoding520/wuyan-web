module.exports = {
	devServer: {
		proxy: {
			'/api-bmap': {
				target: 'https://api.map.baidu.com/',
				changOrigin: true,
				pathRewrite: {
					'^/api-bmap': ''
				}
			}
		}
	}
}
