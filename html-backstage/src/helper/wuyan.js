/**
 * 讲Arrays类型的查询参数转换为json字符串,并且过滤掉其中不必要的查询条件
 */
const queryParamsToJsonStr = (params) => {
	let ps = []

	params.forEach(t => {
		// 强制保留标志
		if (!t.force) {
			if (t && t.right && t.right.length === 1 && (t.right[0] == undefined || t.right[0] === '')) {
				return
			}

			if (t && t.right && t.right.length === 2) {
				if ((t.right[0] == undefined || t.right[0] === '') &&
					(t.right[1] == undefined || t.right[1] === '')) {
					return
				}
			}
		}

		ps.push({
			left: t.left,
			op: t.op,
			right: t.right
		})
	})

	return JSON.stringify(ps)
}

/**
 * list转menu或者children类型
 * @param {Array} list 数据集合
 * @param {String} parentName 父级标识名称
 * @param {String} idName 数据行唯一标识名称
 * @param {Boolean} depth 是否调用深度遍历，默认只生成2层
 */
const listToMenu = (list, {
	parentName = 'parentId',
	childName = "children",
	idName = 'id',
	depth = false,
	sortName = 'cfgSort'
} = {}) => {
	let trees = []

	if (list && list.length < 1) return trees

	let childrens = []
	list.forEach(t => {
		if (!t[sortName]) t[sortName] = 0

		if (t.parentId == 0 || t.parentId == t.id) {
			t[childName] = []
			trees.push(t)
		} else {
			childrens.push(t)
		}
	})

	// 排序
	trees.sort((a, b) => b[sortName] - a[sortName]);

	// 找出子节点
	if (childrens && childrens.length < 1) return trees

	childrens.forEach(t => {
		trees.forEach(c => {
			if (c[idName] == t[parentName]) {
				t['parentName'] = c.name
				c[childName].push(t)
			}
		})
	})

	// 子节点排序
	trees.forEach(t => {
		t[childName].sort((a, b) => b[sortName] - a[sortName])
	})

	return trees
}

/**
 * 数字转树形
 */
const listToTree = (list, {
	parentName = 'parentId',
	childName = "children",
	idName = 'id',
	depth = false,
	sortName = 'cfgSort'
} = {}) => {
	let result = []
	if (!Array.isArray(list)) {
		return result
	}

	// 剔除已有childName
	// ID记录（key，value）
	let map = {};
	list.forEach(item => {
		delete item[childName]
		map[item[idName]] = item
	})

	// 依次将子节点放入父节点中
	list.forEach(item => {
		// 找到父节点
		let parent = map[item[parentName]]

		if (parent) {
			(parent.children || (parent.children = [])).push(item)
		} else {
			result.push(item)
		}
	})

	return result
}

/**
 * 树转数组
 */
const treeToList = (treeData, childName = 'children') => {
	let tree = JSON.parse(JSON.stringify(treeData))
	let list = []

	if (tree instanceof Array) {
		tree.forEach(node => {
			treeToListNode(node, childName, list)
		})
	} else {
		treeToListNode(tree, childName, list)
	}

	return list
}

/**
 * 树节点的递归,转数组
 */
function treeToListNode(node, childName = 'children', list = []) {
	let child = node[childName] || []

	delete node[childName]
	list.push(node)

	child.forEach(item => {
		treeToListNode(item, childName, list)
	})
}

/**
 * 获取树中指定节点
 */
const getTreeNode = (treeData, key, idName = "id", childName = 'children') => {
	if (!key) return treeData

	let tree = JSON.parse(JSON.stringify(treeData))
	let res = {
		node: null
	}

	if (tree instanceof Array) {
		tree.forEach(node => {
			getTreeNodeDep(node, key, res, idName, childName)
		})
	} else {
		return getTreeNodeDep(tree, key, res, idName, childName)
	}

	return res.node
}

/**
 * 递归获取树中指定节点
 */
function getTreeNodeDep(node, key, res, idName = "id", childName = 'children') {
	if (node[idName] == key) res.node = node

	let child = node[childName] || []
	child.forEach(item => {
		getTreeNodeDep(item, key, res, idName, childName)
	})
}

/**
 * 将apis加入到对应的menus中,目前menu只支持两层
 */
const menuListAndApi = (menus, apis, apiMenuName = 'functionId') => {
	let roots = {}
	let nextNodes = {}

	menus.forEach(menu => {
		roots[menu.id] = menu

		if (menu.children && menu.children.length > 0) {
			menu.children.forEach(c => {
				nextNodes[c.id] = c
			})
		}
	})

	// 将api分别纳入对应的menu
	apis.forEach(api => {
		let apiMenuId = api[apiMenuName]

		// 根
		if (roots[apiMenuId]) {
			let menu = roots[apiMenuId]
			api['parentId'] = menu.id

			if (menu.children instanceof Array) {
				menu.children.push(api)
				return
			} else {
				menu.children = [api]
			}
		}

		// 下级
		if (nextNodes[apiMenuId]) {
			let menu = nextNodes[apiMenuId]
			api['parentId'] = menu.id

			if (menu.children instanceof Array) {
				menu.children.push(api)
				return
			} else {
				menu.children = [api]
			}
		}
	})

	return menus
}

/**
 * 解析参数
 * @param {Object} source 原文本-URL
 * @param {Object} params 参数
 */
const analysisText = (source, params) => {
	if (!params || !source) return source

	// 批量操作
	if (params instanceof Array) {
		if (params.length < 1) return source

		let pt = '{id}'
		let ids = params[0].id ? params[0].id : params[0]

		for (let index = 1; index < params.length; index++) {
			ids = ids + ',' + (params[index].id ? params[index].id : params[index])
		}

		source = source.replace(new RegExp(pt, "gm"), ids);
	}
	// 单个
	else {
		for (let key in params) {
			let pt = '{' + key + '}'
			source = source.replace(new RegExp(pt, "gm"), params[key]);
		}
	}

	return source
}

/**
 * 获取完整的请求路径
 * @param one 一级，一般代表大类
 * @param two 二级，一般代表具体的操作
 */
const getUrl = (that = this, one, two, params) => {
	let apiInfo = that.$apiInfo

	if (!one || !two) return ""

	let type = apiInfo[one]
	if (!type) return

	let op = type[two]
	if (!op) return

	let url = '/' + op['modular'] + op['url']
	return params ? analysisText(url, params) : url
}

/**
 * 根据method返回不同的请求实例
 */
const getHttpByMethod = (axios, method) => {
	if ('get' == method) return axios.get
	if ('post' == method) return axios.post
	if ('delete' == method) {
		return axios['delete']
	}
	if ('put' == method) return axios.put
}

/**
 * 根据不同的method返回不同的请求实例
 * @param one 一级，一般代表大类
 * @param two 二级，一般代表具体的操作
 */
const getHttp = (that = this, one, two) => {
	let apiInfo = that.$apiInfo

	if (!one || !two) return ""

	let type = apiInfo[one]
	if (!type) return

	let op = type[two]
	if (!op) return

	return getHttpByMethod(that.$axios, op.method)
}

/**
 * 格式化数据,将b的内容赋值到a.只保留a中存在的属性
 */
const fmtAToB = (a, b) => {
	for (let key in a) {
		a[key] = b[key]
	}

	return a
}

/**
 * 获取当前时间
 */
const getNowDate = (da) => {
	if (!da) da = new Date();

	const Y = da.getFullYear(); // 年
	const M = da.getMonth() + 1; // 月
	const D = da.getDate(); //日
	const h = da.getHours(); //小时
	const m = da.getMinutes(); //分
	const s = da.getSeconds(); //秒

	return Y + "-" +
		(M < 10 ? '0' + M : M) + "-" +
		(D < 10 ? '0' + D : D) + " " +
		(h < 10 ? '0' + h : h) + ":" +
		(m < 10 ? '0' + m : m) + ":" +
		(s < 10 ? '0' + s : s);
}

const getNowDateInWeek = (da) => {
	if (!da) da = new Date();
	const week = ['日', '一', '二', '三', '四', '五', '六']

	const Y = da.getFullYear(); // 年
	const M = da.getMonth() + 1; // 月
	const D = da.getDate(); // 日
	const h = da.getHours(); // 小时
	const m = da.getMinutes(); // 分
	const s = da.getSeconds(); // 秒
	const day = da.getDay(); // 获取当前星期几

	return Y + "-" +
		(M < 10 ? '0' + M : M) + "-" +
		(D < 10 ? '0' + D : D) + " " +
		'星期' + week[day] + " " +
		(h < 10 ? '0' + h : h) + ":" +
		(m < 10 ? '0' + m : m) + ":" +
		(s < 10 ? '0' + s : s);
}

/**
 * 时间戳转时间
 */
const timestampToTime = (timestamp) => {
	if (!timestamp) return timestamp

	timestamp = timestamp.toString()
	if (timestamp.length != 13 && timestamp.length != 10) return timestamp
	if (timestamp.length == 10) timestamp = parseInt(timestamp) * 1000
	timestamp = parseInt(timestamp)

	const date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	const Y = date.getFullYear() + '-';
	const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	const D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
	const h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	const m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	const s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return Y + M + D + h + m + s;
}

/**
 * 存储登录信息
 * 如果autoLogin = true，则同时存储localStorage，否则只存储到sessionStorage
 */
const setLoginInfo = (data, autoLogin = false) => {
	clearLoginInfo()

	localStorage.setItem('AUTO_LOGIN', autoLogin)
	sessionStorage.setItem('LOGIN_INFO', JSON.stringify(data))

	if (autoLogin) {
		localStorage.setItem('LOGIN_INFO', JSON.stringify(data))
	}

}

/**
 * 获取登录信息
 */
const getLoginInfo = () => {
	let data = sessionStorage.getItem('LOGIN_INFO')
	if (!data) data = localStorage.getItem('LOGIN_INFO')

	return data ? JSON.parse(data) : {}
}

/**
 * 清除登录信息
 */
const clearLoginInfo = () => {
	sessionStorage.removeItem('LOGIN_INFO')
	localStorage.removeItem('AUTO_LOGIN')
	localStorage.removeItem('LOGIN_INFO')
}

// 创建用户信息
const createUser = () => {
	return {
		// 临时授权信息
		token: '',
		// 用户信息
		user: null,
		// 账户信息
		account: null,
		// 角色组
		roles: null,
		// 接口组
		apis: null,
		// 菜单组
		menus: [],
		// 登入时间
		loginTime: null,
		// 登出时间
		unLineTime: null,
	}
}

/**
 * 判断当前账户是否属于某个角色组
 */
const accountIsRole = (cfgKey, that = this) => {
	let user = that.$config.user
	let baseConfig = that.$config.baseConfig

	if (!user.roles) return false
	if (!cfgKey) return false

	// 先检查超管
	let roles = baseConfig.role['super-admin']
	let myRoles = user.roles
	for (let item in myRoles) {
		for (let index in roles) {
			if (myRoles[item].id == roles[index]) return true
		}
	}

	roles = baseConfig.role[cfgKey]
	for (let item in myRoles) {
		for (let index in roles) {
			if (myRoles[item].id == roles[index]) return true
		}
	}

	return false
}

/**
 * 组装表单设计器的url
 */
const getFormUrl = ({
	that = this,
	path = 'index.html',
	params = '',
	position = '/'
} = {}) => {
	let request = that.$config.request
	let user = that.$config.user

	let url = request.urlForm + path + '?' + params
	url += '&key=' + user.token
	url += '&baseUrl=' + request.serverRoot
	url += '&origin=' + window.origin
	url += '#' + position

	return url
}

/**
 * 如果text长度超出len，则追加省略号
 */
const getTextPre = (text, len = 7) => {
	return text && text.length > len ? text.substring(0, len) + '...' : text
}

/**
 * 格式化表单字段，取出可以被检索的数据项
 */
const queryConfig2List = (fields) => {
	let query = []

	fields.forEach(field => {
		let config = field.__config__
		if (config.searchShow && config.searchShow != 'false') {
			query.push({
				left: field.__vModel__,
				op: config.searchShow,
				right: ['', ''],
				tag: config.tag,
				label: config.label,
				field: field
			})
		}
	})

	return query
}

/**
 * Vue 页面跳转
 */
const routerGo = (url, query, $router) => {
	$router.push({
		path: url,
		query: query
	})
}

/**
 * 判断移动端
 */
const isMobile = () => {
	let flag = navigator.userAgent.match(
		/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
	)

	return flag;
}

/**
 * 面包屑内容更新
 */
const setBreadcrumb = (title, separator = '/') => {
	let res = title.split(separator)
	localStorage.setItem('BREADCRUMB-INFO', JSON.stringify(res))

	return res
}

// 获取面包屑内容
const getBreadcrumb = () => {
	let data = localStorage.getItem('BREADCRUMB-INFO')

	return data ? JSON.parse(data) : []
}

// 拼装echarts渐变色
const createEchartsGradient = (colors, echarts) => {
	if (!colors || colors.length < 1) return []

	let res = []
	colors.forEach(item => {
		if (!item || item.length < 1) return []

		let group = []
		let position = 0
		let step = 1 / (item.length - 1) // 计算步长
		item.forEach(color => { // 遍历每一个渐变过程
			group.push({
				offset: position,
				color: color
			})

			position += step
		})

		res.push(new echarts.graphic.LinearGradient(0, 0, 0, 1, group))
	})

	return res
}

// bmap动态引入
const loadBMap = (ak) => {
	return new Promise(function(resolve, reject) {
		if (typeof BMap !== 'undefined') {
			resolve(BMap)
			return true
		}

		window.onBMapCallback = function() {
			resolve(BMap)
		}

		let script = document.createElement('script')
		script.type = 'text/javascript'
		script.src =
			'http://api.map.baidu.com/api?v=3.0&ak=' + ak + '&__ec_v__=20190126&callback=onBMapCallback'
		script.onerror = reject
		document.head.appendChild(script)
	})
}

/**
 * 显示一个iframe 
 * 参数: 标题，地址，宽，高 , 点击遮罩是否关闭, 默认false 
 */
const showIframe = (that = this, title, url, w, h, shadeClose = true) => {
	// 参数修正
	w = w || '95%'
	h = h || '95%'
	shadeClose = (shadeClose === undefined ? false : shadeClose)

	// 弹出面板 
	var index = that.$layer.open({
		type: 2,
		title: title, // 标题 
		shadeClose: shadeClose, // 是否点击遮罩关闭
		maxmin: true, // 显示最大化按钮
		shade: 0.8, // 遮罩透明度 
		scrollbar: false, // 屏蔽掉外层的滚动条
		moveOut: true, // 是否可拖动到外面
		area: [w, h], // 大小 
		content: url, // 传值 
		// 解决拉伸或者最大化的时候，iframe高度不能自适应的问题
		// resizing: function(layero) {
		// 	solveLayerBug(index);
		// }
	});

	// 解决拉伸或者最大化的时候，iframe高度不能自适应的问题
	// $('#layui-layer' + index + ' .layui-layer-max').click(function() {
	// 	setTimeout(function() {
	// 		solveLayerBug(index);
	// 	}, 200)
	// })
}

/**
 * 从数组里获取数据,根据指定数据
 * @param {Object} arr
 * @param {Object} prop
 */
const getArrayField = function(arr, prop) {
	var propArr = [];
	for (var i = 0; i < arr.length; i++) {
		propArr.push(arr[i][prop]);
	}
	return propArr;
}

/**
 * 格式化请求实体的参数，剔除不可操作部分
 */
const fmtReqEntity = (entity) => {
	let params = JSON.parse(JSON.stringify(entity))
	delete params.id
	delete params.createTime
	delete params.operateTime
	delete params.children
	delete params.cfgModify
	delete params.create_time
	delete params.operate_time
	return params
}

/**
 * 展开或关闭所有节点
 * @param {Object} node
 */
const treeExpandAll = (that = this, data, ref, val) => {
	if (data.length < 1) return
	let node = that.$refs[ref].getNode(data[0].id).parent
	treeExpandNodes(node, val)
}

/**
 * 控制当前节点以及其所有子节点展开或折叠
 * @param {Object} node
 * @param {Object} expanded
 */
const treeExpandNodes = (node, expanded) => {
	if (node.childNodes && node.childNodes.length > 0) {
		for (let i = 0; node.childNodes && i < node.childNodes.length; i++) {
			node.childNodes[i].expanded = expanded
			treeExpandNodes(node.childNodes[i], expanded)
		}
	}
}

/**
 * 全选/全不选
 * @param {Object} checked
 */
const treeCheckAll = (that = this, data, ref, checked) => {
	if (data.length < 1) return
	let node = that.$refs[ref].getNode(data[0].id).parent
	treeCheckNodes(node, checked)
}

/**
 * 递归
 * 选中指定节点以及所有下级节点
 * @param {Object} node
 * @param {Object} checked
 */
const treeCheckNodes = (node, checked) => {
	node.checked = checked

	if (node.childNodes && node.childNodes.length > 0) {
		for (let i = 0; node.childNodes && i < node.childNodes.length; i++) {
			treeCheckNodes(node.childNodes[i], checked)
		}
	}
}

/**
 * 滚动至元素底部
 */
const scrollToBottom = (that = this, name) => {
	setTimeout(() => {
		let applyProcess = that.$el.querySelector(name);
		applyProcess.scrollTop = applyProcess.scrollHeight;
	}, 1500)
}

/**
 * 把字符串中的汉字转换成Unicode
 * @param {Object} str
 */
const ch2Unicdoe = (str) => {
	if (!str) {
		return;
	}

	let unicode = '';
	for (let i = 0; i < str.length; i++) {
		let temp = str.charAt(i);
		if (isChinese(temp)) {
			unicode += '\\u' + temp.charCodeAt(0).toString(16);
		} else {
			unicode += temp;
		}
	}

	return unicode;
}

/**
 * 把Unicode转换成汉字
 */
const unicode2Ch = (str) => {
	if (!str) {
		return;
	}
	// 控制循环跃迁
	let len = 1;
	let result = '';
	// 注意，这里循环变量的变化是i=i+len 了
	for (let i = 0; i < str.length; i = i + len) {
		len = 1;
		let temp = str.charAt(i);
		if (temp == '\\') {
			// 找到形如 \u 的字符序列
			if (str.charAt(i + 1) == 'u') {
				// 提取从i+2开始(包括)的 四个字符
				let unicode = str.substr((i + 2), 4);
				// 以16进制为基数解析unicode字符串，得到一个10进制的数字
				result += String.fromCharCode(parseInt(unicode, 16).toString(10));
				// 提取这个unicode经过了5个字符， 去掉这5次循环
				len = 6;
			} else {
				result += temp;
			}
		} else {
			result += temp;
		}
	}
	return result;
}


/**
 * 更新登录检查定时器
 */
const setCheckLoginStatusIndex = (index) => {
	let data = sessionStorage.getItem('CHECK_LOGIN_STATUS_INDEXS')
	if (!data) {
		sessionStorage.setItem('CHECK_LOGIN_STATUS_INDEXS', JSON.stringify([index]))
		return
	}

	let indexs = JSON.parse(data)
	indexs.push(index)

	sessionStorage.setItem('CHECK_LOGIN_STATUS_INDEXS', JSON.stringify(indexs))
}

/**
 * 校验登录检查定时器
 */
const checkLoginStatusIndex = () => {
	let data = sessionStorage.getItem('CHECK_LOGIN_STATUS_INDEXS')
	if (!data) {
		return
	}

	let indexs = JSON.parse(data)
	if (indexs.length >= 2) {
		removeCheckLoginStatusIndex()
	}
}

/**
 * 清除登录检查定时器
 */
const removeCheckLoginStatusIndex = () => {
	let data = sessionStorage.getItem('CHECK_LOGIN_STATUS_INDEXS')
	if (data) {
		let indexs = JSON.parse(data)
		indexs.forEach(t => {
			clearInterval(t)
		})
	}

	sessionStorage.removeItem('CHECK_LOGIN_STATUS_INDEXS')
}

/**
 * 登录信息
 */
const createLoginInfoModel = () => {
	return {
		isLogin: false,
		noReadMsg: 0,
		// 临时授权信息
		token: '',
		// 用户信息
		user: null,
		// 账户信息
		account: null,
		// 角色组
		roles: null,
		// 接口组
		apis: null,
		// 菜单组
		menus: [],
		// 登入时间
		loginTime: null,
		// 登出时间
		unLineTime: null,
	}
}

/**
 * 为URL追加认证令牌
 */
const addUrlAuth = (url, that) => {
	if (url.indexOf('?') == -1) {
		url += '?1'
	}

	if (url.indexOf('key=') == -1) {
		url += '&key=' + that.$config.user.token
	}

	return url
}

/**
 * 跳转下载文件
 * @param {Object} uri
 * @param {Object} params
 * @param {Object} that
 */
const openUrl = (uri, params, that) => {
	if (params.indexOf('key=') == -1) {
		params += '&key=' + that.$config.user.token
	}

	let url = uri + (params && params.startsWith('?') ? '' : '?') + (params || '')
	url = encodeURI(url)
	window.open(that.$config.request.serverRoot + url)
}

/**
 * 表单数据导出
 */
const openExportUrl = (formRef, params, that) => {
	let uri = that.$wuyan.getUrl(that, 'form-data', 'export', {
		form: formRef
	})

	openUrl(uri, params, that)
}

/**
 * 判断变量是否以某个字符串结尾
 * @param {Object} endStr
 */
String.prototype.endWith = function(endStr) {
	const d = this.length - endStr.length;
	return (d >= 0 && this.lastIndexOf(endStr) == d)
}

const wuyan = {
	queryParamsToJsonStr,
	listToMenu,
	listToTree,
	menuListAndApi,
	analysisText,
	getUrl,
	getHttpByMethod,
	getHttp,
	fmtAToB,
	getNowDate,
	timestampToTime,
	setLoginInfo,
	getLoginInfo,
	clearLoginInfo,
	createUser,
	accountIsRole,
	getFormUrl,
	getTextPre,
	queryConfig2List,
	routerGo,
	isMobile,
	setBreadcrumb,
	getBreadcrumb,
	createEchartsGradient,
	loadBMap,
	showIframe,
	getArrayField,
	fmtReqEntity,
	treeCheckAll,
	treeCheckNodes,
	treeExpandAll,
	treeExpandNodes,
	treeToList,
	getTreeNode,
	getNowDateInWeek,
	scrollToBottom,
	unicode2Ch,
	ch2Unicdoe,
	createLoginInfoModel,
	setCheckLoginStatusIndex,
	removeCheckLoginStatusIndex,
	checkLoginStatusIndex,
	addUrlAuth,
	openUrl,
	openExportUrl
}

export default wuyan
