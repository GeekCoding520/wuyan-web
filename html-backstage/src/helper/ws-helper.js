/**
 * WS接入
 * @param {Object} url 接入地址
 * @param {Object} subProtocol 子协议s
 */
let Ws = function(url, subProtocol, eventListen) {
	let that = this
	
	// 接入地址
	this.url = ''
	// 子协议
	this.subProtocol = 'def-common'
	// 心跳间隔
	this.interval = 10000
	// 心跳事件索引
	this.heartIndex = null
	// 通知回调函数
	this.eventListen = null

	// 实例
	let ws = null

	/**
	 * 初始化
	 */
	let init = () => {
		this.url = url
		this.subProtocol = subProtocol || 'def-common'
		this.eventListen = eventListen || null

		if ('WebSocket' in window) {
			ws = new WebSocket(url, [subProtocol]);
		} else {
			console.log('当前浏览器不支持WebSocket服务')
		}

		// 连接发生错误的回调方法
		ws.onerror = function(e) {
			console.error('WS error', e)
			
			that.heartClear()

			if (eventListen instanceof Function) {
				eventListen(e)
			}
		}

		// 连接成功建立的回调方法
		ws.onopen = function(e) {
			if (eventListen instanceof Function) {
				eventListen(e)
			}
		}

		// 接收到消息的回调方法
		ws.onmessage = function(message) {
			if (eventListen instanceof Function) {
				eventListen(message)
			}
		}

		// ws连接断开的回调方法
		ws.onclose = function(e) {
			that.heartClear()
			
			if (eventListen instanceof Function) {
				eventListen(e)
			}
		}
	}
	init()

	/**
	 * 心跳检测
	 */
	this.heartListen = (msg, interval) => {
		if (interval) this.interval = interval

		if (this.heartIndex) clearInterval(this.heartIndex)

		this.heartIndex = setInterval(() => {
			this.send(msg)
		}, this.interval)
	}

	/**
	 * 关闭心跳检测
	 */
	this.heartClear = () => {
		if (this.heartIndex) clearInterval(this.heartIndex)
	}

	/**
	 * 关闭连接
	 */
	this.close = () => {
		ws.close()
	}

	/**
	 * 发送消息
	 * @param {Object} msg 消息内容
	 */
	this.send = (msg) => {
		if (msg) {
			if (msg instanceof String) {
				ws.send(msg)
			} else {
				ws.send(JSON.stringify(msg))
			}
		}
	}

	/**
	 * 发送消息
	 */
	this.sendObj = (type, data) => {
		this.send(this.createMsg(type, data))
	}

	/**
	 * 创建消息ID
	 */
	this.createMsgId = () => {
		let s = []
		let hexDigits = "0123456789abcdef"

		for (let i = 0; i < 36; i++) {
			s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
		}

		s[14] = "4"
		s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)
		s[8] = s[13] = s[18] = s[23] = ""

		let uuid = s.join("")

		return uuid
	}

	/**
	 * 创建消息
	 */
	this.createMsg = (type, data) => {
		return {
			id: this.createMsgId(),
			type: type,
			data: data
		}
	}
}

export default {
	// 认证方式打开
	authOpen: function(that, subProtocol, eventListen) {
		let user = that.$wuyan.getLoginInfo() || {}
		if (user.token) {
			let wsUrl = that.$wuyan.analysisText(that.$config.request.urlWs, {
				token: user.token
			})
			return new Ws(wsUrl, subProtocol, eventListen)
		}

		console.warn('未登录')
		return null
	},
	// 直接打开
	open: function(url, subProtocol, eventListen) {
		return new Ws(url, subProtocol, eventListen)
	},
}
