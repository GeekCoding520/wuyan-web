/**
 * 图片上传自定义实现
 */
const images_upload_handler = (that = this) => {
	// 预定义执行函数
	let func = (blobInfo, succFun, failFun, progress) => {
		// 转化为易于理解的file对象
		let file = blobInfo.blob()
		let formData = new FormData()
		formData.append('file', file, file.name)

		// 把图片或文件添加到data
		// 第1个参数 url 第二参数 data数据，第三个是配置渲染，
		that.$axios.post(
				that.$config.request.urlUpload,
				formData,
				// onUploadProgress 当上传进度是
				{
					onUploadProgress: e => {
						// e.loaded 已经上传的字节数据，e.total 字节数据  转换为1-100的比例值 赋值个pre	 
						progress = Math.floor(e.loaded / e.total * 100)
					}
				}
			)
			.then(res => {
				if (res.length > 0) {
					let fileInfo = res[0]
					succFun(fileInfo.url)
				} else {
					failFun(err.msg || '上传失败')

					that.$message({
						message: err.msg || '上传失败'
					})
				}

				progress = 0
			})
			.catch(err => {
				failFun(err.msg || '上传失败')

				that.$message({
					message: err.msg || '上传失败'
				})

				progress = 0
			})
	}

	return func
}

/**
 * 自定义文件选择器的回调内容
 */
const file_picker_callback = (that = this) => {
	let func = (callback, value, meta) => {
		// 文件分类
		let filetype = '.pdf, .txt, .zip, .rar, .7z, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .mp3, .mp4';
		// 为不同插件指定文件类型
		switch (meta.filetype) {
			case 'image':
				filetype = '.jpg, .jpeg, .png, .gif';
				break;
			case 'media':
				filetype = '.mp3, .mp4';
				break;
			case 'file':
			default:
		}

		// 模拟出一个input用于添加本地文件
		let input = document.createElement('input');
		input.setAttribute('type', 'file');
		input.setAttribute('accept', filetype);
		input.click();

		input.onchange = function() {
			let file = this.files[0];

			let formData = new FormData();
			formData.append('file', file, file.name);

			// 把图片或文件添加到data
			// 第1个参数 url 第二参数 data数据，第三个是配置渲染，
			that.$axios.post(
					that.$config.request.urlUpload,
					formData
				)
				.then(res => {
					if (res.length > 0) {
						let fileInfo = res[0]
						callback(fileInfo.url)
					} else {
						that.$message({
							message: err.msg || '上传失败'
						})
					}
				})
				.catch(err => {
					that.$message({
						message: err.msg || '上传失败'
					})
				})
		};
	}
	
	return func
}

export default {
	images_upload_handler,
	file_picker_callback
}
