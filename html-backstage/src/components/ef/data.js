/**
 * 单层克隆
 * @param {Object} source 源
 * @param {Object} target 目标
 */
function cloneSingle(source, target) {
	if (source) {
		for (let key in target) {
			if (source[key]) target[key] = source[key]
		}
	}
}

// 节点
const createNode = (data) => {
	let model = {
		id: '', // 节点ID
		name: '', // 名称
		type: '', // 节点类型(start|end|event|group)
		left: '', // 左边距
		top: '', // 上边距
		icon: '', // 图标
		status: '', // 状态(start|end|running)
		viewOnly: false, // 不可拖拽
		dept: '', // 部门
		post: '', // 职位
		account: '', // 人员
	}
	cloneSingle(data, model)
	return model
}

// 连线
const createLine = (data) => {
	let model = {
		id: '', // ID
		source: '', // 源节点ID
		target: '', // 目标节点ID
		label: '', // 业务名称
		value: '', // 状态值
		connector: 'Bezier', // 连接器
		anchors: [], // 连接点位置
		paintStyle: { // 样式
			strokeWidth: 1, // 厚度
			stroke: '#b4b5b8' // 颜色
		}
	}
	cloneSingle(data, model)
	return model
}

// 数据模型
const createData = (data) => {
	let model = {
		name: '',
		node_list: [], // 节点
		line_list: [], // 连线
		cfg_status: true, // 状态
		creator_id: '', // 创建者ID
		creator_name: '', // 创建者昵称
		operate_time: '', // 更新时间
		create_time: '', // 创建时间
	}
	cloneSingle(data, model)
	return model
}

// 节点菜单
const nodeMenu = () => {
	let startEnd = createNode({
		id: '1',
		type: 'group',
		name: '起始',
		icon: 'el-icon-video-play'
	})
	startEnd.open = true
	startEnd.children = []
	// 增加开始节点
	startEnd.children.push(createNode({
		id: '13',
		type: 'start',
		name: '流程开始',
		icon: 'el-icon-video-play',
		style: {},
		status: 'start'
	}))
	// 增加结束节点
	startEnd.children.push(createNode({
		id: '21',
		type: 'end',
		name: '流程结束',
		icon: 'el-icon-finished',
		// 自定义覆盖样式
		style: {},
		status: 'end'
	}))

	let prozess = createNode({
		id: '2',
		type: 'group',
		name: '过程',
		icon: 'el-icon-video-pause',
	})
	prozess.open = true
	prozess.children = []
	// 增加事件节点
	prozess.children.push(createNode({
		id: '21',
		type: 'event',
		name: '事件',
		icon: 'el-icon-set-up',
		// 自定义覆盖样式
		style: {},
		status: 'running'
	}))

	return [startEnd, prozess]
}

// 演示流程
const testData = () => {
	let node_list = [createNode({
			id: 'nodeA',
			name: '开始',
			type: 'start',
			left: '18px',
			top: '223px',
			icon: 'el-icon-caret-right',
			status: 'start',
			viewOnly: true,
		}),
		createNode({
			id: 'nodeB',
			type: 'event',
			name: '组织部-部长',
			left: '351px',
			top: '96px',
			icon: 'el-icon-goods',
			status: 'running',
		}),
		createNode({
			id: 'nodeC',
			name: '组织部-负责人',
			type: 'event',
			left: '354px',
			top: '351px',
			icon: 'el-icon-present',
			status: 'running',
		}),
		createNode({
			id: 'nodeD',
			name: '结束',
			type: 'end',
			left: '723px',
			top: '215px',
			icon: 'el-icon-switch-button',
			status: 'end',
		})
	]

	let line_list = [createLine({
			source: 'nodeA',
			target: 'nodeB',
			label: '由负责人提交',
			connector: 'Straight',
		}),
		createLine({
			source: 'nodeA',
			target: 'nodeC',
			label: '普通提交',
		}),
		createLine({
			source: 'nodeB',
			target: 'nodeD',
			label: '审批',
		}),
		createLine({
			source: 'nodeC',
			target: 'nodeD',
			label: '审批',
		}),
		createLine({
			source: 'nodeC',
			target: 'nodeC',
			label: '自连接',
		})
	]

	let model = createData({
		name: '演示流程'
	})
	model.node_list = node_list
	model.line_list = line_list
	return model
}

export default {
	cloneSingle,
	createNode,
	createLine,
	createData,
	nodeMenu,
	testData
}
