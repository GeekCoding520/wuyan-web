/**
 * 当前激活节点是否为开始节点
 * @param {Object} flowList
 */
const hasStartNode = (node) => {
	if (node.node_type == 'start' && node.node_status == 'start') {
		return true
	}
	return false
}

// 是否具有该线
export function hasLine(data, from, to) {
	for (let i = 0; i < data.lineList.length; i++) {
		let line = data.lineList[i]
		if (line.from === from && line.to === to) {
			return true
		}
	}
	return false
}

// 是否含有相反的线
export function hashOppositeLine(data, from, to) {
	return hasLine(data, to, from)
}

// 获取连线
export function getConnector(jsp, from, to) {
	let connection = jsp.getConnections({
		source: from,
		target: to
	})[0]
	return connection
}

// 获取唯一标识
export function uuid() {
	return Math.random().toString(36).substr(3, 10)
}

// 返回节点状态信息
export function getNodeDescribe(node) {
	if(node.status_name) return `【${node.creator_name || ''}】${node.status_name}`
	
	let desc = ''

	// 开始节点
	if (node.node_type == "start" && node.node_status == 'start') {
		if (node.cfg_active) {
			return `待【${creator_name || ''}】重新提交资料`
		} else {
			return `【${node.creator_name || ''}】提交了数据`
		}
	}

	// 事件节点
	if (node.node_type == "event" && node.node_status == 'running') {
		return `【${creator_name || ''}】` + (node.cfg_active ? '处理中' : '已处理')
	}

	// 结束节点
	if (node.node_type == "end" && node.node_status == 'end') {
		return node.status_name || '已结束'
	}

	if (node.node_type == 'start' && node.node_status == 'end') {
		return `被【${creator_name || ''}】` + (node.status_name + (node.status_name.endWith('了') ? '' : '了'))
	}

	return desc
}

/**
 * 计算下一组处理单位
 * @param {Object} flowData
 * @param {Object} flowList
 */
export function getFlowNextNode(flowData, flowList) {
	let res = {
		next: [],
		current: null,
		hasStartNode: false,
	}
	let node_list = flowData.node_list
	let line_list = flowData.line_list
	let mapNode = {}

	// 找到当前处理节点
	for (let index in flowList) {
		if (flowList[index].cfg_active) {
			res.current = flowList[index]
			res.hasStartNode = hasStartNode(flowList[index])
			break
		}
	}

	node_list.forEach(item => {
		mapNode[item.id] = item
	})
	if (!res.current) return res

	// 找到下一个节点组
	line_list.forEach(item => {
		if (item.source == res.current.node_id) {
			res.next.push({
				node: mapNode[item.target],
				line: item
			})
		}
	})

	return res
}
