const nodeType = [{
		id: 1,
		name: '开始',
		value: 'start'
	},
	{
		id: 2,
		name: '事件',
		value: 'event'
	},
	{
		id: 3,
		name: '结束',
		value: 'end'
	},
]

// 节点状态
const nodeStatus = [{
		id: 1,
		name: '开始',
		value: 'start'
	},
	{
		id: 2,
		name: '运行中',
		value: 'running'
	},
	{
		id: 3,
		name: '结束',
		value: 'end'
	},
]

// 线的连接器
const lineConnector = [{
		id: 1,
		name: 'Bezier',
		value: 'Bezier'
	},
	{
		id: 2,
		name: 'Straight',
		value: 'Straight'
	},
	{
		id: 3,
		name: 'Flowchart',
		value: 'Flowchart'
	},
	{
		id: 4,
		name: 'StateMachine',
		value: 'StateMachine'
	},
]

// 节点类型
export default {
	nodeType,
	nodeStatus,
	lineConnector
}
