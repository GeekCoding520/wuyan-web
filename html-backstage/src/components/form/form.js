/**
 * 判断是否应该创建子列表输入
 * @param {Object} field
 */
const formCreateChildList = (field) => {
	let bl = field.__config__.defaultValue && field.__config__.defaultValue.toString().startsWith("__config:")
	let childlist = null
	if (bl) {
		let str = field.__config__.defaultValue.toString().replace('__config:', '').trim()
		str = str.replace(/[\r\n]|[\t]/g, "")
		str = str.replace(/(\},\])/g, "}]")
		childlist = JSON.parse(str)
	}

	return childlist
}

const form = {
	formCreateChildList
}

export default form
