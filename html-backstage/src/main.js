import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI, {
	size: 'mini'
})

// e-icon-picker
import iconPicker from 'e-icon-picker';
import "e-icon-picker/lib/symbol.js"; //基本彩色图标库
import 'e-icon-picker/lib/index.css'; // 基本样式，包含基本图标
import 'font-awesome/css/font-awesome.min.css'; //font-awesome 图标库
Vue.use(iconPicker, {
	FontAwesome: true,
	ElementUI: true,
	eIcon: true, //自带的图标，来自阿里妈妈
	eIconSymbol: true, //是否开启彩色图标
	addIconList: [],
	removeIconList: []
});

// tinymce 引入
import tinymce from 'tinymce'
import VueTinymce from '@packy-tang/vue-tinymce'
// 将全局tinymce对象指向给Vue作用域下
Vue.prototype.$tinymce = tinymce
// 安装vue的tinymce组件
Vue.use(VueTinymce, {
	language: 'zh_CN'
})

// 网络请求
import axios from '@/config/axios.js'
Vue.prototype.$axios = axios

// 基础配置
import config from '@/config/config.js'
Vue.prototype.$config = config

// 接口配置
import apiInfo from '@/helper/api.js'
Vue.prototype.$apiInfo = apiInfo

// 工具类
import ws from '@/helper/ws-helper.js'
Vue.prototype.$ws = ws

// 工具类
import wuyan from '@/helper/wuyan.js'
Vue.prototype.$wuyan = wuyan

// echarts
let echarts = require('echarts')
Vue.prototype.$echart = echarts

// layer
import layer from 'vue-layer'
Vue.prototype.$layer = layer(Vue)

// window
Vue.prototype.$window = window

// 页面通信
Vue.prototype.$pageSession = {
	event: null, // 当前绑定的事件
	data: null, // 涉及到的数据
}

import BaiduMap from 'vue-baidu-map'
Vue.use(BaiduMap, {
  /* 需要注册百度地图开发者来获取你的ak */
  ak: config.project.bmapAK
})

// 路由守卫
import router from '@/config/router.js'

const app = new Vue({
	router,
	render: h => h(App),
}).$mount('#app')

// 全局监听消息
window.addEventListener('message', (event) => {
	if (event.data && Vue.prototype.$pageSession.event) {
		Vue.prototype.$pageSession.data = event.data
		Vue.prototype.$pageSession.event(event.data)
	}
}, false)
