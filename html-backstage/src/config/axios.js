import Vue from 'vue'

import axios from 'axios'

import VueAxios from 'vue-axios'

import cfg from '@/config/config.js'

import wuyan from '@/helper/wuyan.js'
cfg.user = wuyan.getLoginInfo()

Vue.use(VueAxios, axios)

axios.defaults.baseURL = cfg.request.serverRoot
axios.defaults.headers.common[cfg.request.headerAuthName] = cfg.user.token
axios.defaults.headers.post['Content-Type'] = 'application/json'

// 添加请求拦截器
let storageVerifyCodeName = 'verifyCodeSecret'
axios.interceptors.request.use(function(config) {
	if (config.url.endWith(cfg.request.urlAuthLogin)) {
		let code = window.sessionStorage.getItem(storageVerifyCodeName)
		if (code) {
			config.headers[cfg.request.headerCodeAuthName] = code
		}
	}

	// 在发送请求之前做些什么
	return config;
}, function(error) {
	// 对请求错误做些什么
	return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function(response) {
	let data = response.data

	// 保证正常响应并且有数据返回时,处理返回数据,只保留data部分
	if (data && data.code === cfg.request.RES_CODE_SUCCESS) {
		if (response.config.url.endWith(cfg.request.urlVerifyCode)) {
			window.sessionStorage.setItem(storageVerifyCodeName, data.data.secret)
		}

		return data.data
	} else {
		return Promise.reject({msg: data.msg || '网络异常，请重新尝试！'})
	}
}, function(error) {
	// 对响应错误做点什么
	return Promise.reject({msg: '网络异常，请重新尝试！'})
});

export default axios
