import Vue from 'vue'
import VueRouter from 'vue-router'

/**
 * 管理端
 */
import ManagerLogin from '@/view/manager/login/index.vue'
import ManagerIndex from '@/view/manager/index.vue'

// wuyan frame
import ManagerMsgList from '@/view/manager/system/msg/list.vue'
import ManagerAccountInfo from '@/view/manager/system/account/info.vue'
import ManagerAccountChangePwd from '@/view/manager/system/account/change-pwd.vue'
import ManagerAccountList from '@/view/manager/system/account/list.vue'
import ManagerDeptList from '@/view/manager/system/dept/list.vue'
import ManagerPostList from '@/view/manager/system/post/list.vue'
import ManagerRoleList from '@/view/manager/system/role/list.vue'
import ManagerMenuList from '@/view/manager/system/menu/list.vue'
import ManagerConfigList from '@/view/manager/system/config/list.vue'
import ManagerFileList from '@/view/manager/system/file/list.vue'
import ManagerTypeList from '@/view/manager/system/type/list.vue'
import ManagerApiList from '@/view/manager/system/api/list.vue'
import ManagerFormList from '@/view/manager/system/form/list.vue'
import ManagerFormDataList from '@/view/manager/system/form/data.vue'
import ManagerAgreementModelList from '@/view/manager/system/agreement-model/list.vue'

// 流程设计
import ManagerFlowIndex from '@/view/manager/system/flow/index.vue'
import ManagerFlowList from '@/view/manager/system/flow/list.vue'
import ManagerLogList from '@/view/manager/system/log/list.vue'

Vue.use(VueRouter)
const routes = [ //
	{
		path: '/',
		redirect: '/manager-index',
	},
	// Manager
	{
		path: '/manager-login',
		name: 'manager-login',
		component: ManagerLogin
	},
	{
		path: '/manager-index',
		name: 'manager-index',
		component: ManagerIndex
	},
	{
		path: '/manager-index-:menu',
		name: 'manager-index-menu',
		component: ManagerIndex
	},
	// wuyan
	{
		path: '/manager-msg/list',
		name: 'manager-msg-list',
		component: ManagerMsgList
	},
	{
		path: '/manager-account/info',
		name: 'manager-account-info',
		component: ManagerAccountInfo
	},
	{
		path: '/manager-account/change-pwd',
		name: 'manager-account-change-pwd',
		component: ManagerAccountChangePwd
	},
	{
		path: '/manager-account/list',
		name: 'manager-account-list',
		component: ManagerAccountList
	},
	{
		path: '/manager-dept/list',
		name: 'manager-dept-list',
		component: ManagerDeptList
	},
	{
		path: '/manager-post/list',
		name: 'manager-post-list',
		component: ManagerPostList
	},
	{
		path: '/manager-role/list',
		name: 'manager-role-list',
		component: ManagerRoleList
	},
	{
		path: '/manager-menu/list',
		name: 'manager-menu-list',
		component: ManagerMenuList
	},
	{
		path: '/manager-config/list',
		name: 'manager-config-list',
		component: ManagerConfigList
	},
	{
		path: '/manager-file/list',
		name: 'manager-file-list',
		component: ManagerFileList
	},
	{
		path: '/manager-type/list',
		name: 'manager-type-list',
		component: ManagerTypeList
	},
	{
		path: '/manager-api/list',
		name: 'manager-api-list',
		component: ManagerApiList
	},
	{
		path: '/manager-form/list',
		name: 'manager-form-list',
		component: ManagerFormList
	},
	{
		path: '/manager-form/data-list',
		name: 'manager-form-data-list',
		component: ManagerFormDataList
	},
	{
		path: '/manager-agreement-model/list',
		name: 'manager-agreement-model',
		component: ManagerAgreementModelList
	},
	// 流程设计
	{
		path: '/manager-flow/index',
		name: 'manager-flow-index',
		component: ManagerFlowIndex
	},
	{
		path: '/manager-flow/list',
		name: 'manager-flow-list',
		component: ManagerFlowList
	},
	{
		path: '/manager-log/list',
		name: 'manager-log-list',
		component: ManagerLogList
	}
]

const router = new VueRouter({
	routes
})

//全局守卫
router.beforeEach((to, from, next) => {
	next()
})

export default router
