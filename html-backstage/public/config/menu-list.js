function initGlobalMenu() {
	return [
		{
			"id": "7",
			"name": "表单配置",
			"icon": "el-icon-bank-card",
			"info": "表单配置",
			"children": [{
					"id": "7-1",
					"name": "表单管理",
					"url": "/#/manager-form/list"
				},
				{
					"id": "7-2",
					"name": "数据管理",
					"url": "/#/manager-form/data-list"
				},
				{
					"id": "7-3",
					"name": "表单分类",
					"url": "",
					"clickFun": "return '/#/manager-type/list?parentId=' + that.$config.baseConfig.type.formParentId"
				}
			]
		},
		{
			"id": "8",
			"name": "流程配置",
			"icon": "el-icon-paperclip",
			"info": "流程配置",
			"children": [{
					"id": "8-1",
					"name": "流程管理",
					"url": "/#/manager-flow/list"
				},
				{
					"id": "8-2",
					"name": "流程分类",
					"url": "",
					"clickFun": "return '/#/manager-type/list?parentId=' + that.$config.baseConfig.type.flowParentId"
				},
				{
					"id": "8-3",
					"name": "流程设计",
					"url": "/#/manager-flow/index"
				}
			]
		},
		{
			"id": "11",
			"name": "系统配置",
			"icon": "el-icon-setting",
			"type": 1,
			"identity": "setting",
			"children": [{
					"id": "11-1",
					"name": "个人信息",
					"icon": "fa fa-user",
					"type": 2,
					"cfgSort": 999,
					"url": "/#/manager-account/info"
				},
				{
					"id": "11-1.1",
					"name": "修改密码",
					"icon": "el-icon-lock",
					"type": 2,
					"cfgSort": 999,
					"url": "/#/manager-account/change-pwd"
				},
				{
					"id": "11-2",
					"name": "部门管理",
					"icon": "fa fa-sitemap",
					"type": 2,
					"cfgSort": 998,
					"identity": "PubDept",
					"url": "/#/manager-dept/list"
				},
				{
					"id": "11-12",
					"name": "岗位管理",
					"icon": "fa fa-address-book-o",
					"url": "/#/manager-post/list",
					"type": 2,
					"cfgSort": 997,
					"identity": "PubPost"
				},
				{
					"id": "11-3",
					"name": "用户账户",
					"icon": "fa fa-user-circle-o",
					"url": "/#/manager-account/list"
				},
				{
					"id": "11-4",
					"name": "角色列表",
					"icon": "el-icon-s-check",
					"url": "/#/manager-role/list"
				},
				{
					"id": "my-msg",
					"name": "消息管理",
					"icon": "el-icon-chat-dot-round",
					"url": "/#/manager-msg/list"
				},
				{
					"id": "11-7",
					"name": "参数配置",
					"icon": "el-icon-edit",
					"url": "/#/manager-config/list"
				},
				{
					"id": "11-8",
					"name": "菜单管理",
					"icon": "el-icon-menu",
					"url": "/#/manager-menu/list"
				},
				{
					"id": "11-9",
					"name": "接口管理",
					"icon": "fa fa-align-right",
					"url": "/#/manager-api/list"
				},
				{
					"id": "11-10",
					"name": "附件管理",
					"icon": "el-icon-files",
					"url": "/#/manager-file/list"
				},
				{
					"id": "11-11",
					"name": "类别管理",
					"icon": "fa fa-square-o",
					"url": "/#/manager-type/list"
				},
				{
					"id": "11-13",
					"name": "协议模板",
					"icon": "fa fa-square-o",
					"url": "/#/manager-agreement-model/list"
				},
				{
					"id": "11-14",
					"name": "请求日志",
					"icon": "fa fa-map-o",
					"url": "/#/manager-log/list"
				}
			]
		},
		{
			"id": "12",
			"name": "退出系统",
			"icon": "el-icon-switch-button",
			"url": "",
			"clickFun": "window.sessionStorage.removeItem('manager-menu');that.$wuyan.routerGo('/manager-login', {}, that.$router)"
		}
	]
}
