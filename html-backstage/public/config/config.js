function initGlobalConfig() {
	// 服务入口
	// let URL_SERVER_ROOT = "http://ybu-duty-record.zgllh.site/1.0"
	let URL_SERVER_ROOT = "http://127.0.0.1:8080"

	// websocket访问地址
	// let WS_SERVER_ROOT = "ws://ybu-duty-record.zgllh.site/1.0"
	let WS_SERVER_ROOT = "ws://127.0.0.1:8086"
	
	// 表单访问地址
	let URL_FORM = 'http://form-generator.ims-nasatp.gxzyit.com/'
	// let URL_FORM = 'http://127.0.0.1:8848/form-generator/'

	return {
		// 工程信息
		project: {
			// 项目名称
			name: '宜宾学院保卫处值班记录系统-后台管理平台',
			// 版本号
			version: 'v0.0.1',
			breadcrumbSeparator: '>>',
			bmapAK: 'BFBikCCwRDMlVTa1IrehV63ZAby9zean',
			manager: {
				// 默认显示搜索区域
				defaultSearchBox: true
			},
			msgRefreshInterval: 10000
		},
		// 请求配置
		request: {
			// 异步请求成功业务标识
			RES_CODE_SUCCESS: 0,
			RES_CODE_ERR_LOGIN_VERIFY_STOP: 2007,
			// 服务入口
			serverRoot: URL_SERVER_ROOT,
			// 新建表单URL前缀
			urlForm: URL_FORM,
			urlWs: WS_SERVER_ROOT + '/ws/api/default/{token}',
			// 文件上传地址
			urlUpload: URL_SERVER_ROOT + '/upload/api/upload/',
			// 获取图片验证码的地址
			urlVerifyCode: URL_SERVER_ROOT + '/auth/api/verify/code-base64',
			// 登录验证地址
			urlAuthLogin: URL_SERVER_ROOT + '/auth/api/login',
			// 请求头部附加信息名称
			headerAuthName: 'authorization',
			headerCodeAuthName: 'authorization-code',
		},
		// 基础信息配置
		baseConfig: {
			// 本地调式模式
			debug: false,
			// 从远程服务加载菜单
			remoteMenu: false,
			// 开启菜单权限校验
			showMenu: false,
			type: {
				// 表单分类ID
				formParentId: 285,
				// 流程分类ID
				flowParentId: 1026,
				// 文章分类ID
				articleParentId: 1027,
			},
			// 表单相关
			form: {
				// 业务流程表ID
				pubFlowBussiness: 376,
				// 协议模板
				agreementModel: 377,
				// 请求日志
				reqLogs: 378
			},
			// 分页参数配置
			pageParams: {
				isPage: true,
				page: 1,
				limit: 10,
				orders: [],
				params: [],
			},
			// 注册方式
			accountSource: {
				0: '普通注册',
				1: '后台创建',
				2: '微信公众号',
				3: '微信小程序',
				4: '手机号一键登录',
				5: '其它',
			},
			// 人员行政级别
			executiveLevel: {
				0: '无',
				1: '国家级正职',
				2: '国家级副职',
				3: '省部级正职',
				4: '省部级副职',
				5: '厅局级正职',
				6: '巡视员',
				7: '厅局级副职',
				8: '副巡视员',
				9: '县处级正职',
				10: '调研员',
				11: '县处级副职',
				12: '副调研员',
				13: '乡科级正职',
				14: '主任科员',
				15: '乡科级副职',
				16: '副主任科员',
				17: '科员',
				18: '社区或驻村干部',
				19: '办事员',
			},
			// 角色配置
			role: {
				// 超管配置
				'super-admin': [1],
			}
		},
		// 用户信息
		user: {
			isLogin: false,
			noReadMsg: 0,
			// 临时授权信息
			token: '',
			// 用户信息
			user: null,
			// 账户信息
			account: null,
			// 角色组
			roles: null,
			// 接口组
			apis: null,
			// 菜单组
			menus: [],
			dept: {},
			posts: [],
			// 登入时间
			loginTime: null,
			// 登出时间
			unLineTime: null,
		},
		// 表单相关配置
		form: {
			// 时间日期选择器
			timePickerOptions: {
				shortcuts: [{
					text: '今天',
					onClick(picker) {
						picker.$emit('pick', new Date());
					}
				}, {
					text: '昨天',
					onClick(picker) {
						const date = new Date();
						date.setTime(date.getTime() - 3600 * 1000 * 24);
						picker.$emit('pick', date);
					}
				}, {
					text: '一周前',
					onClick(picker) {
						const date = new Date();
						date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
						picker.$emit('pick', date);
					}
				}]
			}
		},
		// tinymce配置
		tinymce: {
			//skin:'oxide-dark',
			language: 'zh_CN',
			// 选择需加载的插件
			plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template advcode codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists wordcount imagetools textpattern help emoticons autosave bdmap indent2em autoresize formatpainter axupimgs importword',
			toolbar: 'code undo redo restoredraft | cut copy paste pastetext | forecolor backcolor bold italic underline strikethrough link anchor | alignleft aligncenter alignright alignjustify outdent indent2em | \
		                     styleselect formatselect fontselect fontsizeselect | bullist numlist | blockquote subscript superscript removeformat | \
		                     table image media charmap emoticons hr pagebreak insertdatetime print preview | fullscreen | bdmap lineheight formatpainter importword',
			// 编辑器高度
			height: 650,
			min_height: 400,
			// 字体大小
			fontsize_formats: '12px 14px 16px 18px 24px 36px 48px 56px 72px',
			// 字形
			font_formats: '微软雅黑=Microsoft YaHei,Helvetica Neue,PingFang SC,sans-serif;苹果苹方=PingFang SC,Microsoft YaHei,sans-serif;宋体=simsun,serif;仿宋体=FangSong,serif;黑体=SimHei,sans-serif;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats;知乎配置=BlinkMacSystemFont, Helvetica Neue, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif;小米配置=Helvetica Neue,Helvetica,Arial,Microsoft Yahei,Hiragino Sans GB,Heiti SC,WenQuanYi Micro Hei,sans-serif',
			// 默认链接
			link_list: [{
				title: '百度',
				value: 'http://www.baidu.com'
			}, ],
			// 默认图片
			image_list: [{
				title: '预置图片1',
				value: 'https://www.tiny.cloud/images/glyph-tinymce@2x.png'
			}, ],
			// 预制图片样式
			image_class_list: [{
					title: 'None',
					value: ''
				},
				{
					title: 'Some class',
					value: 'class-name'
				}
			],
			//importcss_append: true,
			// 为内容模板插件提供预置模板
			templates: [{
					title: '模板1',
					description: '介绍文字1',
					content: '模板内容'
				},
				{
					title: '模板2',
					description: '介绍文字2',
					content: '<div class="mceTmpl"><span class="cdate">CDATE</span>，<span class="mdate">MDATE</span>，我的内容</div>'
				}
			],
			// content_security_policy: "script-src *;",
			extended_valid_elements: 'script[src]',
			template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
			template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
			autosave_ask_before_unload: false,
			toolbar_mode: 'wrap',
			convert_urls: false,
			// 此选项可以通过空格或逗号分隔的类型名称指定允许上传的类型。
			file_picker_types: 'file image media',
			// 图片地址基本目录
			// images_upload_base_path: '/demo',
			// 图片上传自定义实现
			images_upload_handler: function(blobInfo, succFun, failFun, progress) {
				succFun('/image/img-1.png');
			},
			// 自定义文件选择器的回调内容
			file_picker_callback: function(callback, value, meta) {
				// 文件分类
				var filetype = '.pdf, .txt, .zip, .rar, .7z, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .mp3, .mp4';
				// 后端接收上传文件的地址
				var upurl = '/demo/upfile.php';
				// 为不同插件指定文件类型及后端地址
				switch (meta.filetype) {
					case 'image':
						filetype = '.jpg, .jpeg, .png, .gif';
						upurl = 'upimg.php';
						break;
					case 'media':
						filetype = '.mp3, .mp4';
						upurl = 'upfile.php';
						break;
					case 'file':
					default:
				}

				// 模拟出一个input用于添加本地文件
				var input = document.createElement('input');
				input.setAttribute('type', 'file');
				input.setAttribute('accept', filetype);
				input.click();

				input.onchange = function() {
					var file = this.files[0];

					var xhr, formData;
					xhr = new XMLHttpRequest();
					xhr.withCredentials = false;
					xhr.open('POST', upurl);

					xhr.onload = function() {
						var json;
						if (xhr.status != 200) {
							failure('HTTP Error: ' + xhr.status);
							return;
						}
						json = JSON.parse(xhr.responseText);
						if (!json || typeof json.location != 'string') {
							failure('Invalid JSON: ' + xhr.responseText);
							return;
						}
						callback(json.location);
					};

					formData = new FormData();
					formData.append('file', file, file.name);
					xhr.send(formData);
				};
			},
			//icons:'ax-color',
			// word导入
			importword_handler: function(editor, files, next) {
				var file_name = files[0].name
				if (file_name.substr(file_name.lastIndexOf(".") + 1) == 'docx') {
					editor.notificationManager.open({
						text: '正在转换中...',
						type: 'info',
						closeButton: false,
					});

					next(files);
					editor.notificationManager.close()
				} else {
					editor.notificationManager.open({
						text: '目前仅支持docx文件格式，若为doc，请将扩展名改为docx',
						type: 'warning',
					});
				}
				// next(files);
			},
			importword_filter: function(result, insert, message) {
				// 自定义操作部分
				insert(result)
			}
		}
	}
}
