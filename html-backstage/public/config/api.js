function initGlobalApi() {
	return {
		"pub-config": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-config",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-config",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-config/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-config/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-config/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-config/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-config/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-form": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-form",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-form",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-form/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-form/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-form/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-form/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-form/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			},
			"upFlow": {
				"modular": "wuyan",
				"url": "/api/pub-form/{id}/up-flow/{flowId}/{flowName}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-user": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-user",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-user",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-user/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-user/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-user/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-user/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-user/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-article-comment": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-article-comment/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-feedback": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-feedback",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-feedback",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-feedback/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-feedback/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-feedback/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-feedback/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-feedback/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-function": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-function",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-function",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-function/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-function/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-function/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-function/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-function/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			},
			"import-json": {
				"modular": "wuyan",
				"url": "/api/pub-function/import-json",
				"method": "post",
				"contentType": "",
			}
		},
		"pub-role": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-role",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-role",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-role/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-role/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-role/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-role/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-role/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-article-liked": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-article-liked/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-user-address": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-user-address",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-user-address",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-user-address/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-user-address/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-user-address/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-user-address/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-user-address/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-article-content": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-article-content",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-article-content",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-article-content/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-article-content/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-article-content/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-article-content/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-article-content/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-account-role": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-account-role",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-account-role",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-account-role/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-account-role/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-account-role/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-account-role/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-account-role/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-role-function": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-role-function",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-role-function",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-role-function/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-role-function/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-role-function/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-role-function/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-role-function/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-article": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-article",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-article",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-article/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-article/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-article/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-article/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-article/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-type": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-type",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-type",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-type/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-type/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-type/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-type/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-type/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-role-api": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-role-api",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-role-api",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-role-api/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-role-api/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-role-api/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-role-api/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-role-api/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-api": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-api",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-api",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-api/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-api/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-api/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-api/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-api/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-article-read": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-article-read",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-article-read",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-article-read/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-article-read/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-article-read/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-article-read/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-article-read/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-file": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-file",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-file",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-file/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-file/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-file/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-file/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-file/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-dept": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-dept",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-dept",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-dept/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-dept/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-dept/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-dept/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-dept/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-account": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-account",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-account",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-account/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-account/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-account/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-account/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-account/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-msg": {
			"ownList": {
				"modular": "wuyan",
				"url": "/api/pub-msg/own",
				"method": "get",
				"contentType": "",
			},
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-msg",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-msg",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-msg/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-msg/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-msg/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-msg/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-msg/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			},
			"read": {
				"modular": "wuyan",
				"url": "/api/pub-msg/{id}/read",
				"method": "get",
				"contentType": "",
			},
			"read-all": {
				"modular": "wuyan",
				"url": "/api/pub-msg/read-all",
				"method": "get",
				"contentType": "",
			},
			"countNoRead": {
				"modular": "wuyan",
				"url": "/api/pub-msg/count/no-read",
				"method": "get",
				"contentType": "",
			}
		},
		"form-data": {
			"list": {
				"modular": "form",
				"url": "/api/{form}",
				"method": "get",
				"contentType": "",
			},
			"listByFormId": {
				"modular": "form",
				"url": "/api/page/formId/{id}",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "form",
				"url": "/api/{form}",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "form",
				"url": "/api/{form}/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "form",
				"url": "/api/{form}/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "form",
				"url": "/api/{form}/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "form",
				"url": "/api/{form}/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "form",
				"url": "/api/{form}/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			},
			"ensure-index-full-text": {
				"modular": "form",
				"url": "/api/{form}/ensure-index-full-text",
				"method": "get",
				"contentType": "",
			},
			"cfg-flow-get": {
				"modular": "form",
				"url": "/api/cfg-flow/{id}",
				"method": "get",
				"contentType": "",
			},
			"cfg-flow-node-get": {
				"modular": "form",
				"url": "/api/cfg-flow/node/{id}",
				"method": "get",
				"contentType": "",
			},
			"cfg-flow-active-node-get": {
				"modular": "form",
				"url": "/api/cfg-flow/active-node/{id}",
				"method": "get",
				"contentType": "",
			},
			"cfg-flow-node-handle": {
				"modular": "form",
				"url": "/api/cfg-flow/{form}/handle/{dataId}/{lineId}-{nodeId}",
				"method": "post",
				"contentType": "",
			},
			"import": {
				"modular": "form",
				"url": "/api/import/{form}",
				"method": "post",
				"contentType": "",
			},
			"export": {
				"modular": "form",
				"url": "/api/export/{form}",
				"method": "post",
				"contentType": "",
			},
		},
		"auth": {
			'register': {
				"modular": "auth",
				"url": "/api/register",
				"method": "post",
				"contentType": "",
			},
			'resetPwd': {
				"modular": "auth",
				"url": "/api/login/reset-pwd",
				"method": "post",
				"contentType": "",
			},
			"roleCreate": {
				"modular": "auth",
				"url": "/api/role/{id}",
				"method": "post",
				"contentType": "",
			},
			'roleGetSimple': {
				"modular": "auth",
				"url": "/api/role/{id}/simple",
				"method": "get",
				"contentType": "",
			},
			'verifyCode': {
				"modular": "auth",
				"url": "/api/verify/code",
				"method": "get",
				"contentType": "",
			},
			'verifyCodeBase64': {
				"modular": "auth",
				"url": "/api/verify/code-base64",
				"method": "get",
				"contentType": "",
			},
			'sendRegister': {
				"modular": "auth",
				"url": "/api/verify/send-register/{username}",
				"method": "get",
				"contentType": "",
			},
			'sendResetPwdCode': {
				"modular": "auth",
				"url": "/api/verify/send-reset-pwd-code/{username}",
				"method": "get",
				"contentType": "",
			},
			'login': {
				"modular": "auth",
				"url": "/api/login",
				"method": "post",
				"contentType": "",
			},
			'check': {
				"modular": "auth",
				"url": "/api/check/{token}",
				"method": "get",
				"contentType": "",
			},
			'change-pwd': {
				"modular": "auth",
				"url": "/api/change-pwd",
				"method": "put",
				"contentType": "",
			},
			'menu': {
				"modular": "auth",
				"url": "/api/menu",
				"method": "get",
				"contentType": "",
			},
		},
		"pub-post": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-post",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-post",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-post/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-post/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-post/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-post/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-post/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			}
		},
		"pub-view-account": {
			"list": {
				"modular": "wuyan",
				"url": "/api/pub-view-account",
				"method": "get",
				"contentType": "",
			},
			"add": {
				"modular": "wuyan",
				"url": "/api/pub-view-account",
				"method": "post",
				"contentType": "",
			},
			"adds": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/plist",
				"method": "post",
				"contentType": "",
			},
			"getOne": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/{id}",
				"method": "get",
				"contentType": "",
			},
			"edit": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/{id}",
				"method": "put",
				"contentType": "application/json",
			},
			"del": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/{id}",
				"method": "delete",
				"contentType": "",
			},
			"upField": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/{id}/{field}/{value}",
				"method": "put",
				"contentType": "",
			},
			"updateInfo": {
				"modular": "wuyan",
				"url": "/api/pub-view-account/info",
				"method": "put",
				"contentType": "",
			}
		},
	}
}
