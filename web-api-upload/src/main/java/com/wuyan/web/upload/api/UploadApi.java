package com.wuyan.web.upload.api;

import cn.hutool.core.io.FileUtil;
import com.wuyan.helper.kit.*;
import com.wuyan.web.aop.ApiLogAnnotation;
import com.wuyan.web.entity.PubConfig;
import com.wuyan.web.entity.PubFile;
import com.wuyan.web.form.PubFileForm;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.PubConfigHelper;
import com.wuyan.web.helper.WuyanWebProperties;
import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.helper.rep.RepBody;
import com.wuyan.web.helper.rep.RepCodeEnum;
import com.wuyan.web.helper.rep.RepHelper;
import com.wuyan.web.service.PubConfigService;
import com.wuyan.web.service.PubFileService;
import com.wuyan.web.upload.helper.UploadRequestMapping;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 文件上传
 *
 * @author wuyan
 */

@Slf4j
@RestController
@UploadRequestMapping("/api")
public class UploadApi extends BaseApi implements RepHelper {

    private static final String CONFIG_TYPE_SYSTEM_NAME = "system";
    private static final String CONFIG_KEY_FILE_SERVER_BASE_NAME = "file-server-base";
    private static final String CONFIG_KEY_ALLOW_FILE_TYPE_NAME = "allow-file-type";
    private static final String CONFIG_KEY_FILE_SAVE_PATH_NAME = "file-save-path";

    @Autowired
    private PubFileService pubFileService;

    @Autowired
    private WuyanWebProperties wuyanWebProperties;

    @Autowired
    private PubConfigHelper configHelper;

    /**
     * 文件上传：同时支持多文件
     *
     * @return RepBody<List < PubFile>>
     */
    @PostMapping(value = "/upload")
    @ApiLogAnnotation(name = "文件上传：同时支持多文件", identity = "Upload:upload")
    public RepBody<List<PubFile>> upload(HttpServletRequest request) {
        MultipartHttpServletRequest multipartRequest;
        List<MultipartFile> files = null;

        if (request instanceof MultipartHttpServletRequest) {
            multipartRequest = (MultipartHttpServletRequest) (request);
            files = multipartRequest.getFiles("file");
        }

        List<PubFile> data = new ArrayList<>();
        AtomicInteger ok = new AtomicInteger();
        AtomicInteger failed = new AtomicInteger();
        String errMsg = null;
        LoginInfo loginInfo = getLoginInfo(request);

        if (files != null && !files.isEmpty()) {
            for (MultipartFile t : files) {
                RepBody<PubFile> pubFileRepBody = saveFile(t, loginInfo);
                if (pubFileRepBody.getCode() == RepCodeEnum.OK.getCode()) {
                    data.add(pubFileRepBody.getData());
                    ok.getAndIncrement();
                } else {
                    failed.getAndIncrement();
                    errMsg = "'" + t.getOriginalFilename() + "': " + pubFileRepBody.getMsg();
                }
            }

            RepBody<List<PubFile>> repBody = ok(data);
            if (StringUtils.isNotBlank(errMsg)) {
                repBody.setMsg(errMsg);
            } else {
                repBody.setMsg("成功：" + ok + ", 失败：" + failed);
            }

            return repBody;
        }

        return error(RepCodeEnum.ERR_UPLOAD_NULL);
    }

    /**
     * 保存
     *
     * @param file 附件
     * @return RepBody<PubFile>
     */
    private RepBody<PubFile> saveFile(MultipartFile file, LoginInfo loginInfo) {
        try {
            String sourceName = Objects.requireNonNull(file.getOriginalFilename());
            String pathRoot = getSavePathRoot();
            String date = DateHelper.getDate(new Date(), DateHelper.DATE_FORMAT_DATE);

            String suffix = FileTypeHelper.getFileType(file.getInputStream());
            if (StringUtils.isBlank(suffix)) {
                suffix = FileTypeHelper.getSuffix(sourceName);
            }

            if (!isAllow(suffix)) {
                return error(RepCodeEnum.ERR_UPLOAD_NOT_ALLOW);
            }

            String filename = RandomHelper.getFileName(FileTypeHelper.getSuffix(sourceName));

            File toFile = FileHelper.get(pathRoot + File.separator + date, filename, true);
            BufferedOutputStream bos = FileHelper.getBOS(toFile);
            bos.write(file.getBytes());
            bos.flush();
            bos.close();

            String thumbnailFileName = filename;
            if (wuyanWebProperties.getUpload().getImage().isThumbnail()) {
                thumbnailFileName = thumbnail(toFile, pathRoot + File.separator + date);
            }

            PubFileForm pubFileForm = PubFileForm.builder()
                    .path(toFile.getPath())
                    .url(getFileServerBase() + "/" + date + "/" + filename)
                    .preUrl(getFileServerBase() + "/" + date + "/" + thumbnailFileName)
                    .userId(null == loginInfo ? 0 : loginInfo.getAccount().getId())
                    .prePath(pathRoot + File.separator + date + File.separator + thumbnailFileName)
                    .sourceName(sourceName)
                    .size(file.getSize())
                    .type(suffix)
                    .typeName(suffix)
                    .build();
            PubFile pubFile = pubFileService.create(pubFileForm);

            return ok(pubFile);
        } catch (Exception e) {
            log.error("保存文件时发生错误：" + e.getMessage(), e);
        }

        return error(RepCodeEnum.UNKNOWN);
    }

    /**
     * 图片压缩处理
     *
     * @return String
     */
    private String thumbnail(File file, String savePath) {
        ImageHelper imageHelper = ImageHelper.init(wuyanWebProperties.getUpload().getImage().getThumbnailScale(),
                wuyanWebProperties.getUpload().getImage().getThumbnailSuffix());

        // 需要校验最大值
        String thumbnailFileName = file.getName();
        File thumbnailFile = FileUtil.file(savePath + File.separator + thumbnailFileName);
        long maxSize = wuyanWebProperties.getUpload().getImage().getThumbnailMax() * 1024;
        float scale = imageHelper.getScale();
        while (thumbnailFile.length() > maxSize) {
            scale = scale * wuyanWebProperties.getUpload().getImage().getThumbnailScale();
            imageHelper.setScale(scale);
            thumbnailFileName = imageHelper.scale(savePath, file.getPath()).get(0);
            thumbnailFile = FileUtil.file(savePath + File.separator + thumbnailFileName);
        }

        return thumbnailFileName;
    }

    /**
     * 获取文件服务访问地址
     *
     * @return String
     */
    private String getFileServerBase() {
        PubConfig pubConfig = configHelper.configSingle(CONFIG_TYPE_SYSTEM_NAME,
                CONFIG_KEY_FILE_SERVER_BASE_NAME,
                wuyanWebProperties.getUpload().getServerBase());
        return pubConfig.getCfgValue();
    }

    /**
     * 判断文件是否被允许上传
     *
     * @param suffix 文件后缀
     * @return boolean
     */
    private boolean isAllow(String suffix) {
        String allType = "*";
        Set<String> fileTypeAllow = getFileTypeAllow();

        final boolean[] res = {false};
        fileTypeAllow.forEach(t -> {
            if (t.equals(allType) || t.equals(suffix)) {
                res[0] = true;
            }
        });

        return res[0];
    }

    /**
     * 获取配置中允许上传的文件类型
     *
     * @return String[]
     */
    private Set<String> getFileTypeAllow() {
        List<PubConfig> list = configHelper.configs(CONFIG_TYPE_SYSTEM_NAME, CONFIG_KEY_ALLOW_FILE_TYPE_NAME);

        if (null == list || list.isEmpty()) {
            if (StringUtils.isNotBlank(wuyanWebProperties.getUpload().getAllowFileType())) {
                String[] allows = wuyanWebProperties.getUpload().getAllowFileType().split(",");

                if ("all".equals(wuyanWebProperties.getUpload().getAllowFileType())) {
                    allows[0] = "*";
                }

                return Arrays.stream(allows).collect(Collectors.toSet());
            }

            return new HashSet<>();
        }

        return list.stream()
                .filter(t -> StringUtils.isNotBlank(t.getCfgValue()))
                .map(PubConfig::getCfgValue)
                .collect(Collectors.toSet());
    }

    /**
     * 获取文件保存路径
     *
     * @return String
     */
    private String getSavePathRoot() {
        PubConfig pubConfig = configHelper.configSingle(CONFIG_TYPE_SYSTEM_NAME,
                CONFIG_KEY_FILE_SAVE_PATH_NAME,
                wuyanWebProperties.getUpload().getSavePath());
        return pubConfig.getCfgValue();
    }
}
