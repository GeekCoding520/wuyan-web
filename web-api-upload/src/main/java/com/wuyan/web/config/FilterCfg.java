package com.wuyan.web.config;

import com.wuyan.web.filter.CorsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 过滤器配置
 * 配置方式一：
 * 1.在Filter类上加@WebFilter；
 * 2. Springboot入口Class上加@ServletComponentScan(basePackages = {"filter所在包名"})
 * 多个Filter都这样配置，只要这些Filter之间没有先后依赖关系。
 * 我这里刚好有两个Filter有先后顺序要求，看到除了@WebFilter注解还有@Order这个注解。
 * 经测试程序在本地是好的，部署到远程顺序就不能保证了。没有深纠是什么原因，知道还有第二种配置方法。
 * 配置方式二：
 * 添加FilterRegistrationBean，如下。
 * RequireLoginFilter用普通的@Component标记，它本身用到的依赖autowire进去即可。
 * 经测试第二种配置Filter的Order是能保证的，order值越小，Filter越早经过。
 *
 * @author wuyan
 **/

@Configuration
public class FilterCfg {

    @Bean
    public CorsFilter corsFilter2() {
        return new CorsFilter();
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> filterRegistrationBean2() {
        FilterRegistrationBean<CorsFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(corsFilter2());
        filterRegistrationBean.addUrlPatterns("/*");
        // order的数值越小 则优先级越高
        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;
    }

}
