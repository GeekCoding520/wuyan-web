package com.wuyan.web.aop;


import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiLogAnnotation {
    String name() default "apiLogAnnotation";
    String identity() default "";
}
