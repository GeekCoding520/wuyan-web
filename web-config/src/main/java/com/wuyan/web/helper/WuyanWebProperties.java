package com.wuyan.web.helper;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 接口路径前缀配置
 *
 * @author wuyan
 */
@Component
@ConfigurationProperties(prefix = "wuyan")
@Data
public class WuyanWebProperties {

    /**
     * 基础配置
     */
    private Web web = new Web();

    /**
     * 微信配置
     */
    private Wx wx = new Wx();

    /**
     * 系统相关
     */
    private System system = new System();

    /**
     * 验证码相关
     */
    private VerifyCode verifyCode = new VerifyCode();

    /**
     * 登录相关
     */
    private Login login = new Login();

    /**
     * 表单相关配置
     */
    private Form form = new Form();

    /**
     * 文件服务配置
     */
    private Upload upload = new Upload();

    private Ws ws = new Ws();

    @Data
    public static class Web {
        /**
         * 基础接口访问服务前缀
         */
        private String basePrefix = "wuyan";

        /**
         * 基础接口访问地址
         */
        private String baseUrl = "http://127.0.0.1:8080";

        /**
         * 认证接口访问服务前缀
         */
        private String authPrefix = "auth";

        /**
         * 认证接口访问地址
         */
        private String authUrl = "http://127.0.0.1:8080";

        /**
         * 表单接口访问服务前缀
         */
        private String formPrefix = "form";

        /**
         * 表单接口访问地址
         */
        private String formUrl = "http://127.0.0.1:8080";

        /**
         * 表单接口访问服务前缀
         */
        private String uploadPrefix = "upload";

        /**
         * 表单接口访问地址
         */
        private String uploadUrl = "http://127.0.0.1:8080";
    }

    @Data
    public static class Wx {
        /**
         * appid
         */
        private String appId = "wxa1b9bc6e56fcca80";

        /**
         * 密钥
         */
        private String secret = "ce9789afca714dfb60f25a411e9ff319";
    }

    @Data
    public static class System {
        /**
         * token过期时间,单位秒
         */
        private String tokenExpiresIn = "7200";
    }

    @Data
    public static class VerifyCode {
        /**
         * 验证码长度
         */
        private int len = 4;

        /**
         * 是否包含字母
         */
        private boolean inLetter = true;

        /**
         * 有效时间,单位秒
         */
        private int expires = 300;
    }

    @Data
    public static class Login {
        /**
         * 是否启用验证码校验
         */
        private boolean verifyCode = false;
    }

    @Data
    public static class Form {
        /**
         * 校验是否存在
         */
        private boolean check = true;

        /**
         * 分词存储功能， 将耗费双倍的存储空间
         */
        private boolean word = false;

        /**
         * 分词所具备的最小字符数
         */
        private int wordLen = 6;

        /**
         * 分词存储的位置：local（同级存储），new（独立存储），all（local和new的共存）
         */
        private String wordStoragePath = "local";

        /**
         * 全文匹配检索(启用此功能会将所有信息合成到单个属性中)
         */
        private Boolean fullText = true;
    }

    @Data
    public static class Upload {
        /**
         * 文件保存根路径
         */
        private String savePath = "C:\\tests\\files";

        /**
         * 文件访问地址
         */
        private String serverBase = "http://127.0.0.1/files/";

        /**
         * 允许上传的文件类型
         */
        private String allowFileType = "all";

        /**
         * 上传图片配置
         */
        private Image image = new Image();

        @Data
        public static class Image {
            /**
             * 是否启用缩略图
             */
            private boolean thumbnail = true;

            /**
             * 允许缩略图文件的最大值(单位KB)
             */
            private long thumbnailMax = 200;

            /**
             * 缩放比例
             */
            private float thumbnailScale = 0.8f;

            /**
             * 缩略图标识
             */
            private String thumbnailSuffix = "thumbnail";
        }
    }

    /**
     * WS配置
     */
    @Data
    public static class Ws {
        /**
         * 启用认证
         */
        private boolean auth = false;
    }
}
