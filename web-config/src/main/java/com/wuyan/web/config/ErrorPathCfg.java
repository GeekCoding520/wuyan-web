package com.wuyan.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 请求不存在的处理
 */

@RestController
@Slf4j
public class ErrorPathCfg {

    @GetMapping("/error")
    public Object handleError(HttpServletRequest request) {
        log.error("no found page -> " + request.getRequestURL());
        return "404";
    }
}
