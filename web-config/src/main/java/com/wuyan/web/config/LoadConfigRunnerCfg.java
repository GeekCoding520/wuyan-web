package com.wuyan.web.config;

import com.wuyan.web.entity.PubConfig;
import com.wuyan.web.helper.PubConfigHelper;
import com.wuyan.web.repo.PubConfigRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 启动配置
 *
 * @author wuyan
 */

@Slf4j
@Component
public class LoadConfigRunnerCfg implements CommandLineRunner {

    @Autowired
    private PubConfigHelper configHelper;

    @Autowired
    private PubConfigRepo pubConfigRepo;

    @Override
    public void run(String... args) throws Exception {
        initConfig();
    }

    /**
     * 初始化配置
     */
    private void initConfig() {
        List<PubConfig> data = pubConfigRepo.findAll();
        configHelper.setConfigs(data);
    }
}
