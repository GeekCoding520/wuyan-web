package com.wuyan.web.config;

import com.wuyan.web.helper.entity.EntityFieldInfo;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实体关系配置
 *
 * @author wuyan
 */

@Data
public class EntityCfg {

    private EntityCfg() {
    }

    /**
     * 执行批量插入时需要先清空的操作
     * key: 实体类类名
     * value: 清空条件，字段描述
     */
    public static final Map<String, EntityFieldInfo<Integer>> ER_BATCH_INSERT_ON_CLEAR = new HashMap<>();

    static {
        Map<String, EntityFieldInfo<Integer>> tmpMap = new HashMap<>();

        tmpMap.put("pubAccountRole", EntityFieldInfo.<Integer>builder().name("userId").clz(Integer.class).build());
        tmpMap.put("pubRoleFunction", EntityFieldInfo.<Integer>builder().name("roleId").clz(Integer.class).build());
        tmpMap.put("pubRoleApi", EntityFieldInfo.<Integer>builder().name("roleId").clz(Integer.class).build());

        ER_BATCH_INSERT_ON_CLEAR.putAll(tmpMap);
    }

    /* *************************************************************************** */
    /* *************************************************************************** */
    /**
     * 数据加密操作
     * key: 实体类类名
     * value: 清空条件，字段描述
     * EntityFieldInfo.additional： 加密方法
     */
    public static final Map<String, List<EntityFieldInfo<?>>> DATA_ENCRYPTION = new HashMap<>();

    static {
        Map<String, List<EntityFieldInfo<?>>> tmpMap = new HashMap<>();

        ArrayList<EntityFieldInfo<?>> pubAccount = new ArrayList<>();
        pubAccount.add(EntityFieldInfo.<String>builder()
                .name("password")
                .clz(String.class)
                .additional("unReversible:md5")
                .build());
        tmpMap.put("pubAccount", pubAccount);

        DATA_ENCRYPTION.putAll(tmpMap);
    }
}
