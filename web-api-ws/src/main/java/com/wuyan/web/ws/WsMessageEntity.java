package com.wuyan.web.ws;

import lombok.*;

/**
 * 消息收发实体类
 */

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WsMessageEntity {
    /**
     * 消息ID
     */
    private String id;

    /**
     * 消息类型
     */
    private String type;

    /**
     * 描述
     */
    private String msg;

    /**
     * 附加数据
     */
    private Object data;

    public WsMessageEntity(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public WsMessageEntity(String id, String type, Object data) {
        this.id = id;
        this.type = type;
        this.data = data;
    }
}
