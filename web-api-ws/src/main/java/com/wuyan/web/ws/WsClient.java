package com.wuyan.web.ws;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.RandomHelper;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.SpringContextHelper;
import com.wuyan.web.helper.auth.LoginInfo;
import com.wuyan.web.repo.extend.PubMsgRepoExtend;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.yeauty.pojo.Session;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Slf4j
@Component
// 多例注解
@Scope("prototype")
public class WsClient implements WsSubProtocol {

    @Autowired
    private BaseApi baseApi;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PubMsgRepoExtend pubMsgExtendRepo;

    /**
     * 存储所有已接入节点
     * key：session.id
     * value: WsClient
     */
    private static final ConcurrentHashMap<String, WsClient> CLIENTS = new ConcurrentHashMap<>();

    /**
     * 消息监听组
     */
    private static final Set<WsMsgListen> WS_MSG_LISTENS = new HashSet<>();

    /**
     * 链接唯一标识符，一般用session.id
     */
    private String id;

    /**
     * 登录凭证
     */
    private String token;

    /**
     * 会话信息
     */
    private Session session;

    /**
     * 自定义子协议
     */
    private String subProtocol;

    /**
     * 用户信息
     */
    private LoginInfo loginInfo;

    /**
     * 登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime loginTime;

    /**
     * 刷新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime refreshTime;

    /**
     * 下线时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private LocalDateTime unLineTime;

    /**
     * 校验本子协议是否存在
     *
     * @return boolean
     */
    public boolean isSubProtocol() {
        return isSubProtocol(getSubProtocol());
    }

    /**
     * 取所有token的节点
     *
     * @param token token
     * @return Set<WsClient>
     */
    public static Set<WsClient> getClientsByToken(String token) {
        if (StringUtils.isBlank(token)) return new HashSet<>();
        return CLIENTS.values().stream().filter(t -> token.equals(t.getToken())).collect(Collectors.toSet());
    }

    /**
     * 取所有subProtocol的节点
     *
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> getClientsBySubProtocol(String subProtocol) {
        if (StringUtils.isBlank(subProtocol)) return new HashSet<>();
        return CLIENTS.values().stream().filter(t -> subProtocol.equals(t.getSubProtocol())).collect(Collectors.toSet());
    }

    /**
     * 取所有token&subProtocol的节点
     *
     * @param token       token
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> getClients(String token, String subProtocol) {
        if (StringUtils.isBlank(token) || StringUtils.isBlank(subProtocol)) {
            return new HashSet<>();
        }

        return CLIENTS.values().stream()
                .filter(t -> token.equals(t.getToken()) && subProtocol.equals(t.getSubProtocol()))
                .collect(Collectors.toSet());
    }

    /**
     * 取所有userid&subProtocol的节点
     *
     * @param userId      用户
     * @param subProtocol 子协议
     * @return Set<WsClient>
     */
    public static Set<WsClient> getClients(Integer userId, String subProtocol) {
        if (null == userId || StringUtils.isBlank(subProtocol)) {
            return new HashSet<>();
        }

        return CLIENTS.values().stream()
                .filter(t -> {
                    LoginInfo loginInfo = t.getLoginInfo();

                    if (null == loginInfo || null == loginInfo.getAccount()) {
                        return false;
                    }

                    return subProtocol.equals(t.getSubProtocol());
                })
                .collect(Collectors.toSet());
    }

    /**
     * 取所有id的节点
     *
     * @param id 唯一性，一般用session.id
     * @return WsClient
     */
    public static WsClient getClient(String id) {
        return CLIENTS.get(id);
    }

    /**
     * 新增节点
     *
     * @param key      唯一性，一般用session.id
     * @param wsClient 节点描述
     * @return WsClient
     */
    public static WsClient put(String key, WsClient wsClient) {
        return CLIENTS.put(key, wsClient);
    }

    /**
     * 创建节点
     *
     * @param key         唯一性，一般用session.id
     * @param session     会话
     * @param token       token
     * @param subProtocol subProtocol
     * @return WsClient
     */
    public static WsClient create(String key, Session session, String token, String subProtocol, LoginInfo loginInfo) {
        WsClient wsClient = SpringContextHelper.getBean(WsClient.class);
        wsClient.setId(key);
        wsClient.setSession(session);
        wsClient.setToken(token);
        wsClient.setSubProtocol(subProtocol);
        wsClient.setLoginInfo(loginInfo);
        wsClient.setLoginTime(LocalDateTime.now());
        wsClient.setRefreshTime(LocalDateTime.now());
        return put(key, wsClient);
    }

    /**
     * 发送消息
     *
     * @param key 唯一性，一般用session.id
     * @param msg 消息内容
     */
    public static void sendMsg(String key, String msg) {
        WsClient client = getClient(key);
        client.session.sendText(msg);
    }

    /**
     * 发送消息
     *
     * @param key 唯一性，一般用session.id
     * @param msg 消息内容
     */
    public static void sendMsg(String key, byte[] msg) {
        WsClient client = getClient(key);
        client.session.sendBinary(msg);
    }

    /**
     * 消息通知
     *
     * @param session 会话
     * @param message 消息内容
     */
    public static void noticeListen(Session session, WsMessageEntity message, String subProtocol) {
        WS_MSG_LISTENS.stream()
                .filter(t -> StringUtils.isBlank(t.getSubProtocol()) || t.getSubProtocol().equals(subProtocol))
                .forEach(t -> t.readMsg(session, message));
    }

    /**
     * 消息通知
     *
     * @param session 会话
     * @param bytes   消息内容
     */
    public static void noticeListen(Session session, byte[] bytes, String subProtocol) {
        WS_MSG_LISTENS
                .stream().filter(t -> t.getSubProtocol().equals(subProtocol) || StringUtils.isBlank(t.getSubProtocol()))
                .forEach(t -> t.readMsg(session, bytes));
    }

    /**
     * 移除
     *
     * @param key 唯一性，一般用session.id
     * @return WsClient
     */
    public static WsClient remove(String key) {
        WsClient client = CLIENTS.remove(key);
        if (null != client) {
            client.setUnLineTime(LocalDateTime.now());
        }
        return client;
    }

    /**
     * 移除，根据token
     *
     * @param token token
     * @return Set<WsClient>
     */
    public static Set<WsClient> removeByToken(String token) {
        Set<WsClient> clients = getClientsByToken(token);
        clients.forEach(t -> remove(t.getId()));
        return clients;
    }

    /**
     * 移除，根据subProtocol
     *
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> removeBySubProtocol(String subProtocol) {
        Set<WsClient> clients = getClientsBySubProtocol(subProtocol);
        clients.forEach(t -> remove(t.getId()));
        return clients;
    }

    /**
     * 移除，根据subProtocol&token
     *
     * @param token       token
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> removes(String token, String subProtocol) {
        Set<WsClient> clients = getClients(token, subProtocol);
        clients.forEach(t -> remove(t.getId()));
        return clients;
    }

    /**
     * 关闭节点
     *
     * @param key 唯一性，一般用session.id
     */
    public static void close(String key) {
        WsClient client = getClient(key);
        client.getSession().close();
    }

    /**
     * 关闭节点，根据token
     *
     * @param token token
     * @return Set<WsClient>
     */
    public static Set<WsClient> closeByToken(String token) {
        Set<WsClient> clients = getClientsByToken(token);
        clients.forEach(t -> close(t.getId()));
        return clients;
    }

    /**
     * 关闭节点，根据subProtocol
     *
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> closeBySubProtocol(String subProtocol) {
        Set<WsClient> clients = getClientsBySubProtocol(subProtocol);
        clients.forEach(t -> close(t.getId()));
        return clients;
    }

    /**
     * 关闭节点，根据subProtocol&token
     *
     * @param token       token
     * @param subProtocol subProtocol
     * @return Set<WsClient>
     */
    public static Set<WsClient> closes(String token, String subProtocol) {
        Set<WsClient> clients = getClients(token, subProtocol);
        clients.forEach(t -> close(t.getId()));
        return clients;
    }

    /**
     * 注册消息监听器
     *
     * @param wsMsgListen 监听组
     */
    public static void register(WsMsgListen... wsMsgListen) {
        assert null != wsMsgListen : "无效的监听组";
        WS_MSG_LISTENS.addAll(Arrays.stream(wsMsgListen).collect(Collectors.toSet()));
    }

    /**
     * 移除监听器
     *
     * @param wsMsgListen 监听组
     */
    public static void clearListen(WsMsgListen... wsMsgListen) {
        assert null != wsMsgListen : "无效的监听组";
        WS_MSG_LISTENS.removeAll(Arrays.stream(wsMsgListen).collect(Collectors.toSet()));
    }

    /**
     * 创建一个消息ID
     *
     * @return String
     */
    public static String createMsgId() {
        return RandomHelper.createAccessToken();
    }

    /**
     * 创建消息并发送
     *
     * @param key           接收者
     * @param messageEntity 消息主体
     */
    public static void createMsgAndSend(String key, WsMessageEntity messageEntity) {
        WsClient client = getClient(key);

        try {
            sendMsg(key, client.mapper.writeValueAsString(messageEntity));
        } catch (JsonProcessingException e) {
            log.error("数据格式错误：{}", messageEntity);
        }
    }

    /**
     * 创建消息并发送
     *
     * @param key  接收者
     * @param type 消息类型
     * @param data 附加数据
     */
    public static void createMsgAndSend(String key, String type, Object data) {
        WsMessageEntity messageEntity = WsMessageEntity.builder()
                .id(createMsgId())
                .type(type)
                .msg(null)
                .data(data)
                .build();
        createMsgAndSend(key, messageEntity);
    }

    /**
     * 创建消息并发送
     *
     * @param key  接收者
     * @param type 消息类型
     * @param data 附加数据
     * @param msg  描述
     */
    public static void createMsgAndSend(String key, String type, Object data, String msg) {
        WsMessageEntity messageEntity = WsMessageEntity.builder()
                .id(createMsgId())
                .type(type)
                .msg(msg)
                .data(data)
                .build();
        createMsgAndSend(key, messageEntity);
    }

    /**
     * 广播
     *
     * @param token         Token
     * @param messageEntity 消息
     */
    public static void broadcastByToken(String token, WsMessageEntity messageEntity) {
        Set<WsClient> clients = getClientsByToken(token);
        clients.forEach(t -> createMsgAndSend(t.getId(), messageEntity));
    }

    /**
     * 广播
     *
     * @param subProtocol   subProtocol
     * @param messageEntity 消息
     */
    public static void broadcastBySubProtocol(String subProtocol, WsMessageEntity messageEntity) {
        Set<WsClient> clients = getClientsBySubProtocol(subProtocol);
        clients.forEach(t -> createMsgAndSend(t.getId(), messageEntity));
    }

    /**
     * 广播
     *
     * @param userId        账户
     * @param subProtocol   子协议
     * @param messageEntity 消息
     */
    public static void broadcast(Integer userId, String subProtocol, WsMessageEntity messageEntity) {
        Set<WsClient> clients = getClients(userId, subProtocol);
        clients.forEach(t -> createMsgAndSend(t.getId(), messageEntity));
    }

    /**
     * 广播
     *
     * @param subProtocol   subProtocol
     * @param token         Token
     * @param messageEntity 消息
     */
    public static void broadcast(String subProtocol, String token, WsMessageEntity messageEntity) {
        Set<WsClient> clients = getClients(token, subProtocol);
        clients.forEach(t -> createMsgAndSend(t.getId(), messageEntity));
    }

    /**
     * 广播
     *
     * @param messageEntity 消息
     */
    public static void broadcast(WsMessageEntity messageEntity) {
        CLIENTS.values().forEach(t -> createMsgAndSend(t.getId(), messageEntity));
    }

    /**
     * 消息处理
     *
     * @param session 会话
     * @param message 消息
     */
    public static void handleMsg(Session session, String message) {
        WsClient client = WsClient.getClient(session.id().asLongText());
        log.debug("{}-{}-{}, recv: {}", session.id().asLongText(), client.getToken(), client.getSubProtocol(), message);

        try {
            WsMessageEntity wsMessageEntity = client.mapper.readValue(message, new TypeReference<>() {
            });

            // 属于预定义协议的消息
            if (client.isSubProtocol()) {
                LoginInfo loginInfo = client.getBaseApi().getLoginInfo(client.getToken(), null);

                switch (client.getSubProtocol()) {
                    // 登录状态校验
                    case AUTH:
                        if (null == loginInfo) {
                            createMsgAndSend(session.id().asLongText(), WsMessageType.STATE_EXPIRE, false);
                            session.close();
                            return;
                        }

                        client.setLoginInfo(loginInfo);
                        client.setRefreshTime(LocalDateTime.now());

                        // 回复
                        WsClient.createMsgAndSend(session.id().asLongText(), WsMessageEntity.builder()
                                .id(wsMessageEntity.getId())
                                .type(wsMessageEntity.getType())
                                .data(loginInfo)
                                .build());
                        break;
                    // 默认
                    case DEF_COMMON:
                        // 未读消息统计
                        if (WsMessageType.NO_READ_MSG.equals(wsMessageEntity.getType())) {
                            if (null == loginInfo) {
                                createMsgAndSend(session.id().asLongText(), WsMessageType.STATE_EXPIRE, 0);
                                return;
                            }

                            // 取出未读消息数
                            int count = client.getPubMsgExtendRepo()
                                    .countAllByRecvUserIdAndReadTimeIsNull(loginInfo.getAccount().getId());

                            // 回复
                            WsClient.createMsgAndSend(session.id().asLongText(), WsMessageEntity.builder()
                                    .id(wsMessageEntity.getId())
                                    .type(wsMessageEntity.getType())
                                    .data(count)
                                    .build());
                        }
                        break;
                    default:

                        break;
                }
            }

            // 特殊的消息类型处理(转接消息)
            if (WsMessageType.TRANSFER_TO.equals(wsMessageEntity.getType())) {
                if (null != wsMessageEntity.getData()) {
                    // 解析接收方
                    String userIds = wsMessageEntity.getMsg();
                    List<Integer> ids = client.mapper.readValue(userIds, new TypeReference<>() {
                    });

                    // 对每一个人发送消息
                    ids.forEach(t -> {
                        Set<WsClient> clients = getClients(t, client.getSubProtocol());
                        clients.forEach(c -> {
                            if (wsMessageEntity.getData() instanceof String) {
                                sendMsg(c.getId(), wsMessageEntity.getData().toString());
                            } else {
                                try {
                                    sendMsg(c.getId(), c.getMapper().writeValueAsString(wsMessageEntity.getData()));
                                } catch (JsonProcessingException e) {
                                    log.error(e.getMessage(), e);
                                }
                            }
                        });
                    });
                }
            }

            WsClient.noticeListen(session, wsMessageEntity, client.getSubProtocol());
        } catch (JsonProcessingException e) {
            log.error("{}-{}-{}, 错误的消息格式: {}", session.id().asLongText(), client.getToken(), client.getSubProtocol(), message, e);
        }
    }
}
