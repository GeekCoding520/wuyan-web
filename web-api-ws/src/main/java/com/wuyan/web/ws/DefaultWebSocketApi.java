package com.wuyan.web.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.WuyanWebProperties;
import com.wuyan.web.helper.auth.LoginInfo;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.yeauty.annotation.*;
import org.yeauty.pojo.Session;

import java.io.IOException;

import static com.wuyan.web.ws.WsSubProtocol.DEF_COMMON;

/**
 * websocket服务
 */

@ServerEndpoint(port = "${wuyan.ws.port}", path = "${wuyan.ws.path}/{token}")
@Component
@Slf4j
public class DefaultWebSocketApi extends BaseApi {

    @Autowired
    private WuyanWebProperties wuyanWebProperties;

    @Autowired
    private ObjectMapper mapper;

    /**
     * 当有新的连接进入时，对该方法进行回调
     *
     * @param session 会话信息
     * @param headers 请求头部
     * @param token   TOKEN
     */
    @BeforeHandshake
    public void handshake(Session session, HttpHeaders headers, @PathVariable String token) {
        String subProtocol = headers.get("Sec-WebSocket-Protocol");

        if (StringUtils.isBlank(subProtocol)) {
            subProtocol = DEF_COMMON;
        }
        session.setSubprotocols(subProtocol);

        log.info("handshake: {}, token: {}, sub protocol: {}", session.id().asLongText(), token, subProtocol);
    }

    /**
     * 当有新的WebSocket连接完成时，对该方法进行回调
     *
     * @param session 会话信息
     * @param headers 请求头部
     * @param token   TOKEN
     */
    @OnOpen
    public void onOpen(Session session, HttpHeaders headers, @PathVariable String token) {
        LoginInfo loginInfo = getLoginInfo(token, null);
        if (wuyanWebProperties.getWs().isAuth()) {
            if (null == loginInfo) {
                WsMessageEntity messageEntity = WsMessageEntity.builder()
                        .id(WsClient.createMsgId())
                        .type(WsMessageType.AUTH_FAILED)
                        .build();

                try {
                    session.sendText(mapper.writeValueAsString(messageEntity));
                } catch (JsonProcessingException e) {
                    log.error("消息格式错误：{}", messageEntity);
                }

                session.close();
                return;
            }
        }

        String subProtocol = headers.get("Sec-WebSocket-Protocol");
        if (StringUtils.isBlank(subProtocol)) {
            subProtocol = DEF_COMMON;
        }

        WsClient.create(session.id().asLongText(), session, token, subProtocol, loginInfo);

        log.info("connection: {}, token: {}, sub protocol: {}", session.id().asLongText(), token, subProtocol);
    }

    /**
     * 当有WebSocket连接关闭时，对该方法进行回调
     *
     * @param session 会话信息
     * @throws IOException 异常
     */
    @OnClose
    public void onClose(Session session) throws IOException {
        WsClient client = WsClient.remove(session.id().asLongText());
        if (null == client) return;

        log.info("closed: {}, token: {}, sub protocol: {}", session.id().asLongText(), client.getToken(), client.getSubProtocol());
    }

    /**
     * 当有WebSocket抛出异常时，对该方法进行回调
     *
     * @param session   会话
     * @param throwable 异常信息
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        session.close();
        log.error(throwable.getMessage(), throwable);
    }

    /**
     * 当接收到字符串消息时，对该方法进行回调
     *
     * @param session 会话
     * @param message 消息内容
     */
    @OnMessage
    public void onMessage(Session session, String message) {
        WsClient.handleMsg(session, message);
    }

    /**
     * 当接收到二进制消息时，对该方法进行回调
     *
     * @param session 会话
     * @param bytes   二进制数据
     */
    @OnBinary
    public void onBinary(Session session, byte[] bytes) {
        WsClient client = WsClient.getClient(session.id().asLongText());
        log.debug("{}-{}-{}, recv: {}", session.id().asLongText(), client.getToken(), client.getSubProtocol(), "二进制数据");

        WsClient.noticeListen(session, bytes, client.getSubProtocol());
    }

    /**
     * 当接收到Netty的事件时，对该方法进行回调
     *
     * @param session 会话
     * @param evt     事件
     */
    @OnEvent
    public void onEvent(Session session, Object evt) {
        WsClient client = WsClient.getClient(session.id().asLongText());

        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            switch (idleStateEvent.state()) {
                case READER_IDLE:
                    log.info("{}-{}-{}, read idle", session.id().asLongText(), client.getToken(), client.getSubProtocol());
                    break;
                case WRITER_IDLE:
                    log.info("{}-{}-{}, write idle", session.id().asLongText(), client.getToken(), client.getSubProtocol());
                    break;
                case ALL_IDLE:
                    log.info("{}-{}-{}, all idle", session.id().asLongText(), client.getToken(), client.getSubProtocol());
                    session.close();
                    break;
                default:
                    break;
            }
        }
    }

}
