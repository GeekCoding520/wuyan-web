package com.wuyan.web.ws;

import org.yeauty.pojo.Session;

/**
 * 消息监听，当有新的消息时，触发
 */

public interface WsMsgListen {

    /**
     * 子协议
     *
     * @return String
     */
    String getSubProtocol();

    /**
     * 接收消息-字符串
     *
     * @param session 会话
     * @param message 消息内容
     */
    void readMsg(Session session, WsMessageEntity message);

    /**
     * 接收消息-字节
     *
     * @param session 会话
     * @param bytes   字节数据
     */
    void readMsg(Session session, byte[] bytes);
}
