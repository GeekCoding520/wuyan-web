package com.wuyan.web.ws;

/**
 * 消息类型
 */

public interface WsMessageType {
    /**
     * 刷新操作
     */
    String REFRESH = "refresh";

    /**
     * 正常关闭
     */
    String CLOSE = "close";

    /**
     * 认证失败，强制关闭
     */
    String AUTH_FAILED = "auth-failed";

    /**
     * 登录状态过期
     */
    String STATE_EXPIRE = "state-expire";

    /**
     * 中转,对于此类型,需要在msg中绑定转接对象
     */
    String TRANSFER_TO = "to";

    /**
     * 转接到管理层
     */
    String MANAGER_TRANSFER_TO = "to-manager";

    /**
     * 未读消息统计
     */
    String NO_READ_MSG = "no-read-msg";
}
