package com.wuyan.web.ws;

/**
 * 子协议定义
 */

public interface WsSubProtocol {
    String AUTH = "auth";
    String DEF_COMMON = "def-common";

    /**
     * 校验子协议是否存在
     *
     * @param subProtocol 子协议
     * @return boolean
     */
    default boolean isSubProtocol(String subProtocol) {
        return AUTH.equals(subProtocol)
                || DEF_COMMON.equals(subProtocol);
    }
}
