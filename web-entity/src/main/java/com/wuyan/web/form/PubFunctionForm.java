package com.wuyan.web.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * form for PubFunction generated by jpa-codegen
 * codegen auto
 *
 * @author wuyan
 * @date 2021-09-14 14:40:35
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubFunctionForm {


    /**
    * parentId
    */
    private Integer parentId;


    /**
    * type
    */
    private Integer type;


    /**
    * name
    */
    private String name;


    /**
    * icon
    */
    private String icon;


    /**
    * identity
    */
    private String identity;


    /**
    * url
    */
    private String url;


    /**
    * clickFun
    */
    private String clickFun;


    /**
    * status
    */
    private Integer status;


    /**
    * cfgSort
    */
    private Integer cfgSort;


    /**
    * remark
    */
    private String remark;

}