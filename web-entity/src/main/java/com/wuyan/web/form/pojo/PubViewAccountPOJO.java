package com.wuyan.web.form.pojo;

import com.wuyan.helper.kit.EntityDescAnno;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户账户信息
 * 
 * @author wuyan
 */

@EntityDescAnno(value = "用户账户信息视图")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubViewAccountPOJO {

    /**
     * 邮箱 varchar(32)
     */
    private String email;

    /**
    /**
     * 用户昵称 varchar(64)
     */
    private String nickname;

    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 tinyint(2) unsigned
     */
    private Integer sex;

    /**
     * 用户头像 varchar(255)
     */
    private String img;

    /**
     * 备注 varchar(255)
     */
    private String remark;
}
