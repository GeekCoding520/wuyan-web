package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 功能
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_function")
@EntityDescAnno(value = "功能")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubFunction {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 父级 int(11) unsigned
     */
    @Column(name = "parent_id")
    @EntityDescAnno(value = "父级", type = "int", len = 11)
    private Integer parentId;

    /**
     * 菜单类型：1（目录）；2（菜单）；3（按钮） tinyint(1) unsigned
     */
    @Column(name = "type")
    @EntityDescAnno(value = "菜单类型", type = "tinyint", len = 1)
    private Integer type;

    /**
     * 功能名称 varchar(20)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "功能名称", type = "varchar", len = 20)
    private String name;

    /**
     * 图标：可以是URL，或者是本地已有图标 varchar(255)
     */
    @Column(name = "icon")
    @EntityDescAnno(value = "图标", type = "varchar", len = 255)
    private String icon;

    /**
     * 权限名 varchar(64)
     */
    @Column(name = "identity")
    @EntityDescAnno(value = "权限名", type = "varchar", len = 64)
    private String identity;

    /**
     * 访问路径：当存在下级菜单时为空 varchar(255)
     */
    @Column(name = "url")
    @EntityDescAnno(value = "访问路径", type = "varchar", len = 255)
    private String url;

    /**
     * 自定义函数 varchar(1024)
     */
    @Column(name = "click_fun")
    @EntityDescAnno(value = "自定义函数", type = "varchar", len = 1024)
    private String clickFun;

    /**
     * 状态：关闭（0），开启（1） tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 排序 smallint(3) unsigned
     */
    @Column(name = "cfg_sort")
    @EntityDescAnno(value = "排序", type = "smallint", len = 3)
    private Integer cfgSort;

    /**
     * 是否可修改：是（1），否（0） int(11) unsigned
     */
    @Column(name = "cfg_modify")
    @EntityDescAnno(value = "是否可修改", type = "int", len = 11)
    private Integer cfgModify;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
