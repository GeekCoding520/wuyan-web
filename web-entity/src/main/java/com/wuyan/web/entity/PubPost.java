package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 岗位、职能表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_post")
@EntityDescAnno(value = "岗位、职能表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubPost {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 岗位名称 varchar(32)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "岗位名称", type = "varchar", len = 32)
    private String name;

    /**
     * 岗位编码 varchar(32)
     */
    @Column(name = "code")
    @EntityDescAnno(value = "岗位编码", type = "varchar", len = 32)
    private String code;

    /**
     * 行政级别 tinyint(2) unsigned
     */
    @Column(name = "executive_level")
    @EntityDescAnno(value = "行政级别", type = "tinyint", len = 2)
    private Integer executiveLevel;

    /**
     * 所属部门 int(11) unsigned
     */
    @Column(name = "dept")
    @EntityDescAnno(value = "所属部门", type = "int", len = 11)
    private Integer dept;

    /**
     * 排序 int(3) unsigned
     */
    @Column(name = "cfg_sort")
    @EntityDescAnno(value = "排序", type = "int", len = 3)
    private Integer cfgSort;

    /**
     * 状态 tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 更新时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "更新时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

}
