package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 文章阅读记录
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_article_read")
@EntityDescAnno(value = "文章阅读记录")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubArticleRead {

    /**
     * ID int(11)
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "ID", type = "int", len = 11)
    private Integer id;

    /**
     * 用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "用户ID", type = "int", len = 11)
    private Integer userId;

    /**
     * 文章主键 int(11) unsigned
     */
    @Column(name = "article_id")
    @EntityDescAnno(value = "文章主键", type = "int", len = 11)
    private Integer articleId;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
