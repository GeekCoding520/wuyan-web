package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 文章评论记录
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_article_comment")
@EntityDescAnno(value = "文章评论记录")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubArticleComment {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 文章ID int(11) unsigned
     */
    @Column(name = "article_id")
    @EntityDescAnno(value = "文章ID", type = "int", len = 11)
    private Integer articleId;

    /**
     * 内容 varchar(255)
     */
    @Column(name = "content")
    @EntityDescAnno(value = "内容", type = "varchar", len = 255)
    private String content;

    /**
     * 评论目标ID int(11) unsigned
     */
    @Column(name = "target_id")
    @EntityDescAnno(value = "评论目标ID", type = "int", len = 11)
    private Integer targetId;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 评论者 int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "评论者", type = "int", len = 11)
    private Integer userId;

}
