package com.wuyan.web.entity;

import javax.persistence.*;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 用户基本信息
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_user")
@EntityDescAnno(value = "用户基本信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubUser {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 用户昵称 varchar(64)
     */
    @Column(name = "nickname")
    @EntityDescAnno(value = "用户昵称", type = "varchar", len = 64)
    private String nickname;

    /**
     * 用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "用户ID", type = "int", len = 11)
    private Integer userId;

    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 tinyint(2) unsigned
     */
    @Column(name = "sex")
    @EntityDescAnno(value = "用户的性别", type = "tinyint", len = 2)
    private Integer sex;

    /**
     * 省份 varchar(60)
     */
    @Column(name = "province")
    @EntityDescAnno(value = "省份", type = "varchar", len = 60)
    private String province;

    /**
     * 所在市 varchar(60)
     */
    @Column(name = "city")
    @EntityDescAnno(value = "所在市", type = "varchar", len = 60)
    private String city;

    /**
     * 所在国家 varchar(60)
     */
    @Column(name = "country")
    @EntityDescAnno(value = "所在国家", type = "varchar", len = 60)
    private String country;

    /**
     * 所在区县 varchar(60)
     */
    @Column(name = "county")
    @EntityDescAnno(value = "所在区县", type = "varchar", len = 60)
    private String county;

    /**
     * 用户头像 varchar(255)
     */
    @Column(name = "img")
    @EntityDescAnno(value = "用户头像", type = "varchar", len = 255)
    private String img;

}
