package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 问题反馈
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_feedback")
@EntityDescAnno(value = "问题反馈")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubFeedback {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 标题 varchar(64)
     */
    @Column(name = "title")
    @EntityDescAnno(value = "标题", type = "varchar", len = 64)
    private String title;

    /**
     * 分类名称 varchar(32)
     */
    @Column(name = "type_name")
    @EntityDescAnno(value = "分类名称", type = "varchar", len = 32)
    private String typeName;

    /**
     * 评分 smallint(3) unsigned
     */
    @Column(name = "score")
    @EntityDescAnno(value = "评分", type = "smallint", len = 3)
    private Integer score;

    /**
     * 联系方式 varchar(32)
     */
    @Column(name = "contact")
    @EntityDescAnno(value = "联系方式", type = "varchar", len = 32)
    private String contact;

    /**
     * 文件资料 text
     */
    @Column(name = "img")
    @EntityDescAnno(value = "文件资料", type = "text", len = -1)
    private String img;

    /**
     * 主体内容 text
     */
    @Column(name = "content")
    @EntityDescAnno(value = "主体内容", type = "text", len = -1)
    private String content;

    /**
     * 回复内容 text
     */
    @Column(name = "reply")
    @EntityDescAnno(value = "回复内容", type = "text", len = -1)
    private String reply;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 回复时间 timestamp
     */
    @Column(name = "reply_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "回复时间", type = "timestamp", len = -1)
    private LocalDateTime replyTime;

}
