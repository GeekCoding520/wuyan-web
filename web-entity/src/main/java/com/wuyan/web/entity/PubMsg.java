package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 消息表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_msg")
@EntityDescAnno(value = "消息表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubMsg {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 发送方用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "发送方用户ID", type = "int", len = 11)
    private Integer userId;

    /**
     * 接收方用户ID int(11) unsigned
     */
    @Column(name = "recv_user_id")
    @EntityDescAnno(value = "接收方用户ID", type = "int", len = 11)
    private Integer recvUserId;

    /**
     * 发送内容 varchar(255)
     */
    @Column(name = "content")
    @EntityDescAnno(value = "发送内容", type = "varchar", len = 255)
    private String content;

    /**
     * 消息类型 int(1) unsigned
     */
    @Column(name = "type")
    @EntityDescAnno(value = "消息类型", type = "int", len = 1)
    private Integer type;

    /**
     * 目标ID，如果需要，可以绑定为与消息相关的记录ID varchar(255)
     */
    @Column(name = "target_id")
    @EntityDescAnno(value = "目标ID", type = "varchar", len = 255)
    private String targetId;

    /**
     * 已读时间 ，不为0 表示已读 默认0 timestamp
     */
    @Column(name = "read_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "已读时间", type = "timestamp", len = -1)
    private LocalDateTime readTime;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
