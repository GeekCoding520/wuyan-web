package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 用户与角色关联表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_account_role")
@EntityDescAnno(value = "用户与角色关联表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubAccountRole {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "用户ID", type = "int", len = 11)
    private Integer userId;

    /**
     * 角色ID int(11) unsigned
     */
    @Column(name = "role_id")
    @EntityDescAnno(value = "角色ID", type = "int", len = 11)
    private Integer roleId;

    /**
     * 临时授权标志：是（1），否（0） tinyint(1) unsigned
     */
    @Column(name = "cfg_temp")
    @EntityDescAnno(value = "临时授权标志", type = "tinyint", len = 1)
    private Integer cfgTemp;

    /**
     * 临时授权：开始时间 timestamp
     */
    @Column(name = "start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "临时授权", type = "timestamp", len = -1)
    private LocalDateTime startTime;

    /**
     * 临时授权：结束时间 timestamp
     */
    @Column(name = "end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "临时授权", type = "timestamp", len = -1)
    private LocalDateTime endTime;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
