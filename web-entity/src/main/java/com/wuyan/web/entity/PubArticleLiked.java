package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 文章点赞表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_article_liked")
@EntityDescAnno(value = "文章点赞表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubArticleLiked {

    /**
     * ID int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "ID", type = "int", len = 11)
    private Integer id;

    /**
     * 文章ID int(11) unsigned
     */
    @Column(name = "article_id")
    @EntityDescAnno(value = "文章ID", type = "int", len = 11)
    private Integer articleId;

    /**
     * 点赞时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "点赞时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "用户ID", type = "int", len = 11)
    private Integer userId;

}
