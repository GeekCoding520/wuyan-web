package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 角色与功能关联
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_role_function")
@EntityDescAnno(value = "角色与功能关联")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubRoleFunction {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 角色ID int(11) unsigned
     */
    @Column(name = "role_id")
    @EntityDescAnno(value = "角色ID", type = "int", len = 11)
    private Integer roleId;

    /**
     * 功能ID int(11) unsigned
     */
    @Column(name = "function_id")
    @EntityDescAnno(value = "功能ID", type = "int", len = 11)
    private Integer functionId;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
