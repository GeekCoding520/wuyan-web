package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 接口表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_api")
@EntityDescAnno(value = "接口表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubApi {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 功能ID int(11) unsigned
     */
    @Column(name = "function_id")
    @EntityDescAnno(value = "功能ID", type = "int", len = 11)
    private Integer functionId;

    /**
     * 接口名称 varchar(32)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "接口名称", type = "varchar", len = 32)
    private String name;

    /**
     * 模块 varchar(255)
     */
    @Column(name = "modular")
    @EntityDescAnno(value = "模块", type = "varchar", len = 255)
    private String modular;

    /**
     * 请求路径：不包括域名以及模块，一般与config配置合并成一个完整的url varchar(255)
     */
    @Column(name = "url")
    @EntityDescAnno(value = "请求路径", type = "varchar", len = 255)
    private String url;

    /**
     * 请求方式：多个用英文逗号拼接。如：POST,GET varchar(255)
     */
    @Column(name = "method")
    @EntityDescAnno(value = "请求方式", type = "varchar", len = 255)
    private String method;

    /**
     * 请求内容格式 varchar(255)
     */
    @Column(name = "content_type")
    @EntityDescAnno(value = "请求内容格式", type = "varchar", len = 255)
    private String contentType;

    /**
     * 状态：启动（1）；关闭（0） tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 请求样例|模板 varchar(255)
     */
    @Column(name = "request_demo")
    @EntityDescAnno(value = "请求样例|模板", type = "varchar", len = 255)
    private String requestDemo;

    /**
     * 请求响应报文样例 varchar(255)
     */
    @Column(name = "response_demo")
    @EntityDescAnno(value = "请求响应报文样例", type = "varchar", len = 255)
    private String responseDemo;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
