package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 文章表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_article")
@EntityDescAnno(value = "文章表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubArticle {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 分类ID int(11) unsigned
     */
    @Column(name = "type_id")
    @EntityDescAnno(value = "分类ID", type = "int", len = 11)
    private Integer typeId;

    /**
     * 分类名称 varchar(20)
     */
    @Column(name = "type_name")
    @EntityDescAnno(value = "分类名称", type = "varchar", len = 20)
    private String typeName;

    /**
     * 标题 varchar(255)
     */
    @Column(name = "title")
    @EntityDescAnno(value = "标题", type = "varchar", len = 255)
    private String title;

    /**
     * 标签 varchar(255)
     */
    @Column(name = "tags")
    @EntityDescAnno(value = "标签", type = "varchar", len = 255)
    private String tags;

    /**
     * 作者ID int(11) unsigned
     */
    @Column(name = "author")
    @EntityDescAnno(value = "作者ID", type = "int", len = 11)
    private Integer author;

    /**
     * 图 varchar(255)
     */
    @Column(name = "img")
    @EntityDescAnno(value = "图", type = "varchar", len = 255)
    private String img;

    /**
     * 点赞次数 int(11) unsigned
     */
    @Column(name = "liked")
    @EntityDescAnno(value = "点赞次数", type = "int", len = 11)
    private Integer liked;

    /**
     * 评论次数 int(11) unsigned
     */
    @Column(name = "comments")
    @EntityDescAnno(value = "评论次数", type = "int", len = 11)
    private Integer comments;

    /**
     * 阅读次数,点击数 int(11) unsigned
     */
    @Column(name = "hits")
    @EntityDescAnno(value = "阅读次数", type = "int", len = 11)
    private Integer hits;

    /**
     * 状态: 关闭0,打开1 tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 更新时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "更新时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

}
