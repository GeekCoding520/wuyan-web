package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 用户账户信息
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_account")
@EntityDescAnno(value = "用户账户信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubAccount {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 账户 varchar(32)
     */
    @Column(name = "username")
    @EntityDescAnno(value = "账户", type = "varchar", len = 32)
    private String username;

    /**
     * 密码 varchar(128)
     */
    @Column(name = "password")
    @EntityDescAnno(value = "密码", type = "varchar", len = 128)
    private String password;

    /**
     * 手机号 varchar(15)
     */
    @Column(name = "phone")
    @EntityDescAnno(value = "手机号", type = "varchar", len = 15)
    private String phone;

    /**
     * 邮箱 varchar(32)
     */
    @Column(name = "email")
    @EntityDescAnno(value = "邮箱", type = "varchar", len = 32)
    private String email;

    /**
     * 状态：正常（1）；异常（0） tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 是否允许编辑: 允许(1),不允许(0) tinyint(1) unsigned
     */
    @Column(name = "cfg_modify")
    @EntityDescAnno(value = "是否允许编辑", type = "tinyint", len = 1)
    private Integer cfgModify;

    /**
     * 第三方开放平台用户标识 varchar(60)
     */
    @Column(name = "open_id")
    @EntityDescAnno(value = "第三方开放平台用户标识", type = "varchar", len = 60)
    private String openId;

    /**
     * 联合ID，对于多平台同一用户的唯一标识 varchar(60)
     */
    @Column(name = "union_id")
    @EntityDescAnno(value = "联合ID", type = "varchar", len = 60)
    private String unionId;

    /**
     * 账户来源，普通注册（0），后台创建（1），微信公众号（2），微信小程序（3），手机号一键登录（4），其它（5） smallint(1) unsigned
     */
    @Column(name = "source")
    @EntityDescAnno(value = "账户来源", type = "smallint", len = 1)
    private Integer source;

    /**
     * 归属部门 int(11) unsigned
     */
    @Column(name = "dept")
    @EntityDescAnno(value = "归属部门", type = "int", len = 11)
    private Integer dept;

    /**
     * 岗位 varchar(1024)
     */
    @Column(name = "post")
    @EntityDescAnno(value = "岗位", type = "varchar", len = 1024)
    private String post;

    /**
     * 角色 varchar(1024)
     */
    @Column(name = "role")
    @EntityDescAnno(value = "角色", type = "varchar", len = 1024)
    private String role;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 更新时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "更新时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

}
