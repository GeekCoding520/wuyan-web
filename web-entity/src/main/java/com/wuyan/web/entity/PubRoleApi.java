package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 角色与接口关系
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_role_api")
@EntityDescAnno(value = "角色与接口关系")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubRoleApi {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 角色ID int(11) unsigned
     */
    @Column(name = "role_id")
    @EntityDescAnno(value = "角色ID", type = "int", len = 11)
    private Integer roleId;

    /**
     * 接口ID int(11) unsigned
     */
    @Column(name = "apid")
    @EntityDescAnno(value = "接口ID", type = "int", len = 11)
    private Integer apid;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
