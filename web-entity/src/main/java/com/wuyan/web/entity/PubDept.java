package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 部门表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_dept")
@EntityDescAnno(value = "部门表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubDept {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 部门名称 varchar(255)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "部门名称", type = "varchar", len = 255)
    private String name;

    /**
     * 上级部门ID int(11) unsigned
     */
    @Column(name = "parent_id")
    @EntityDescAnno(value = "上级部门ID", type = "int", len = 11)
    private Integer parentId;

    /**
     * 部门在当前层级下的顺序，由小到大 int(3) unsigned
     */
    @Column(name = "cfg_sort")
    @EntityDescAnno(value = "部门在当前层级下的顺序", type = "int", len = 3)
    private Integer cfgSort;

    /**
     * 部门层级 varchar(255)
     */
    @Column(name = "level")
    @EntityDescAnno(value = "部门层级", type = "varchar", len = 255)
    private String level;

    /**
     * 联系人|负责人 varchar(32)
     */
    @Column(name = "contacts")
    @EntityDescAnno(value = "联系人|负责人", type = "varchar", len = 32)
    private String contacts;

    /**
     * 联系电话 varchar(15)
     */
    @Column(name = "contact_tel")
    @EntityDescAnno(value = "联系电话", type = "varchar", len = 15)
    private String contactTel;

    /**
     * 联系邮箱 varchar(32)
     */
    @Column(name = "contact_email")
    @EntityDescAnno(value = "联系邮箱", type = "varchar", len = 32)
    private String contactEmail;

    /**
     * 状态 tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 更新时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "更新时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

}
