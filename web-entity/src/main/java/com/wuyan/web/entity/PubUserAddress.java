package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 用户地址
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_user_address")
@EntityDescAnno(value = "用户地址")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubUserAddress {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 用户ID int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "用户ID", type = "int", len = 11)
    private Integer userId;

    /**
     * 姓名，称呼 varchar(60)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "姓名", type = "varchar", len = 60)
    private String name;

    /**
     * 联系号码 varchar(15)
     */
    @Column(name = "tel")
    @EntityDescAnno(value = "联系号码", type = "varchar", len = 15)
    private String tel;

    /**
     * 省份 varchar(32)
     */
    @Column(name = "province")
    @EntityDescAnno(value = "省份", type = "varchar", len = 32)
    private String province;

    /**
     * 所在市 varchar(32)
     */
    @Column(name = "city")
    @EntityDescAnno(value = "所在市", type = "varchar", len = 32)
    private String city;

    /**
     * 区县 varchar(32)
     */
    @Column(name = "county")
    @EntityDescAnno(value = "区县", type = "varchar", len = 32)
    private String county;

    /**
     * 详细地址 varchar(255)
     */
    @Column(name = "address")
    @EntityDescAnno(value = "详细地址", type = "varchar", len = 255)
    private String address;

    /**
     * 经度 decimal(13,10)
     */
    @Column(name = "lng")
    @EntityDescAnno(value = "经度", type = "decimal", len = 13)
    private Double lng;

    /**
     * 维度 decimal(13,10)
     */
    @Column(name = "lat")
    @EntityDescAnno(value = "维度", type = "decimal", len = 13)
    private Double lat;

    /**
     * 是否默认，否（0），是（1） tinyint(1) unsigned
     */
    @Column(name = "default_status")
    @EntityDescAnno(value = "是否默认", type = "tinyint", len = 1)
    private Integer defaultStatus;

    /**
     * 删除时间 timestamp
     */
    @Column(name = "delete_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "删除时间", type = "timestamp", len = -1)
    private LocalDateTime deleteTime;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 更新时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "更新时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

}
