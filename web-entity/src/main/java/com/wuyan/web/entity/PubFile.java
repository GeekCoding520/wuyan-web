package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 上载文件信息
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_file")
@EntityDescAnno(value = "上载文件信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubFile {

    /**
     * ID int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "ID", type = "int", len = 11)
    private Integer id;

    /**
     * 源文件名称 varchar(255)
     */
    @Column(name = "source_name")
    @EntityDescAnno(value = "源文件名称", type = "varchar", len = 255)
    private String sourceName;

    /**
     * 访问路径-web varchar(255)
     */
    @Column(name = "url")
    @EntityDescAnno(value = "访问路径-web", type = "varchar", len = 255)
    private String url;

    /**
     * 文件保存位置（完整的） varchar(255)
     */
    @Column(name = "path")
    @EntityDescAnno(value = "文件保存位置（完整的）", type = "varchar", len = 255)
    private String path;

    /**
     * 文件类型，一般指后缀名 varchar(10)
     */
    @Column(name = "type")
    @EntityDescAnno(value = "文件类型", type = "varchar", len = 10)
    private String type;

    /**
     * 文件类型名称 varchar(20)
     */
    @Column(name = "type_name")
    @EntityDescAnno(value = "文件类型名称", type = "varchar", len = 20)
    private String typeName;

    /**
     * 预览路径-web varchar(255)
     */
    @Column(name = "pre_url")
    @EntityDescAnno(value = "预览路径-web", type = "varchar", len = 255)
    private String preUrl;

    /**
     * 预览文件保存位置 varchar(255)
     */
    @Column(name = "pre_path")
    @EntityDescAnno(value = "预览文件保存位置", type = "varchar", len = 255)
    private String prePath;

    /**
     * 文件大小，单位：字节 bigint(20)
     */
    @Column(name = "size")
    @EntityDescAnno(value = "文件大小", type = "bigint", len = 20)
    private Long size;

    /**
     * 上传者 int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "上传者", type = "int", len = 11)
    private Integer userId;

    /**
     * 上传时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "上传时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
