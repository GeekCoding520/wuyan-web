package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 基本配置
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_config")
@EntityDescAnno(value = "基本配置")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubConfig {

    /**
     * ID int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "ID", type = "int", len = 11)
    private Integer id;

    /**
     * 节点名称 varchar(255)
     */
    @Column(name = "node")
    @EntityDescAnno(value = "节点名称", type = "varchar", len = 255)
    private String node;

    /**
     * 类别 varchar(32)
     */
    @Column(name = "type")
    @EntityDescAnno(value = "类别", type = "varchar", len = 32)
    private String type;

    /**
     * 键 varchar(64)
     */
    @Column(name = "cfg_key")
    @EntityDescAnno(value = "键", type = "varchar", len = 64)
    private String cfgKey;

    /**
     * 值 varchar(512)
     */
    @Column(name = "cfg_value")
    @EntityDescAnno(value = "值", type = "varchar", len = 512)
    private String cfgValue;

    /**
     * 是否可修改: 允许(1), 不允许(0) tinyint(1) unsigned
     */
    @Column(name = "cfg_modify")
    @EntityDescAnno(value = "是否可修改", type = "tinyint", len = 1)
    private Integer cfgModify;

    /**
     * 简要说明 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "简要说明", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
