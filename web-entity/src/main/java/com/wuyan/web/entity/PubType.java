package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 类别：一般以一级分类为起始（不可改，初始化定义），向下扩展
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_type")
@EntityDescAnno(value = "类别：一般以一级分类为起始（不可改，初始化定义），向下扩展")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubType {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 名称 varchar(20)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "名称", type = "varchar", len = 20)
    private String name;

    /**
     * 父级 int(11) unsigned
     */
    @Column(name = "parent_id")
    @EntityDescAnno(value = "父级", type = "int", len = 11)
    private Integer parentId;

    /**
     * 是否允许修改: 允许(1), 不允许(0) tinyint(1) unsigned
     */
    @Column(name = "cfg_modify")
    @EntityDescAnno(value = "是否允许修改", type = "tinyint", len = 1)
    private Integer cfgModify;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
