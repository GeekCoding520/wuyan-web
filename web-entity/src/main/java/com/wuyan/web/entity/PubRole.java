package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 角色表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_role")
@EntityDescAnno(value = "角色表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubRole {

    /**
     * 主键 int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "主键", type = "int", len = 11)
    private Integer id;

    /**
     * 名称 varchar(64)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "名称", type = "varchar", len = 64)
    private String name;

    /**
     * 状态：开启（1）；关闭（0） tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 权限限定名 varchar(32)
     */
    @Column(name = "identity")
    @EntityDescAnno(value = "权限限定名", type = "varchar", len = 32)
    private String identity;

    /**
     * 是否允许编辑：允许（1）；不允许（0） tinyint(1) unsigned
     */
    @Column(name = "cfg_modify")
    @EntityDescAnno(value = "是否允许编辑", type = "tinyint", len = 1)
    private Integer cfgModify;

    /**
     * 菜单权限 text
     */
    @Column(name = "menu_ids")
    @EntityDescAnno(value = "菜单权限", type = "text", len = -1)
    private String menuIds;

    /**
     * 数据权限 text
     */
    @Column(name = "data_identity")
    @EntityDescAnno(value = "数据权限", type = "text", len = -1)
    private String dataIdentity;

    /**
     * 数据权限范围，类型： all（全部），custom（自定义），dept-current（仅当前部门），dept-current-and-sub（当前部门及以下），self（仅自身） varchar(32)
     */
    @Column(name = "data_identity_type")
    @EntityDescAnno(value = "数据权限范围", type = "varchar", len = 32)
    private String dataIdentityType;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

}
