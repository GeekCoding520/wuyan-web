package com.wuyan.web.entity;

import javax.persistence.*;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 文章内容表
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_article_content")
@EntityDescAnno(value = "文章内容表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubArticleContent {

    /**
     * 文章表ID int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "文章表ID", type = "int", len = 11)
    private Integer id;

    /**
     * 文章内容 text
     */
    @Column(name = "content")
    @EntityDescAnno(value = "文章内容", type = "text", len = -1)
    private String content;

}
