package com.wuyan.web.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.wuyan.helper.kit.jackson.DateTimeDeserializer;
import com.wuyan.helper.kit.EntityDescAnno;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * 自定义表单
 * 
 * @author wuyan
 */
@Entity
@Table(name = "pub_form")
@EntityDescAnno(value = "自定义表单")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubForm {

    /**
     * null int(11) unsigned
     */
    @Id
    @GeneratedValue()
    @Column(name = "id")
    @EntityDescAnno(value = "", type = "int", len = 11)
    private Integer id;

    /**
     * 名称 varchar(32)
     */
    @Column(name = "name")
    @EntityDescAnno(value = "名称", type = "varchar", len = 32)
    private String name;

    /**
     * 表单名 varchar(64)
     */
    @Column(name = "form_ref")
    @EntityDescAnno(value = "表单名", type = "varchar", len = 64)
    private String formRef;

    /**
     * 表单模型 varchar(32)
     */
    @Column(name = "form_model")
    @EntityDescAnno(value = "表单模型", type = "varchar", len = 32)
    private String formModel;

    /**
     * 接口地址 varchar(255)
     */
    @Column(name = "url")
    @EntityDescAnno(value = "接口地址", type = "varchar", len = 255)
    private String url;

    /**
     * 表单尺寸 varchar(32)
     */
    @Column(name = "size")
    @EntityDescAnno(value = "表单尺寸", type = "varchar", len = 32)
    private String size;

    /**
     * 标签对其方式 varchar(255)
     */
    @Column(name = "label_position")
    @EntityDescAnno(value = "标签对其方式", type = "varchar", len = 255)
    private String labelPosition;

    /**
     * 标签宽度 smallint(4)
     */
    @Column(name = "label_width")
    @EntityDescAnno(value = "标签宽度", type = "smallint", len = 4)
    private Integer labelWidth;

    /**
     * 校验模型 varchar(32)
     */
    @Column(name = "form_rules")
    @EntityDescAnno(value = "校验模型", type = "varchar", len = 32)
    private String formRules;

    /**
     * 栅格间隔 int(5) unsigned
     */
    @Column(name = "gutter")
    @EntityDescAnno(value = "栅格间隔", type = "int", len = 5)
    private Integer gutter;

    /**
     * 是否禁用: 禁用(1), 不禁用(0) tinyint(1) unsigned
     */
    @Column(name = "disabled")
    @EntityDescAnno(value = "是否禁用", type = "tinyint", len = 1)
    private Integer disabled;

    /**
     * 暂时未知 int(4) unsigned
     */
    @Column(name = "span")
    @EntityDescAnno(value = "暂时未知", type = "int", len = 4)
    private Integer span;

    /**
     * 表单按钮:启用(1), 禁用(0) tinyint(1) unsigned
     */
    @Column(name = "form_btns")
    @EntityDescAnno(value = "表单按钮", type = "tinyint", len = 1)
    private Integer formBtns;

    /**
     * 显示未选中的边框线:启用(1), 禁用(0) tinyint(1) unsigned
     */
    @Column(name = "un_focused_component_border")
    @EntityDescAnno(value = "显示未选中的边框线", type = "tinyint", len = 1)
    private Integer unFocusedComponentBorder;

    /**
     * 流程ID varchar(64)
     */
    @Column(name = "flow_id")
    @EntityDescAnno(value = "流程ID", type = "varchar", len = 64)
    private String flowId;

    /**
     * 流程名称 varchar(64)
     */
    @Column(name = "flow_name")
    @EntityDescAnno(value = "流程名称", type = "varchar", len = 64)
    private String flowName;

    /**
     * 状态: 启用(1), 禁用(0) tinyint(1) unsigned
     */
    @Column(name = "status")
    @EntityDescAnno(value = "状态", type = "tinyint", len = 1)
    private Integer status;

    /**
     * 页面类型: 页面,弹框 varchar(32)
     */
    @Column(name = "page_type")
    @EntityDescAnno(value = "页面类型", type = "varchar", len = 32)
    private String pageType;

    /**
     * 备注 varchar(255)
     */
    @Column(name = "remark")
    @EntityDescAnno(value = "备注", type = "varchar", len = 255)
    private String remark;

    /**
     * 表单内容 text
     */
    @Column(name = "fields")
    @EntityDescAnno(value = "表单内容", type = "text", len = -1)
    private String fields;

    /**
     * 创建者 int(11) unsigned
     */
    @Column(name = "user_id")
    @EntityDescAnno(value = "创建者", type = "int", len = 11)
    private Integer userId;

    /**
     * 当前已录入数据总数 int(11) unsigned
     */
    @Column(name = "data_count")
    @EntityDescAnno(value = "当前已录入数据总数", type = "int", len = 11)
    private Integer dataCount;

    /**
     * 表单分类名 varchar(32)
     */
    @Column(name = "type_name")
    @EntityDescAnno(value = "表单分类名", type = "varchar", len = 32)
    private String typeName;

    /**
     * excel导入时首条数据所在的行数 int(11) unsigned
     */
    @Column(name = "excel_data_row_start")
    @EntityDescAnno(value = "excel导入时首条数据所在的行数", type = "int", len = 11)
    private Integer excelDataRowStart;

    /**
     * excel中表头所在的位置 varchar(32)
     */
    @Column(name = "excel_title_position")
    @EntityDescAnno(value = "excel中表头所在的位置", type = "varchar", len = 32)
    private String excelTitlePosition;

    /**
     * excel严格匹配工作表名 tinyint(1) unsigned
     */
    @Column(name = "excel_sheet_strict_match")
    @EntityDescAnno(value = "excel严格匹配工作表名", type = "tinyint", len = 1)
    private Integer excelSheetStrictMatch;

    /**
     * 自定义布局内容 text
     */
    @Column(name = "layout_design")
    @EntityDescAnno(value = "自定义布局内容", type = "text", len = -1)
    private String layoutDesign;

    /**
     * 创建时间 timestamp
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "创建时间", type = "timestamp", len = -1)
    private LocalDateTime createTime;

    /**
     * 操作时间 timestamp
     */
    @Column(name = "operate_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @EntityDescAnno(value = "操作时间", type = "timestamp", len = -1)
    private LocalDateTime operateTime;

    /**
     * 输入类型：form（单体表单），table(表格输入) varchar(32)
     */
    @Column(name = "input_type")
    @EntityDescAnno(value = "输入类型", type = "varchar", len = 32)
    private String inputType;

}
