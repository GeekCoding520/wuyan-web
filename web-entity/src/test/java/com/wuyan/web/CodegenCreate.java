package com.wuyan.web;

import io.github.gcdd1993.jpa.codegen.CodeGenerator;
import org.junit.jupiter.api.Test;

/**
 * CRUD代码生成器
 *
 * @author wuyan
 */
class CodegenCreate {
    // 配置文件路径
    private static final String TEMP_NAME = "src/test/resources/codegen.cfg";
    // 实体类所在完整包名
    private static final String PAG_PAGE_ENTITY = "com.wuyan.web.entity";

    /**
     * 生成web、service、repo的CRUD代码到PAG_PAGE_ENTITY的相对目录
     */
    @Test
    void generate() {
        CodeGenerator codeGenerator = new CodeGenerator(TEMP_NAME)
                // 批量加入生成的实体类包名
                .packInclude(PAG_PAGE_ENTITY)
                // 排除生成的实体类名，通常与packInclude混用，以排除包下的特殊实体类不参与生成代码
                .clazzExlude()
                // 加入生成的实体类名
                .clazzInclude()
                .registerRender("form");

        codeGenerator.generate();
    }
}
