package com.wuyan.web.service;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.wuyan.web.helper.PageHelper;
import com.wuyan.web.helper.ReflexHelper;
import com.wuyan.web.helper.entity.EntityFieldInfo;
import com.wuyan.web.helper.rep.RepPageData;
import com.wuyan.web.helper.req.CustomQueryHelper;
import com.wuyan.web.helper.req.CustomQueryParams;
import com.wuyan.web.helper.req.CustomQueryOrderParams;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuyan.web.entity.PubFeedback;
import com.wuyan.web.entity.QPubFeedback;
import com.wuyan.web.repo.PubFeedbackRepo;
import com.wuyan.web.form.PubFeedbackForm;
import org.springframework.transaction.annotation.Transactional;
import lombok.extern.slf4j.Slf4j;

import com.wuyan.web.helper.DataEncryptionHelper;
import static com.wuyan.web.config.EntityCfg.DATA_ENCRYPTION;
import static com.wuyan.web.config.EntityCfg.ER_BATCH_INSERT_ON_CLEAR;


import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.time.LocalDateTime;


/**
 * service for PubFeedback generated by jpa-codegen
 * codegen auto
 *
 * @author wuyan
 * @date 2021-09-14 16:35:34
 */

@Slf4j
@Service
public class PubFeedbackService {

    @Autowired
    private PubFeedbackRepo pubFeedbackRepo;

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    private JPAQueryFactory queryFactory;

    @PostConstruct
    public void init() {
        queryFactory = new JPAQueryFactory(entityManager);
    }

    /**
     * 创建实体
     *
     * @param form 表单
     * @return 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public PubFeedback create(PubFeedbackForm form) throws Exception {
        // 特殊处理：加密
        DataEncryptionHelper.encode("pubFeedback", form, DATA_ENCRYPTION);

        PubFeedback pubFeedback = PubFeedback.builder().build();
        BeanUtils.copyProperties(form, pubFeedback);

        pubFeedback.setCreateTime(LocalDateTime.now());

        return pubFeedbackRepo.save(pubFeedback);
    }

    /**
    * 创建实体-批量
    * 根据 EntityConfig.ER_BATCH_INSERT_ON_CLEAR判断是否需要执行对应的清除操作
    *
    * @param forms 表单集合
    */
    @Transactional(rollbackFor = Exception.class)
    public List<PubFeedback> creates(List<PubFeedbackForm> forms) throws Exception {
        List<PubFeedback> res = new ArrayList<>();

        // 根据ER_BATCH_INSERT_ON_CLEAR匹配是否在批量插入时执行清空操作
        String key = "pubFeedback";
        if (ER_BATCH_INSERT_ON_CLEAR.containsKey(key) && forms.size() > 0) {
            EntityFieldInfo<Integer> entityFieldInfo = ER_BATCH_INSERT_ON_CLEAR.get(key);
            PubFeedbackForm pubFeedbackForm = forms.get(0);

            try {
                // 匹配查询条件
                Integer value = ReflexHelper.getFieldValue(pubFeedbackForm, entityFieldInfo.getName(), entityFieldInfo.getClz());
                List<CustomQueryParams> paramsList = new ArrayList<>();
                Integer[] right = {value};
                paramsList.add(CustomQueryParams.builder().left(entityFieldInfo.getName()).op("eq").right(right).build());

                QPubFeedback q = QPubFeedback.pubFeedback;
                PathBuilder<PubFeedback> pathBuilder = new PathBuilder<>(PubFeedback.class, q.getMetadata());

                // 构建查询条件
                CustomQueryHelper<PubFeedback> customQueryHelper = CustomQueryHelper.<PubFeedback>builder()
                        .paramsList(paramsList)
                        .pathBuilder(pathBuilder)
                        .entityClass(PubFeedback.class)
                        .build()
                        .structure();

                // 先查询，再删除
                List<PubFeedback> deleteAll = queryFactory.select(q).from(q)
                        .where(customQueryHelper.getPredicates().toArray(new Predicate[0]))
                        .fetch();

                Integer[] ids = deleteAll.stream()
                        .map(PubFeedback::getId)
                        .toArray(Integer[]::new);
                delete(ids);
            } catch (IllegalAccessException e) {
                log.error("查询列：" + entityFieldInfo.getName() + "非法访问。" + e.getMessage());
            } catch (NoSuchMethodException e) {
                log.error("查询列：" + entityFieldInfo.getName() + "不存在此方法。" + e.getMessage());
            } catch (InvocationTargetException e) {
                log.error("查询列：" + entityFieldInfo.getName() + "调用方法时错误。" + e.getMessage());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        for (PubFeedbackForm form : forms) {
            res.add(create(form));
        }

        return res;
    }

    /**
     * 删除实体
     *
     * @param id 需要被删除的行ID组
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer[] id) {
        List<PubFeedback> all = pubFeedbackRepo.findAllById(Arrays.asList(id));

        if(all.isEmpty()){
            return null;
        }

        List<Integer> ids = all.stream()
            .map(PubFeedback::getId)
            .collect(Collectors.toList());

        return pubFeedbackRepo.deleteAll(ids);
    }

    /**
     * 更新实体
     *
     * @param form 表单
     * @param id      实体id
     * @return 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public Optional<PubFeedback> update(PubFeedbackForm form, Integer id)  throws Exception {
            // 特殊处理：加密
            DataEncryptionHelper.encode("pubFeedback", form, DATA_ENCRYPTION);

            return pubFeedbackRepo.findById(id)
                .map(pubFeedback -> {
                    BeanUtils.copyProperties(form, pubFeedback);


                    return pubFeedbackRepo.save(pubFeedback);
                });
    }

    /**
    * 根据ID更新指定字段的值
    *
    * @param id    ID
    * @param field 字段名
    * @param value 字段值
    * @return long
    */
    @Transactional(rollbackFor = Exception.class)
    public long updateByField(Integer id, String field, String value) throws Exception {
        List<EntityFieldInfo<?>> entityFieldInfoList = DATA_ENCRYPTION.get("pubFeedback");
        value = DataEncryptionHelper.encode(field, value, entityFieldInfoList);

        QPubFeedback q = QPubFeedback.pubFeedback;
        PathBuilder<PubFeedback> pathBuilder = new PathBuilder<>(PubFeedback.class, q.getMetadata());

        List<CustomQueryParams> paramsList = new ArrayList<>();
            paramsList.add(CustomQueryParams.builder()
            .left("id")
            .op("eq")
            .right(new Integer[]{id})
            .build());
            List<CustomQueryOrderParams> orderList = new ArrayList<>();

        // 构建查询条件
        CustomQueryHelper<PubFeedback> customQueryHelper = CustomQueryHelper.<PubFeedback>builder()
                .paramsList(paramsList)
                .pathBuilder(pathBuilder)
                .paramsOrders(orderList)
                .entityClass(PubFeedback.class)
                .build()
                .structure();

        JPAUpdateClause update = queryFactory.update(q);
        JPAUpdateClause set = customQueryHelper.setJPAUpdateClause(update, field, value);
        JPAUpdateClause where = set.where(customQueryHelper.getPredicates().toArray(new Predicate[0]));

        return where.execute();
    }

    /**
     * 获取一个实体对象
     *
     * @param id 实体id
     * @return 实体对象
     */
    public Optional<PubFeedback> get(Integer id) {
        return pubFeedbackRepo.findById(id);
    }

    /**
    * 分页接口
    *
    * @param predicates 查询条件
    * @param orderSpecifiers 排序
    * @param isPage 是否分页
    * @param page 当前页
    * @param limit 每页大小
    * @return 分页结果
    */
    public RepPageData<PubFeedback> page(List<Predicate> predicates, List<OrderSpecifier<?>> orderSpecifiers, Boolean isPage, Integer page, Integer limit) {
        QPubFeedback q = QPubFeedback.pubFeedback;
        JPAQuery<PubFeedback> from = queryFactory.select(q).from(q);

        if(predicates != null && !predicates.isEmpty()){
            from = from.where(predicates.toArray(new Predicate[0]));
        }

        if(orderSpecifiers != null && !orderSpecifiers.isEmpty()){
            from = from.orderBy(orderSpecifiers.toArray(new OrderSpecifier[0]));
        }

        // 分页或者返回全部
        if (isPage != null && isPage) {
            QueryResults<PubFeedback> pubFeedbackQueryResults = from
                    .offset((page - 1) * limit)
                    .limit(limit)
                    .fetchResults();
            return PageHelper.pageResp(pubFeedbackQueryResults, page, limit);
        } else {
            List<PubFeedback> pubFeedbackQueryResults = from.fetch();
            return RepPageData.<PubFeedback>builder()
                    .list(pubFeedbackQueryResults)
                    .page(1)
                    .limit(pubFeedbackQueryResults.size())
                    .build();
        }
    }
}