package com.wuyan.web.service.extend;

import com.fasterxml.jackson.databind.JsonNode;
import com.wuyan.web.entity.PubFunction;
import com.wuyan.web.repo.PubFunctionRepo;
import com.wuyan.web.service.PubFunctionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 扩展
 *
 * @author wuyan
 * @date 2021-04-02 10:48:15
 */

@Slf4j
@Primary
@Service
public class PubFunctionServiceExtend extends PubFunctionService {

    @Autowired
    private PubFunctionRepo pubFunctionRepo;

    /**
     * 导入数据-JSON格式
     * 全量更新，更新成功后将清空原有数据。即替换更新
     *
     * @param jsonNode 数据内容
     * @return List<PubFunction>
     */
    @Transactional(rollbackFor = Exception.class)
    public List<PubFunction> importJSON(JsonNode jsonNode) {
        List<PubFunction> beforeAll = pubFunctionRepo.findAll();

        boolean array = jsonNode.isArray();
        if (array) {
            AtomicInteger sort = new AtomicInteger(999);
            jsonNode.forEach(node -> importJSONSave(node, null, sort));
        } else {
            return beforeAll;
        }

        pubFunctionRepo.deleteAll(beforeAll);
        return pubFunctionRepo.findAll();
    }

    /**
     * 保存数据
     *
     * @param jsonNode 数据
     * @param parent   父级
     * @param sort     排序序号
     */
    @Transactional(rollbackFor = Exception.class)
    public void importJSONSave(JsonNode jsonNode, PubFunction parent, AtomicInteger sort) {
        PubFunction function = save(jsonNode, parent, sort);

        if (jsonNode.has("children")) {
            JsonNode children = jsonNode.get("children");
            if (children.isArray()) {
                children.forEach(node -> importJSONSave(node, function, new AtomicInteger(999)));
            } else {
                save(jsonNode, function, new AtomicInteger(999));
            }
        }
    }

    /**
     * 保存
     *
     * @param jsonNode 节点数据
     * @param parent   父级
     * @param sort     排序
     * @return PubFunction
     */
    private PubFunction save(JsonNode jsonNode, PubFunction parent, AtomicInteger sort) {
        return pubFunctionRepo.save(PubFunction.builder()
                .parentId(null == parent ? 0 : parent.getId())
                .type(jsonNode.has("type") ? jsonNode.get("type").asInt() : 1)
                .name(jsonNode.has("name") ? jsonNode.get("name").asText() : "")
                .icon(jsonNode.has("icon") ? jsonNode.get("icon").asText() : "")
                .identity(jsonNode.has("identity") ? jsonNode.get("identity").asText() : "")
                .url(jsonNode.has("url") ? jsonNode.get("url").asText() : "")
                .clickFun(jsonNode.has("clickFun") ? jsonNode.get("clickFun").asText() : "")
                .status(1)
                .cfgSort(jsonNode.has("cfgSort") ? jsonNode.get("cfgSort").asInt() : sort.getAndDecrement())
                .cfgModify(1)
                .remark(jsonNode.has("remark") ? jsonNode.get("remark").asText() : "")
                .createTime(LocalDateTime.now())
                .build());
    }
}