package ${packageName};

import ${entity.packageName}.${entity.className};
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

<#list imports as import>
<#if import!="java.lang.Integer">
import ${import};
</#if>
</#list>

/**
 * repository for ${entity.className} generated by jpa-codegen
 * ${comments}
 *
 * @author ${author}
 * @date ${date}
 */

@Repository
public interface ${className} extends JpaRepository<${entity.className}, ${entity.id.className}>, QuerydslPredicateExecutor<${entity.className}> {
    /**
    * 批量删除
    * @param ids 主键
    * @return int
    */
    @Modifying
    @Query("delete from ${entity.className} where id in (:ids)")
    int deleteAll(@Param("ids") List<Integer> ids);
}