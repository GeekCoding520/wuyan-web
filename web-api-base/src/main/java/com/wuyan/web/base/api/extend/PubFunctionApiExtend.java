package com.wuyan.web.base.api.extend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wuyan.web.aop.ApiLogAnnotation;
import com.wuyan.web.base.helper.BaseRequestMapping;
import com.wuyan.web.entity.PubFunction;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.rep.RepBody;
import com.wuyan.web.helper.rep.RepHelper;
import com.wuyan.web.service.extend.PubFunctionServiceExtend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 扩展接口
 */

@Slf4j
@RestController
@BaseRequestMapping("/api/pub-function")
public class PubFunctionApiExtend extends BaseApi implements RepHelper {
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PubFunctionServiceExtend pubFunctionServiceExtend;

    /**
     * 菜单导入-JSON
     *
     * @param jsonStr 完整的菜单配置信息，JSON格式
     * @return RepBody<List < PubFunction>>
     */
    @PostMapping("/import-json")
    @ApiLogAnnotation(name = "菜单导入-JSON", identity = "PubFunction:import-json")
    @Transactional(rollbackFor = Exception.class)
    public RepBody<List<PubFunction>> importJSON(@RequestBody String jsonStr) throws JsonProcessingException {
        jsonStr = jsonStr.replace("\n", "");
        jsonStr = jsonStr.replace("\t", "");
        JsonNode jsonNode = mapper.readTree(jsonStr);

        return ok(pubFunctionServiceExtend.importJSON(jsonNode));
    }
}