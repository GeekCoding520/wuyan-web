package com.wuyan.web.base.api.extend;

import com.wuyan.web.aop.ApiLogAnnotation;
import com.wuyan.web.base.helper.BaseRequestMapping;
import com.wuyan.web.helper.BaseApi;
import com.wuyan.web.helper.rep.RepBody;
import com.wuyan.web.helper.rep.RepCodeEnum;
import com.wuyan.web.helper.rep.RepHelper;
import com.wuyan.web.service.extend.PubFormServiceExtend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 扩展接口
 */

@Slf4j
@RestController
@BaseRequestMapping("/api/pub-form")
public class PubFormApiExtend extends BaseApi implements RepHelper {
    @Autowired
    private PubFormServiceExtend pubFormService;

    /**
     * 更新表单对应的流程信息
     *
     * @param id       id
     * @param flowId   流程ID
     * @param flowName 流程名称
     * @return RepBody<Long>
     */
    @PutMapping("/{id}/up-flow/{flowId}/{flowName}")
    @ApiLogAnnotation(name = "更新表单对应的流程信息", identity = "PubForm:up-flow")
    @Transactional(rollbackFor = Exception.class)
    public RepBody<Long> upFlow(
            @PathVariable("id") Integer id,
            @PathVariable("flowId") String flowId,
            @PathVariable("flowName") String flowName
    ) throws Exception {
        long update = pubFormService.upFlow(id, flowId, flowName);
        return update > 0 ? ok(update) : error(RepCodeEnum.ERR_UPDATE);
    }
}