package com.wuyan.web.config;

import com.wuyan.web.base.helper.BaseRequestMapping;
import com.wuyan.web.helper.WuyanWebProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 配置统一接口访问路径的前缀
 *
 * @author wuyan
 */
@Configuration
public class BaseRequestMappingCfg implements WebMvcConfigurer {

    @Resource
    private WuyanWebProperties wuyanWebProperties;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer
                .addPathPrefix(wuyanWebProperties.getWeb().getBasePrefix(), c -> c.isAnnotationPresent(BaseRequestMapping.class));
    }
}
